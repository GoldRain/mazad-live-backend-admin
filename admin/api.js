User = require('../models/user')
ReportUsers = require('../models/reportedUsers')
Route = require('../route/route')
Posts = require('../models/posts')
Country = require('../models/countries')
Rou = require('../route/route')
Fun = require('../functions/function')

var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var schedule = require('node-schedule');
const nodemailer = require('nodemailer');

exports.userListA = function (req, res) {
    console.log("userListAdmin");
    let number = req.body.number;
    console.log("number: ", number)
    let query = number * 15;
    User.find({ is_admin_block: false }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    }).skip(query).limit(15)
};

exports.totalDeviceList = function (req, res) {
    console.log("totalDeviceList");
    deviceType = req.body.deviceType;
    User.aggregate([{ $match: { user_type: deviceType } }], function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    })
};

exports.deviceList = function (req, res) {
    console.log("deviceList");
    let number = req.body.number;
    let query = number * 15;
    deviceType = req.body.name
    User.aggregate([{ $match: { user_type: deviceType, is_admin_block: false } },
    { "$skip": query }, { "$limit": 15 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    })
};

exports.reportList = function (req, res) {
    console.log("reportList");
    let number = req.body.number,
        query = number * 15;
    ReportUsers.aggregate([
        { $sort: { created_at: 1 } },
        { $group: { _id: "$reported_user_id", count: { $sum: 1 } } },
        { $lookup: { from: "users", localField: "_id", foreignField: "_id", as: "reportedUserData" } },
        { "$skip": query }, { "$limit": 10 }
    ],
        function (error, data) {
            if (error) {
                res.json({ error: true, message: error })
            } else if (data.length == 0) {
                res.json({ error: true, message: "Data not found", description: [] })
            } else {
                res.json({ error: false, description: data });
            }
        })
    // })
};

exports.allPostsA = function (req, res) {
    let number = req.body.number;
    let query = number * 10;
    // authenticate(req, res, () => {
    Posts.aggregate([{ $match: { "is_deleted": false } }, { $lookup: { from: 'tags', localField: 'tags.tag_id', foreignField: '_id', as: 'tags.tag_id' } },
    { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'userDetails' } },
    { $sort: { "post_created_at": -1 } }, { "$skip": query }, { "$limit": 10 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: "Data not found" });
        } else {
            res.json({ error: false, message: data });
        }
    })
    // })
};

exports.allPost = function (req, res) {
    Post.aggregate([{ $count: "Counting" }], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: "Data not found" });
        } else {
            res.json({ error: false, message: data });
        }
    })
};

exports.countRequest = function (req, res) {
    PaymentRequests.aggregate([{ $match: { "status": "requested" } }, { $count: "Counting" }], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            res.json({ error: false, message: data });
        }
    })
};

exports.getMyPostA = function (req, res) {
    let number = req.body.number,
        user_id = req.body.user_id;
    let query = number * 10;
    // authenticate(req, res, () => {
    Posts.aggregate([{ $match: { post_user_id: ObjectId(user_id) } }, { $lookup: { from: 'tags', localField: 'tags.tag_id', foreignField: '_id', as: 'tags.tag_id' } },
    { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'userDetails' } },
    { $sort: { "post_created_at": -1 } }, { "$skip": query }, { "$limit": 10 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: "Data not found" });
        } else {
            res.json({ error: false, message: data });
        }
    })
    // })
};

exports.reportUserDetails = function (req, res) {
    console.log("reportUserDetails")
    let reported_user_id = req.body.reported_user_id;
    ReportUsers.aggregate([{ $match: { reported_user_id: ObjectId(reported_user_id) } },
    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } },
    { $lookup: { from: 'users', localField: 'reported_user_id', foreignField: '_id', as: 'reported_userDetails' } }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting the reported user details", description: error })
        } else if (data.length == 0) {
            res.json({ error: true, message: "Data not found", description: [] });
        } else {
            User.findOne({ _id: ObjectId(reported_user_id) }, function (error, reportedPerson) {
                if (error) {
                    res.json({ error: true, message: "Error while getting country details of reported user", description: error });
                } else if (!reportedPerson) {
                    res.json({ error: true, message: "No country data found" });
                } else {
                    res.json({ error: false, message: "Done", description: data, reportedPerson: reportedPerson });
                }
            })

        }
    })
};

exports.approvedPostList = function (req, res) {
    console.log("approvedPostList");
    let postList = [];
    let number = req.body.number;
    let query = number * 10;
    Posts.aggregate([{ $match: { 'bids.status': 'approved' } },
    { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'userDetails' } },
    { $lookup: { from: 'countries', localField: 'available_country_id.country_id', foreignField: '_id', as: 'available_country_id.country_id' } },
    { $lookup: { from: 'countries', localField: 'userDetails.country_code', foreignField: 'countryCode', as: 'userCountryDetails' } },
    { "$skip": query }, { "$limit": 10 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: data });
        }
    }).skip(query).limit(10)
};

exports.verificationList = function (req, res) {
    console.log("verificationList");
    User.find({ 'verification.status': 'pending' }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting pending verified users list", description: error });
        } else if (data.length == 0) {
            res.json({ error: true, message: "No user found" });
        } else {
            res.json({ error: false, message: "Done", description: data })
        }
    })
};

exports.requestToAdmin = function (req, res) {
    let number = req.body.number;
    let query = number * 10;
    console.log("requestToAdmin ", number);
    // type check
    PaymentRequests.aggregate([{ $match: { status: "requested" } },
    { $sort: { "created_at": -1 } }, { "$skip": query }, { "$limit": 10 }
    ], function (err, typeCheckData) {
        if (err) {
            res.json({ error: true, message: error });
        } else if (typeCheckData.length == 0) {
            res.json({ error: true, message: [] });
        } else {
            let type;
            let whole = [];
            if (typeCheckData.length > 0) {
                for (let i = 0; i < typeCheckData.length; i++) {
                    // console.log(">>>>>>>", typeCheckData[i]._id)
                    type = typeCheckData[i].type;
                    if (type == "post") { type = "allposts"; } else if (type == "story") { type = "stories"; } else { console.log("error") }

                    PaymentRequests.aggregate([{ $match: { _id: typeCheckData[i]._id } },
                    { $lookup: { from: 'ordersinfos', localField: 'order_id', foreignField: '_id', as: 'orderDetails' } },
                    { $lookup: { from: 'users', localField: 'seller_id', foreignField: '_id', as: 'sellerDetails' } },
                    { $lookup: { from: type, localField: 'type_id', foreignField: '_id', as: 'imageDetails' } },
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, message: error });
                        } else if (data.length == 0) {
                            res.json({ error: true, message: [] });
                        } else {
                            whole.push(data[0]);
                            // console.log("<<<<<<<", whole[i])
                            if (whole.length == typeCheckData.length) {
                                res.json({ error: true, message: whole });
                            }
                        }
                    })

                }
            } else {
                res.json({ error: true, message: [] });
            }
        }
    })
};

exports.loginAdmin = function (req, res) {
    console.log("loginAdmin");
    let adminEmail = req.body.email;
    let adminPassword = req.body.password;
    Admin.aggregate([{ $match: { $and: [{ "email": adminEmail }, { "password": adminPassword }] } }], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (data.length == 0) {
            res.json({ error: true, message: "Data not found" });
        } else {
            res.json({ error: false, message: data });
        }
    })
};

exports.sendMail = function (req, res) {
    console.log("sendMail");
    let output = `
        <p>Output</p>
    `;

    Admin.aggregate([{ $match: { "email": req.body.email } }], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (data.length == 0) {
            res.json({ error: true, message: "Email not found" });
        } else {
            let otp = Math.floor(100000 + Math.random() * 900000);
            output = `
            <p>Otp that's been generated is:</p>
            <p>${otp}</p>
            `;
            Otp.update({ email: req.body.email }, { $set: { 'otp': otp } }, { upsert: true }, function (err, data) {
                if (err) {
                    res.json({ error: true, message: "No otp generated" });
                } else {
                    let transporter = nodemailer.createTransport({
                        host: 'smtp.gmail.com',
                        port: 465,
                        secure: true, // true for 465, false for other ports
                        auth: {
                            user: "alrabiahi053@gmail.com", // generated ethereal user
                            pass: "Ib27620683" // generated ethereal password
                        },
                        tls: {
                            // do not fail on invalid certs
                            rejectUnauthorized: false
                        }
                    });

                    // setup email data with unicode symbols
                    let mailOptions = {
                        from: '"mazadAdmin" <alrabiahi053@gmail.com>', // sender address
                        to: req.body.email, // list of receivers
                        subject: 'OTP', // Subject line
                        text: 'Hello world?', // plain text body
                        html: output // html body
                    };

                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            console.log(error);
                            res.json({ error: true, message: "Something wend wrong" });
                        } else {
                            res.json({ error: false, message: "Success" });
                        }
                        // console.log('Message sent: %s', info.messageId);
                        // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                    });



                }
            })

        }
    })


};

exports.verifyAdminOtp = function (req, res) {
    console.log("verifyAdminOtp");
    let otp = req.body.otp;
    let email = req.body.email;
    Otp.aggregate([{ $match: { $and: [{ "email": email }, { "otp": otp }] } }], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (data.length == 0) {
            res.json({ error: true, message: "Data not found", data: [] });
        } else {
            res.json({ error: false, message: "Success", data: data });
        }
    })
};

exports.UpdateAdminPassword = function (req, res) {
    console.log("UpdateAdminPassword");
    password = req.body.password;
    email = req.body.email;

    Admin.update({ email: req.body.email }, { $set: { 'password': password } }, { upsert: true }, function (err, data) {
        if (err) {
            res.json({ error: true, message: "Something went wrong" });
        } else {
            res.json({ error: false, message: "Success" });
        }
    })
};

exports.orderList = function (req, res) {
    console.log("orderList");
    condition = {};
    let number = req.body.number;
    console.log("number: ", number)
    let query = number * 10;
    typeWay = req.body.typeWay;
    FilterOrder = req.body.FilterOrder;
    typeCancel = req.body.typeCancel;
    if (typeCancel != "" && FilterOrder != "") {
        condition = {
            $and: [
                { "status": typeCancel },
                { "display_id": FilterOrder }
            ]
        }
    } else if (typeCancel != "" && FilterOrder == "") {
        condition = {
            $and: [
                { "status": typeCancel }
            ]
        }
    } else if (typeCancel == "") {

        if (typeWay != "" && FilterOrder != "") {
            condition = {
                $and: [
                    { "payment_method": typeWay },
                    { "display_id": FilterOrder },
                    {
                        $or: [
                            { "status": "accepted" },
                            { "status": "pending" },
                            { "status": "delivery confirmed" },
                        ]
                    }
                ]
            }
        } else
            if (typeWay != "" && FilterOrder == "") {
                condition = {
                    $and: [
                        { "payment_method": typeWay },
                        {
                            $or: [
                                { "status": "accepted" },
                                { "status": "pending" },
                                { "status": "delivery confirmed" },
                            ]
                        }
                    ]
                }
            } else if (typeWay == "" && FilterOrder != "") {
                condition = {
                    $and: [
                        { "display_id": FilterOrder },
                        {
                            $or: [
                                { "status": "accepted" },
                                { "status": "pending" },
                                { "status": "delivery confirmed" },
                            ]
                        }
                    ]
                }
            } else if (typeWay == "" && FilterOrder == "") {
                condition = {
                    $or: [
                        { "status": "accepted" },
                        { "status": "pending" },
                        { "status": "delivery confirmed" },
                    ]
                }
            }
    }
    Order.aggregate([{ $match: condition },
    { $lookup: { from: 'users', localField: 'seller_id', foreignField: '_id', as: 'sellerDetails' } },
    { $lookup: { from: 'countries', localField: 'sellerDetails.country_code', foreignField: 'countryCode', as: 'sellerCountryDetails' } },
    { $sort: { "created_at": -1 } }, { "$skip": query }, { "$limit": 10 }, { "$project": { sellerDetails: 0 } }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: "Data not found" });
        } else {
            res.json({ error: false, message: data });
        }
    })
};

exports.orderDetail = function (req, res) {
    console.log("orderDetail");
    id = req.body.id;

    Order.aggregate([{ $match: { "_id": ObjectId(id) } }], function (err, typeCheckData) {
        if (err) {
            res.json({ error: true, message: error });
        } else if (!typeCheckData) {
            res.json({ error: true, message: "Data not found" });
        } else {
            let type;
            if (typeCheckData.length > 0) {
                for (let i = 0; i < typeCheckData.length; i++) {
                    type = typeCheckData[i].type;
                    if (type == "post") { type = "allposts"; } else if (type == "story") { type = "stories"; }

                    Order.aggregate([{ $match: { "_id": ObjectId(id) } },
                    { $lookup: { from: 'users', localField: 'seller_id', foreignField: '_id', as: 'sellerDetails' } },
                    { $lookup: { from: 'addressinfos', localField: 'user_id', foreignField: 'user_id', as: 'addressArray' } },
                    { $lookup: { from: type, localField: 'type_id', foreignField: '_id', as: 'imageDetails' } },
                    { $lookup: { from: 'paymentrequests', localField: '_id', foreignField: 'order_id', as: 'paymentReqDetails' } }
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, message: error });
                        } else if (!data) {
                            res.json({ error: true, message: "Data not found" });
                        } else {
                            res.json({ error: false, message: data });
                        }
                    })

                }
            } else {
                res.json({ error: true, message: [] });
            }
        }

    })
};

exports.postDetail = function (req, res) {
    console.log("postDetail");
    id = req.body.id;
    Posts.aggregate([{ $match: { "_id": ObjectId(id) } },
    { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'sellerDetail' } }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error })
        } else if (data.length == 0) {
            res.json({ error: true, message: "Data not found", description: [] })
        } else {
            res.json({ error: false, message: "Done", description: data })
        }

    })
};

exports.orderCount = function (req, res) {
    console.log("orderCount");
    Order.aggregate([{ $count: "Counting" }], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: "Data not found" });
        } else {
            res.json({ error: false, message: data });
        }
    })
};

exports.storyDetail = function (req, res) {
    console.log("storyDetail");
    id = req.body.id;
    Story.aggregate([{ $match: { "_id": ObjectId(id) } },
    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetail' } }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: "Data not found" });
        } else {
            res.json({ error: false, message: data });
        }
    })
};

exports.adminBlock = function (req, res) {
    console.log("adminBlock");
    admin_id = req.body.admin_id;
    id = req.body.id;
    is_block = req.body.is_block;
    if (is_block == false) { isBlock = false } else if (is_block == true) { isBlock = true }
    User.updateOne({ _id: ObjectId(id) }, { $set: { 'is_admin_block': isBlock } }, function (err, data) {
        if (err) {
            res.json({ error: true, message: 'something went wrong' });
        } else if (!data) {
            res.json({ error: true, message: 'No Data' });
        } else {
            if (is_block == true) {
                if (data.socket_id != undefined) {
                    io.to(data.socket_id).emit('isAdminBlock', 'true');
                } else {
                    // Funn.adminNotification(admin_id, id, "blocked you", "Blocked by Admin", data);
                }
            }
            res.json({ error: false, message: 'Success' })
        }
    })
};

exports.unReportUser = function (req, res) {
    console.log("unReportUser");
    reported_id = req.body.reported_id;
    reported_user_id = req.body.reported_user_id;
    check = req.body.check;
    if (check == 'all') {
        ReportUser.deleteMany({ reported_user_id: ObjectId(reported_user_id) }, function (err, data) {
            if (err) {
                res.json({ error: true, message: 'something went wrong' });
            } else if (!data) {
                res.json({ error: true, message: 'No Data' });
            } else {
                res.json({ error: false, message: 'Success' })
            }
        })
    } else {
        ReportUser.deleteOne({ _id: ObjectId(reported_id) }, function (err, data) {
            if (err) {
                res.json({ error: true, message: 'something went wrong' });
            } else if (!data) {
                res.json({ error: true, message: 'No Data' });
            } else {
                res.json({ error: false, message: 'Success' })
            }
        })
    }
};

exports.approvedStoryList = function (req, res) {
    console.log("approvedPostList");
    let postList = [];
    let number = req.body.number;
    let query = number * 10;
    Story.aggregate([{ $match: { 'bidsAndComments.status': 'approved' } },
    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } },
    { $lookup: { from: 'countries', localField: 'userDetails.country_code', foreignField: 'countryCode', as: 'userCountryDetails' } },
    { "$skip": query }, { "$limit": 10 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: "Data not found" });
        } else {
            res.json({ error: false, message: data });
        }
    })
};

exports.userListRegex = function (req, res) {
    console.log("userListRegex");
    let word = req.body.word;
    let number = req.body.number;
    let query = number * 15;
    User.find({ username: { $regex: ".*" + word + ".*", $options: 'si' }, is_admin_block: false }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    }).skip(query).limit(15)
};

exports.deviceListRegex = function (req, res) {
    console.log("deviceListRegex");
    let word = req.body.word;
    let number = req.body.number;
    let query = number * 15;
    deviceType = req.body.name;
    User.find({ username: { $regex: ".*" + word + ".*", $options: 'si' }, user_type: deviceType, is_admin_block: false }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    }).skip(query).limit(15)
};

exports.userListVerified = function (req, res) {
    console.log("userListVerified");
    let number = req.body.number;
    let query = number * 15;
    User.find({ is_verified: true }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    }).skip(query).limit(15)
};

exports.userListVerifiedRegex = function (req, res) {
    console.log("userListVerifiedRegex");
    let word = req.body.word;
    let number = req.body.number;
    let query = number * 15;
    User.find({ username: { $regex: ".*" + word + ".*", $options: 'si' }, is_verified: true }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    }).skip(query).limit(15)
};

exports.analyseDailyPost = function (req, res) {
    console.log("AnalyseDailyPost");
    var todayDate = new Date();
    let lesserDynDate = new Date();
    let greaterDynDate = new Date();
    value = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    array_made = [];
    check_arrange = [];
    finalize = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    value.forEach(function (i) {
        lesserDynDate.setDate(todayDate.getDate() - i);
        if (i == 0) {
            // console.log("unique");
            greaterDynDate.setDate(todayDate.getDate() + 1);
        } else {
            let flag = i - 1;
            // console.log("i>>", i);
            // console.log("flag>>", flag);
            greaterDynDate.setDate(todayDate.getDate() - flag);
        }
        // greaterDynDate.setDate(todayDate.getDate() - flag);
        // console.log(lesserDynDate + ">>>>>" + greaterDynDate);
        lesserDynDate = lesserDynDate.toISOString().slice(0, 11);
        lesserDynDate = lesserDynDate + "00:00:00.000Z";
        greaterDynDate = greaterDynDate.toISOString().slice(0, 11);
        greaterDynDate = greaterDynDate + "00:00:00.000Z";
        // console.log(lesserDynDate + ">>>>>" + greaterDynDate);
        lesserDynDate = new Date(lesserDynDate);
        greaterDynDate = new Date(greaterDynDate);
        lesserDynDate = lesserDynDate.getTime();
        greaterDynDate = greaterDynDate.getTime();
        // console.log(lesserDynDate + ">>>>>" + greaterDynDate);
        if (lesserDynDate < greaterDynDate) {
            console.log("lessesDynDate")
        } else {
            console.log("greaterDynDate")
        }
        var z = new Date(lesserDynDate);
        zdate = z.getDate();
        check_arrange.push(zdate);
        // console.log(check_arrange);
        gettingDayPostCount(req, res, lesserDynDate, greaterDynDate, (ok, mtime) => {
            console.log("activity")
            // console.log(mtime);
            var d = new Date(mtime);
            date = d.getDate();
            look = { 'date': date, 'count': ok };
            array_made.push(look);
            // console.log(array_made, "json");
            // console.log(check_arrange);
            for (len = 0; len < array_made.length; len++) {
                for (per = 0; per < check_arrange.length; per++) {
                    // console.log(array_made[len]['date'], "----------------------", check_arrange[per]);
                    if (array_made[len]['date'] == check_arrange[per]) {
                        finalize[per] = array_made[len];
                    }
                }
            }
            if (len == 12) { res.json({ error: false, data: finalize }); }
            // console.log(finalize, "finalize");

        })
        lesserDynDate = new Date();
        greaterDynDate = new Date();

    });
};

function gettingDayPostCount(req, res, lesserDynDate, greaterDynDate, done) {
    console.log("gettingDayPostCount")
    // console.log("hello", lesserDynDate);
    lesserDynDate = lesserDynDate;
    greaterDynDate = greaterDynDate;
    Post.aggregate([{ $match: { 'post_created_at': { $gte: lesserDynDate, $lt: greaterDynDate } } }], function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            console.log("nodata", data.length);
            done(data.length, lesserDynDate);
        } else {
            // console.log("answers", lesserDynDate);
            // console.log(lesserDynDate + ">>>>>" + greaterDynDate);
            // console.log("data", data.length);
            done(data.length, lesserDynDate);
        }
    })
};

exports.userListAll = function (req, res) {
    console.log("userListAll");
    User.find(function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    })
};

exports.userDetailId = function (req, res) {
    console.log("userDetailId")
    let user_id = ObjectId(req.body.user_id);
    User.aggregate([{ $match: { _id: user_id } },
    { $lookup: { from: 'users', localField: 'followers.user_id', foreignField: '_id', as: 'followers' } },
    { $lookup: { from: 'users', localField: 'following.user_id', foreignField: '_id', as: 'following' } },
    { $lookup: { from: 'countries', localField: 'country_code', foreignField: 'countryCode', as: 'countryDetails' } }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting details of a user.", description: error });
        } else if (data.length == 0) {
            res.json({ error: false, message: "No data found." });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    })
};

exports.adminChangesYourPassword = function (req, res) {
    console.log("adminChangesYourPassword");
    user_id = req.body.user_id;
    password = req.body.password;
    User.findOneAndUpdate({ _id: ObjectId(user_id) }, { password: password }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while changing pasword by user", description: error });
        } else if (!data) {
            res.json({ error: true, message: "Data not found" });
        } else {
            res.json({ error: false, message: "Password changed" })
        }
    })
};

exports.givingPostCountAll = function (req, res) {
    console.log("givingPostCountAll");
    let user_id = req.body.user_id;
    Posts.aggregate([{ $match: { post_user_id: ObjectId(user_id) } },
    { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
    { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
    { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
    { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
    ], function (err, data) {
        if (err) {
            res.json({ error: true, message: "Something went wrong.", data: err });
        } else if (!data) {
            res.json({ error: false, message: 'No data found, user id might not be matched' })
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    })
};

// ----------------------------------------------------------
exports.verificationList = function (req, res) {
    console.log("verificationList");
    let number = req.body.number;
    let query = number * 15;
    User.aggregate([{ $match: { $or: [{ 'verification.status': 'pending' }, { 'verification.payment_status': 'pending' }] } },
    { $sort: { "created_at": -1 } }, { "$skip": query }, { "$limit": 15 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting pending verified users list", description: error });
        } else if (data.length == 0) {
            res.json({ error: true, message: "No user found", description: [] });
        } else {
            res.json({ error: false, message: "Done", description: data })
        }
    })
};

exports.verifyUser = function (req, res) {
    console.log("verifyUser");
    let user_id = req.body.user_id,
        status = req.body.status,
        admin_id = req.body.admin_id;

    User.findOneAndUpdate({ _id: ObjectId(user_id) }, { 'verification.status': status }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while updating verification status of user", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No user found" });
        } else {
            Fun.userDetails(user_id, (userData) => {
                res.json(userData);
                if (status == "approved") {
                    Fun.adminNotification(admin_id, user_id, "approved your documents", "Document verification", "");
                    Rou.emitToUser(userData.description.socket_id, userData.description.verification)
                } else {
                    User.findOneAndUpdate({ _id: ObjectId(user_id) }, { 'verification': {} }, function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while updating verification status of user", description: error });
                        } else if (!data) {
                            res.json({ error: true, message: "No user found" });
                        } else {
                            Fun.adminNotification(admin_id, user_id, "denied your verification of document", "Document verification", "")
                        }
                    })
                    // Fun.adminNotification(admin_id, user_id, "denied your verification of document", "Document verification", "")

                }
            })
        }
    })
};
// ---------------------------------------------------------------------

exports.tags = function (req, res) { //    TO CRUD TAGS IN DATABASE
    // authenticate(req, res, () => {
    console.log("tags");
    let tag_id = req.body.tag_id;
    let type = req.body.type;
    let name = req.body.name;
    let createdAt = new Date();
    let updatedAt = new Date();
    switch (type) {
        case 'create':

            let tag = new Tag();
            tag.name = name;
            tag.createdAt = createdAt;
            tag.updatedAt = updatedAt;

            Tag.findOne({ "name": tag.name }, function (err, data) {
                if (err) {
                    res.json({ error: true, message: 'Error while storing tags into database.', description: err });
                } else if (!data) {
                    tag.save(function (error, data) {
                        if (error) {
                            res.json({ error: true, message: 'Error while storing tags into database.', description: error });
                        } else {
                            res.json({ error: false, message: 'Done', description: data })
                        }
                    })
                } else {
                    res.json({ error: true, message: 'Already Exist', description: data })
                }
            })

            break;

        case 'read':
            Tag.find({ "isDeleted": false }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while fetching tags from database.", description: error });
                } else if (data.length == 0) {
                    res.json({ error: false, message: "No data found" })
                } else {
                    res.json({ error: false, message: "Done", description: data });
                }
            })

            break;

        case 'update':
            Tag.findOne({ "name": name }, function (err, data) {
                if (err) {
                    res.json({ error: true, message: 'No tag found.', description: err });
                } else if (!data) {
                    Tag.updateOne({ _id: ObjectId(tag_id) }, { $set: { "name": name } }, function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting tags details", description: error });
                        } else if (!data) {
                            res.json({ error: false, message: "No tag found" });
                        } else {
                            res.json({ error: false, message: "Tag updated" });
                        }
                    })
                } else {
                    res.json({ error: true, message: 'Already Exist', description: data })
                }
            })
            break;

        case 'delete':
            Tag.updateOne({ _id: ObjectId(tag_id) }, { $set: { "isDeleted": true } }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting tags details", description: error });
                } else if (!data) {
                    res.json({ error: false, message: "No tag found" });
                } else {
                    res.json({ error: false, message: "Tag deleted" });
                }
            })
            break;

        case 'readDeleted':
            Tag.find({ "isDeleted": true }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while fetching tags from database.", description: error });
                } else if (data.length == 0) {
                    res.json({ error: false, message: "No data found" })
                } else {
                    res.json({ error: false, message: "Done", description: data });
                }
            })
            break;
        case 'undo':

            Tag.updateOne({ _id: ObjectId(tag_id) }, { $set: { "isDeleted": false } }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting tags details", description: error });
                } else if (!data) {
                    res.json({ error: false, message: "No tag found" });
                } else {
                    res.json({ error: false, message: "Tag Undo" });
                }
            })
            break;
    }
    // })
};

exports.updateAllCount = function (req, res) {
    // Fun.authenticate(req, res, () => {
    console.log("updateAllCount");
    let user_id = req.body.user_id,
        count = req.body.count,
        type = req.body.type;
    Fun.userDetails(user_id, (userData) => {
        let data = userData.description,
            newCount;
        if (type == "post") {
            newCount = parseInt(data.unlimited_post_count.count) + parseInt(count);
            let unObj = { status: data.unlimited_post_count.status, activation: data.unlimited_post_count.activation, expiry: data.unlimited_post_count.expiry, count: newCount }
            let obj = { unlimited_post_count: unObj }
            Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                res.json(yes)
            });
        } else if (type == "story") {
            newCount = parseInt(data.unlimited_story_count.count) + parseInt(count);
            let unObj = { status: data.unlimited_story_count.status, activation: data.unlimited_story_count.activation, expiry: data.unlimited_story_count.expiry, count: newCount }
            let obj = { unlimited_story_count: unObj }
            Fun.update_story_count(user_id, obj, (yes) => {
                res.json(yes)
            });
        } else if (type == "live") {
            newCount = parseInt(data.unlimited_live_count.count) + parseInt(count);
            let unObj = { status: data.unlimited_live_count.status, activation: data.unlimited_live_count.activation, expiry: data.unlimited_live_count.expiry, count: newCount }
            let obj = { unlimited_live_count: unObj }
            Fun.update_live_count(user_id, obj, (yes) => {
                res.json(yes)
            });
        }
    })
    // })
};

// pass payment

exports.PassPaymentFromAdmin = function (req, res) {
    console.log("PassPaymentFromAdmin");
    payement_user_id = req.body.payement_user_id;
    message = req.body.message;
    PaymentRequests.updateOne({ _id: ObjectId(payement_user_id) }, { $set: { 'status': "completed", "message": message } }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Something went wrong", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data Found" });
        } else {
            PaymentRequests.findOne({ _id: ObjectId(payement_user_id) }, function (err, details) {
                if (err) {
                    res.json({ error: true, message: "Something went wrong", description: err });
                } else if (!details) {
                    res.json({ error: true, message: "No data Found" });
                } else {
                    res.json({ error: false, message: details });
                }
            })
        }
    })
}

exports.ChangePaymentStatus = function (req, res) {
    console.log("ChangePaymentStatus");
    let order_id = req.body.order_id,
        status = req.body.status;
    Order.findOneAndUpdate({ _id: ObjectId(order_id) }, { user_payment_status: "completed" }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Something went wrong", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data Found" });
        } else {
            Order.findOne({ _id: ObjectId(order_id) }, function (error, updateData) {
                if (error) {
                    res.json({ error: true, message: "Something went wrong", description: error });
                } else if (!updateData) {
                    res.json({ error: true, message: "No data Found" });
                } else {
                    res.json({ error: false, message: updateData });
                }
            })
        }
    })
}

exports.allStoryA = function (req, res) {
    console.log("allStoryA");
    let number = req.body.number;
    let query = number * 10;
    Story.aggregate([{ $match: { "status": "running" } },
    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } },
    { $sort: { "created_at": -1 } }, { "$skip": query }, { "$limit": 10 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            res.json({ error: false, message: data });
        }
    })
};

exports.setBadge = function (req, res) {
    console.log('setBadge');
    isBadge = req.body.isBadge;
    id = req.body.id;
    User.updateOne({ _id: ObjectId(id) }, { $set: { 'is_verified': isBadge } }, function (err, data) {
        if (err) {
            res.json({ error: true, message: err });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            res.json({ error: false, message: data });
        }
    })
}

exports.reportPostList = function (req, res) {
    console.log('reportPostList');
    let number = req.body.number;
    let query = number * 10;
    ReportPost.aggregate([
        { $sort: { created_at: 1 } },
        { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'user_id' } },
        {
            $group: { _id: "$post_id", count: { $sum: 1 }, userDetails: { $push: '$$ROOT' } }
        },
        { $lookup: { from: "allposts", localField: "_id", foreignField: "_id", as: "postDetail" } },
        { $lookup: { from: 'users', localField: 'postDetail.post_user_id', foreignField: '_id', as: 'ReportedUserPost' } },
        { "$skip": query }, { "$limit": 10 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            res.json({ error: false, message: data });
        }
    });
}

exports.deleteReportedPost = function (req, res) {
    console.log('deleteReportedPost');
    let post_id = req.body.post_id;
    let reported_id = req.body.reported_id;
    Post.updateOne({ _id: ObjectId(post_id) }, { $set: { "is_deleted": true } }, function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            ReportPost.deleteMany({ post_id: ObjectId(post_id) }, function (err, data) {
                if (err) {
                    res.json({ error: true, message: err });
                } else if (!data) {
                    res.json({ error: true, message: [] });
                } else {
                    res.json({ error: false, message: "Success" });
                }
            })
        }
    })
}

exports.discardReportedPost = function (req, res) {
    console.log('discardReportedPost');
    let post_id = req.body.post_id;
    let check = req.body.check;
    let reported_id = req.body.reported_id
    console.log(check);
    if (check == 'all') {
        ReportPost.deleteMany({ post_id: ObjectId(post_id) }, function (err, data) {
            if (err) {
                res.json({ error: true, message: err });
            } else if (!data) {
                res.json({ error: true, message: [] });
            } else {
                res.json({ error: false, message: "Success" });
            }
        })
    } else {
        ReportPost.deleteOne({ _id: ObjectId(reported_id) }, function (err, data) {
            if (err) {
                res.json({ error: true, message: err });
            } else if (!data) {
                res.json({ error: true, message: [] });
            } else {
                res.json({ error: false, message: "Success" });
            }
        })
    }
}

exports.reportStoryList = function (req, res) {
    console.log('reportStoryList');
    let number = req.body.number;
    let query = number * 10;
    ReportStory.aggregate([
        { $sort: { created_at: 1 } },
        { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'user_id' } },
        {
            $group: { _id: "$story_id", count: { $sum: 1 }, userDetails: { $push: '$$ROOT' } }
        },
        { $lookup: { from: "stories", localField: "_id", foreignField: "_id", as: "storyDetail" } },
        { $lookup: { from: 'users', localField: 'storyDetail.user_id', foreignField: '_id', as: 'ReportedUserStory' } },
        { "$skip": query }, { "$limit": 10 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            res.json({ error: false, message: data });
        }
    });
}

exports.deleteReportedStory = function (req, res) {
    console.log('deleteReportedStory');
    let story_id = req.body.story_id;
    let reported_id = req.body.reported_id;
    Story.updateOne({ _id: ObjectId(story_id) }, { $set: { "status": "stop" } }, function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            ReportStory.deleteMany({ story_id: ObjectId(story_id) }, function (err, data) {
                if (err) {
                    res.json({ error: true, message: err });
                } else if (!data) {
                    res.json({ error: true, message: [] });
                } else {
                    res.json({ error: false, message: "Success" });
                }
            })
        }
    })
}

exports.discardReportedStory = function (req, res) {
    console.log('discardReportedStory');
    let story_id = req.body.story_id;
    let check = req.body.check;
    let reported_id = req.body.reported_id
    if (check == 'all') {
        ReportStory.deleteMany({ story_id: ObjectId(story_id) }, function (err, data) {
            if (err) {
                res.json({ error: true, message: err });
            } else if (!data) {
                res.json({ error: true, message: [] });
            } else {
                res.json({ error: false, message: "Success" });
            }
        })
    } else {
        ReportStory.deleteOne({ _id: ObjectId(reported_id) }, function (err, data) {
            if (err) {
                res.json({ error: true, message: err });
            } else if (!data) {
                res.json({ error: true, message: [] });
            } else {
                res.json({ error: false, message: "Success" });
            }
        })
    }
}

exports.removeProfilePhoto = function (req, res) {
    console.log("removeProfilePhoto");
    user_id = req.body.user_id;
    User.updateOne({ _id: ObjectId(user_id) }, { $set: { "profilePhoto": "" } }, function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            res.json({ error: false, message: "Success" });
        }
    })
}

exports.userStories = function (req, res) {
    console.log("userStories");
    user_id = req.body.user_id;
    let number = req.body.number;
    let query = number * 10;
    Story.aggregate([{ $match: { "user_id": ObjectId(user_id) } },
    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } },
    { $sort: { "created_at": -1 } }, { "$skip": query }, { "$limit": 10 }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            res.json({ error: false, message: data });
        }
    })
}

exports.userStoriesCount = function (req, res) {
    console.log("userStoriesCount");
    user_id = req.body.user_id;
    Story.aggregate([{ $group: { _id: "$user_id", count: { $sum: 1 } } },], function (error, data) {
        if (error) {
            res.json({ error: true, message: error });
        } else if (!data) {
            res.json({ error: true, message: [] });
        } else {
            res.json({ error: false, message: data });
        }
    })
}

exports.userBlockListRegex = function (req, res) {
    console.log("userBlockListRegex");
    let word = req.body.word;
    let number = req.body.number;
    let query = number * 15;
    User.find({ username: { $regex: ".*" + word + ".*", $options: 'si' }, is_admin_block: true }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all users detail", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data });
        }
    }).skip(query).limit(15)
};

// *******************************************************************************