import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';
import { ActivatedRoute,Router,ParamMap } from "@angular/router";

@Component({
  selector: 'app-admin-password',
  templateUrl: './admin-password.component.html',
  styleUrls: ['./admin-password.component.css']
})
export class AdminPasswordComponent implements OnInit {

  adminEmail=sessionStorage.getItem('adminEmail');
  password;
  confirm;
  toggleSuccess:boolean;
  toggleError:boolean;
  toggleCharError:boolean;
  constructor(private route: ActivatedRoute,private router: Router,private all:AllService) { 
    this.all.changeLoader(false);
  }

  ngOnInit() {
    if(sessionStorage.getItem('admin-password')!="true")
    {
      this.router.navigate(['confirm-otp'])
    }
  }

  onSubmit()
  {
    this.all.changeLoader(true)
    if(this.password == this.confirm && this.password.length > 7)
    {
      this.all.changePasswordAdmin(this.adminEmail,this.password).subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        if(result.message == "Success")
        {
          this.toggleSuccess = true;
          this.toggleError = false;
          this.toggleCharError=false;
          sessionStorage.removeItem('adminEmail');
          this.adminEmail="";
          sessionStorage.removeItem('confirm-otp');
          sessionStorage.removeItem('admin-password');
          this.router.navigate(['dashboard'])
        }else{
          this.toggleSuccess = false;
          this.toggleError = true;
          this.toggleCharError=false;
        }
        this.all.changeLoader(false)
      })
    }else{
      this.toggleCharError=true;
      this.all.changeLoader(false)
    }
  }

}
