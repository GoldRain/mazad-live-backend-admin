import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Constants } from "./constants";
import { retry } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AllService {

  loggedInStatus = JSON.parse(localStorage.getItem("loggedIn") || "false");
  
  private toggleLoader = new BehaviorSubject(false);
  currentLoader = this.toggleLoader.asObservable();

  private ImageSource = new BehaviorSubject("");
  currentImage = this.ImageSource.asObservable();

  private StorySource = new BehaviorSubject("");
  currentStory = this.StorySource.asObservable();

  private toggleLogin = new BehaviorSubject(
    JSON.parse(
      localStorage.getItem("loggedIn") || this.loggedInStatus.toString()
    )
  );
  currentLogin = this.toggleLogin.asObservable();
  
  changeLoader(value){
    this.toggleLoader.next(value);
  }

  changeImage(Image) {
    this.ImageSource.next(Image);
  }

  changeStory(Story) {
    this.StorySource.next(Story);
  }

  setLoggedIn(value: boolean) {
    this.loggedInStatus = value;
    this.toggleLogin.next(value);
    localStorage.setItem("loggedIn", "true");
  }

  get isLoggedIn() {
    return JSON.parse(
      localStorage.getItem("loggedIn") || this.loggedInStatus.toString()
    );
  }

  logout(value) {
    this.loggedInStatus = value;
    this.toggleLogin.next(value);
    localStorage.setItem("loggedIn", "false");
    localStorage.setItem("admin_id", "");
    // localStorage.removeItem('NavItem');
  }

  constructor(private http: HttpClient, private constant: Constants) { }
  port = this.constant.getPort();

  version = this.constant.getVersion();

  httpOptions = new HttpHeaders({
    "Content-Type": "application/json"
  });

  userList(page) {
    console.log(page);
    return this.http.post( this.port + "/userListA", { version: this.version, number: page }, { headers: this.httpOptions } );
  }

  // userDetail
  userDetail(id) {
    return this.http.post(
      this.port + "/userDetailIdA",
      { version: this.version, user_id: id },
      { headers: this.httpOptions }
    );
  }

  // For Post Number
  getPostNo(id) {
    return this.http.post(
      this.port + "/givingPostCountAllA",
      { version: this.version, user_id: id },
      { headers: this.httpOptions }
    );
  }

  // change password of user
  changeUserPassword(id, password, admin) {
    return this.http.post(
      this.port + "/adminChangesYourPasswordA",
      { user_id: id, password: password },
      { headers: this.httpOptions }
    );
  }

  // user Number for dashboard
  seeUserNumber() {
    return this.http.post(
      this.port + "/userListAllA",
      { version: this.version },
      { headers: this.httpOptions }
    );
  }

  // totalDeviceList
  totalDeviceList(deviceType) {
    return this.http.post(
      this.port + "/totalDeviceListA",
      { deviceType: deviceType },
      { headers: this.httpOptions }
    );
  }

  // deviceList
  deviceList(number, name) {
    return this.http.post(
      this.port + "/deviceListA",
      { number: number, name: name },
      { headers: this.httpOptions }
    );
  }

  // All post
  allPost(number) {
    return this.http.post(
      this.port + "/allPostsA",
      { number: number },
      { headers: this.httpOptions }
    );
  }

  // user post
  userPost(id, number) {
    return this.http.post(
      this.port + "/getMyPostA",
      { user_id: id, number: number },
      { headers: this.httpOptions }
    );
  }

  // reportList
  reportList(number) {
    return this.http.post(
      this.port + "/reportListA",
      { number: number },
      { headers: this.httpOptions }
    );
  }

  // reportUser
  reportUser(id) {
    return this.http.post(
      this.port + "/reportUserDetailsA",
      { reported_user_id: id },
      { headers: this.httpOptions }
    );
  }

  // approved list
  approvedList(number) {
    return this.http.post(
      this.port + "/approvedPostListA",
      { number: number },
      { headers: this.httpOptions }
    );
  }

  // RequestToAdmin by seller
  requestList(number) {
    return this.http.post(
      this.port + "/requestedToAdminA",
      { number: number },
      { headers: this.httpOptions }
    );
  }

  // loginAdmin
  loginAdmin(email, password) {
    return this.http.post(
      this.port + "/adminLoginA",
      { email: email, password: password },
      { headers: this.httpOptions }
    );
  }

  // PostCount
  postCount() {
    return this.http.post(
      this.port + "/allPostA",
      {},
      { headers: this.httpOptions }
    );
  }

  // countRequest
  countRequest() {
    return this.http.post(
      this.port + "/countRequestA",
      {},
      { headers: this.httpOptions }
    );
  }

  sendEmail(email) {
    return this.http.post(
      this.port + "/sendMailA",
      { email: email },
      { headers: this.httpOptions }
    );
  }

  verifyAdminOtp(email, otp) {
    return this.http.post(
      this.port + "/verifyAdminOtpA",
      { email: email, otp: otp },
      { headers: this.httpOptions }
    );
  }

  changePasswordAdmin(email, password) {
    return this.http.post(
      this.port + "/UpdateAdminPasswordA",
      { email: email, password: password },
      { headers: this.httpOptions }
    );
  }

  orderList(number, typeWay, FilterOrder,typeCancel) {
    return this.http.post(
      this.port + "/orderListA",
      { number: number, typeWay: typeWay, FilterOrder: FilterOrder,typeCancel:typeCancel },
      { headers: this.httpOptions }
    );
  }

  orderDetail(id) {
    return this.http.post(
      this.port + "/orderDetailA",
      { id: id },
      { headers: this.httpOptions }
    );
  }

  postDetail(id): Observable<any> {
    return this.http.post(
      this.port + "/postDetailA",
      { id: id },
      { headers: this.httpOptions }
    );
  }

  orderCount() {
    return this.http.post(
      this.port + "/orderCountA",
      {},
      { headers: this.httpOptions }
    );
  }

  storyDetail(id) {
    return this.http.post(
      this.port + "/storyDetailA",
      { id: id },
      { headers: this.httpOptions }
    );
  }

  adminBlock(admin_id,id, check) {
    return this.http.post(
      this.port + "/adminBlockA",
      { admin_id: admin_id, id: id, is_block: check },
      { headers: this.httpOptions }
    );
  }

  unReportUser(check,reported_id,reported_user_id){
    return this.http.post(
      this.port + "/unReportUserA",
      {check:check,reported_id:reported_id,reported_user_id:reported_user_id},
      {headers:this.httpOptions}
    )
  }

  approvedStoryList(page){
    return this.http.post(
      this.port + '/approvedStoryListA',
      { number:page },
      {headers:this.httpOptions}
    )
  }

  userListRegex(word,page){
    return this.http.post(
      this.port + '/userListRegexA',
      {word:word,number:page},
      {headers:this.httpOptions}
    )
  }

  deviceListRegex(word,page,name){
    return this.http.post(
      this.port + '/deviceListRegexA',
      {word:word,number:page,name:name},
      {headers:this.httpOptions}
    )
  }

  userListVerified(page){
    return this.http.post(
      this.port + '/userListVerifiedA',
      {number:page},
      {headers:this.httpOptions}
    )
  }

  userListVerifiedRegex(word,page){
    return this.http.post(
      this.port + '/userListVerifiedRegexA',
      {word:word,number:page},
      {headers:this.httpOptions}
    )
  }

  analyseDailyPost(){
    return this.http.post(
      this.port + '/analyseDailyPostA',
      {},
      {headers:this.httpOptions}
    )
  }

  // list for action verification
  verificationList(page){
    return this.http.post(
      this.port + '/verificationListA',
      {number:page},
      {headers:this.httpOptions}
    )
  }

  // action on single verification
  verificationAction(user_id,status,admin_id){
    return this.http.post(
      this.port + '/verifyUserA',
      {user_id:user_id,status:status,admin_id},
      {headers:this.httpOptions}
    )
  }

  // tags
  Tags(tag_id,type,name){
    return this.http.post(
      this.port + '/tagsA',
      {tag_id:tag_id,type:type,name:name},
      {headers:this.httpOptions}
    )
  }

  UpdateCount(user_id,count,type){
    return this.http.post(
      this.port + '/updateCountA',
      {user_id:user_id,count:count,type:type},
      {headers:this.httpOptions}
    )
  }

  PassPaymentFromAdmin(payement_user_id,message){
    return this.http.post(
      this.port + '/PassPaymentFromAdminA',
      {payement_user_id:payement_user_id,message:message},
      {headers:this.httpOptions}
    )
  }

  ChangePaymentStatus(order_id){
    return this.http.post(
      this.port + '/ChangePaymentStatusA',
      {order_id:order_id},
      {headers:this.httpOptions}
    )
  }
  
  AllStory(number){
    return this.http.post(
      this.port + '/AllStoryA',
      {number:number},
      {headers:this.httpOptions}
    )
  }

  SetBadge(id,isBadge){
    return this.http.post(
      this.port + '/setBadgeA',
      {id:id,isBadge:isBadge},
      {headers:this.httpOptions}
    )
  }

  reportedPostList(number){
    return this.http.post(
      this.port + '/reportPostListA',
      {number:number},
      {headers:this.httpOptions}
    )
  }

  deleteReportedPost(post_id,reported_id){
    return this.http.post(
      this.port + '/deleteReportedPostA',
      {post_id:post_id,reported_id:reported_id},
      {headers:this.httpOptions}
    )
  }

  discardReportedPost(post_id,reported_id,check){
    return this.http.post(
      this.port + '/discardReportedPostA',
      {post_id:post_id,reported_id:reported_id,check:check},
      {headers:this.httpOptions}
    )
  }

  reportedStoryList(number){
    return this.http.post(
      this.port + '/reportStoryListA',
      {number:number},
      {headers:this.httpOptions}
    )
  }

  deleteReportedStory(story_id,reported_id){
    return this.http.post(
      this.port + '/deleteReportedStoryA',
      {story_id:story_id,reported_id:reported_id},
      {headers:this.httpOptions}
    )
  }

  discardReportedStory(story_id,reported_id,check){
    return this.http.post(
      this.port + '/discardReportedStoryA',
      {story_id:story_id,reported_id:reported_id,check:check},
      {headers:this.httpOptions}
    )
  }

  removeProfilePhoto(user_id){
    return this.http.post(
      this.port + '/removeProfilePhotoA',
      {user_id:user_id},
      {headers:this.httpOptions}
    )
  }

  userStories(user_id,number){
    return this.http.post(
      this.port + '/userStoriesA',
      {user_id:user_id,number:number},
      {headers:this.httpOptions}
    )
  }

  userStoriesCount(user_id){
    return this.http.post(
      this.port + '/userStoriesCountA',
      {user_id:user_id},
      {headers:this.httpOptions}
    )
  }

  userBlockListRegex(word,number){
    return this.http.post(
      this.port + '/userBlockListRegexA',
      {word:word,number:number},
      {headers:this.httpOptions}
    )
  }

}
