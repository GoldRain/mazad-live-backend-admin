import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './auth.guard';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { UserVarifiedComponent } from './user-varified/user-varified.component';
import { VerificationComponent } from './verification/verification.component';
import { ReportedUserComponent } from './reported-user/reported-user.component';
import { PostListComponent } from './post-list/post-list.component';
import { RequestComponent } from './request/request.component';
import { ApprovedPostComponent } from './approved-post/approved-post.component';
import { ApprovedStoryComponent } from './approved-story/approved-story.component';
import { OrderListComponent } from './order-list/order-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { TagsComponent } from './tags/tags.component';
import { LoginComponent } from './login/login.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { ConfirmOtpComponent } from './confirm-otp/confirm-otp.component';
import { UserPasswordComponent } from './user-password/user-password.component';
import { ReportedDetailComponent } from './reported-detail/reported-detail.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { AdminPasswordComponent } from './admin-password/admin-password.component';
import { ImagePopUpComponent } from './image-pop-up/image-pop-up.component';
import { StoryPopUpComponent } from './story-pop-up/story-pop-up.component';
import { StoryListComponent } from './story-list/story-list.component';
import { StoryDetailComponent } from './story-detail/story-detail.component';
import { ReportedPostComponent } from './reported-post/reported-post.component';
import { ReportedStoryComponent } from './reported-story/reported-story.component';
import { BlockUserComponent } from './block-user/block-user.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path : 'dashboard',
    component : DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'users',
    component : UsersComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'users/:id',
    component : UsersComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'user-varified',
    component : UserVarifiedComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'verification',
    component : VerificationComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'reported-user',
    component : ReportedUserComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'post-list',
    component : PostListComponent,
    canActivate: [AuthGuard],
    children  : [
      { 
        path : 'pop-up',
        component : ImagePopUpComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path : 'post-list/:id',
    component : PostListComponent,
    canActivate: [AuthGuard],
    children  : [
      { 
        path : 'pop-up',
        component : ImagePopUpComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path : 'request',
    component : RequestComponent,
    canActivate: [AuthGuard],
    children  : [
      { 
        path : 'story-pop-up',
        component : StoryPopUpComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path : 'approved-post',
    component : ApprovedPostComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'approved-story',
    component : ApprovedStoryComponent,
    canActivate: [AuthGuard],
    children  : [
      { 
        path : 'story-pop-up',
        component : StoryPopUpComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path : 'order-list',
    component : OrderListComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'user-detail/:id',
    component : UserDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'post-detail/:id',
    component : PostDetailComponent,
    canActivate: [AuthGuard],
    children  : [
      { 
        path : 'pop-up',
        component : ImagePopUpComponent,
        canActivate: [AuthGuard]
      },
    ]
  },
  {
    path : 'tags',
    component : TagsComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'login',
    component : LoginComponent
  },
  {
    path : 'confirm-email',
    component : ConfirmEmailComponent
  },
  {
    path : 'confirm-email/:name',
    component : ConfirmEmailComponent,
  },
  {
    path : 'confirm-otp',
    component : ConfirmOtpComponent
  },
  {
    path : 'user-password/:id',
    component : UserPasswordComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'reported-detail/:id',
    component : ReportedDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path : 'order-detail/:id',
    component : OrderDetailComponent,
    canActivate: [AuthGuard],
    children  : [
      { 
        path : 'story-pop-up',
        component : StoryPopUpComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path : 'admin-password',
    component : AdminPasswordComponent
  },
  {
    path : 'story-list',
    component : StoryListComponent,
    canActivate : [AuthGuard],
    children : [
      {
        path : 'story-pop-up',
        component : StoryPopUpComponent,
        canActivate : [AuthGuard]
      }
    ]
  },{
    path : 'story-list',
    component : StoryListComponent,
    canActivate : [AuthGuard],
    children : [
      {
        path : 'story-pop-up',
        component : StoryPopUpComponent,
        canActivate : [AuthGuard]
      }
    ]
  },
  {
    path : 'story-list/:id',
    component : StoryListComponent,
    canActivate : [AuthGuard],
    children : [
      {
        path : 'story-pop-up',
        component : StoryPopUpComponent,
        canActivate : [AuthGuard]
      }
    ]
  },
  {
    path : 'story-detail/:id',
    component : StoryDetailComponent,
    canActivate : [AuthGuard],
    children : [
      {
        path : 'story-pop-up',
        component : StoryPopUpComponent,
        canActivate : [AuthGuard]
      }
    ]
  },
  {
    path : 'reported-post',
    component : ReportedPostComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'reported-story',
    component : ReportedStoryComponent,
    canActivate : [AuthGuard],
    children : [
      {
        path : 'story-pop-up',
        component : StoryPopUpComponent,
        canActivate : [AuthGuard]
      }
    ]
  },
  {
    path : 'block-users',
    component : BlockUserComponent,
    canActivate : [AuthGuard]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    // RouterModule.forRoot(routes),
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
