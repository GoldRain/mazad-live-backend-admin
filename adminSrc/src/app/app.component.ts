import { Component, HostListener, ViewChild,OnInit, Inject,AfterViewChecked } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';
import { AllService } from './all.service';
import { Router } from '@angular/router';
import { NbMenuService } from '@nebular/theme';
import { filter, map } from 'rxjs/operators';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit,AfterViewChecked {

  @ViewChild('snav') snav: NbSidebarService;

  title = 'TestingNebular';
  selectedItem;
  constructor(private sidebarService: NbSidebarService,private all:AllService,private router: Router,private nbMenuService: NbMenuService,public breakpointObserver: BreakpointObserver) {
    this.all.changeLoader(true)
  }

  items = [
    { title: 'Change Password' },
    { title: 'Logout' },
  ];

  routLink=[
    '/dashboard',
    '/users',
    '/user-varified',
    '/verification',
    '/block-users',
    '/reported-user',
    '/reported-post',
    '/reported-story',
    '/tags',
    '/post-list',
    '/story-list',
    '/request',
    '/approved-post',
    '/approved-story',
    '/order-list']
  groups=[
    'Dashboard',
    'Users',
    'Verified Users',
    'Verify Request',
    'Blocked Users',
    'Reported Users',
    'Reported Post',
    'Reported Story',
    'Tags',
    'Posts',
    'Stories',
    'Payment Requests',
    'Post Bids',
    'Story Bids',
    'Orders List']
  icons=[
    "icon ion-md-globe",
    "icon ion-md-person",
    "icon ion-md-checkmark-circle-outline",
    "icon ion-md-paper",
    "icon ion-md-alert",
    "icon ion-md-sad",
    "icon ion-md-eye-off",
    "icon ion-md-warning",
    "icon ion-md-sunny",
    "icon ion-md-eye",
    "icon ion-md-film",
    "icon ion-md-wallet",
    "icon ion-md-star",
    "icon ion-md-star-outline",
    "icon ion-md-cart"]

  toggleLogin:boolean = true;
  toggleLoader:boolean = false;

  ngOnInit() {
    console.log("hello again");
    this.all.currentLogin.subscribe(value => {this.toggleLogin = value;})
    this.all.currentLoader.subscribe(value => {this.toggleLoader = value;})
    this.selectedItem = localStorage.getItem('NavItem')
    this.nbMenuService.onItemClick()
    .pipe(
      filter(({ tag }) => tag === 'my-context-menu'),
      map(({ item: { title } }) => title),
    )
    .subscribe(title => {
      if(title == "Change Password"){
        this.router.navigateByUrl('/confirm-email');
      }else if(title=="Logout"){
        this.logout();
      }
      // this.window.alert(`${title} was clicked!`)
    });

    
    this.breakpointObserver
    .observe(['(min-width: 575.98px)'])
    .subscribe((state: BreakpointState) => {
      if(this.toggleLogin==true){
      if (state.matches) {
        console.log("expand")
        this.snav.expand();
        
      } else {
        console.log("collapse")
          this.snav.collapse();
      }
    }
    });
  
  }

  listClick(event, newValue) {
    console.log(newValue);
    this.selectedItem = newValue;
    localStorage.setItem('NavItem',newValue);
  }

  // @HostListener('window:resize', ['$event'])
  // onResize(event) {
  //   console.log("windowresize");
  //   if (event.target.innerWidth < 575.98) {
  //     console.log("collapse");
  //       this.snav.collapse();
  //   }
  //   if (event.target.innerWidth > 575.98 ){
  //     console.log("expand");
  //     this.snav.expand();
  //   }
  // }

  sideClickToggle(){
    let width=window.innerWidth;
     if(width < 575.98)
    {
      console.log(width);
      this.snav.collapse();
    }else{
      console.log(width);
    }
  }

  togglee() {
    // this.sidebarService.toggle(true);
    // return false;
    this.sidebarService.toggle(false, 'left');
  }

  logout(){
    this.selectedItem = 'Dashboard'
    this.all.logout(false);
    this.router.navigate(['login'])
  }
  ngAfterViewChecked(){
      let width=window.innerWidth;
      if(this.toggleLogin == true)
      {
        if(this.snav != undefined)
        {
          if(localStorage.getItem('toggleFix') == '0')
          {
            if(width < 575.98)
            {
            localStorage.setItem('toggleFix','1');
            this.snav.collapse();
            }else{
          }
      }
    }
    }
  }
}

