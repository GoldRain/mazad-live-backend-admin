import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { 
  NbThemeModule, 
  NbCardModule, 
  NbTabsetModule, 
  NbRouteTabsetModule, 
  NbMenuModule, 
  NbUserModule, 
  NbActionsModule, 
  NbSearchModule, 
  NbCheckboxModule,
  NbPopoverModule, 
  NbContextMenuModule, 
  NbProgressBarModule,
  NbTooltipModule,
  NbInputModule,
  NbButtonModule,
  NbDialogModule,
  NbListModule,
  NbMenuService,
  NbAlertModule,
  NbSpinnerModule,
  NbBadgeModule,
  NbAccordionModule,
  NbToastrModule
  } from '@nebular/theme';

import {MaterialModule} from './material'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Constants } from './constants';
import { AllService } from './all.service';
import { HttpClientModule } from '@angular/common/http';
import { CarouselModule } from 'ngx-bootstrap/carousel';

import { AppComponent } from './app.component';

import { NbSidebarModule, NbLayoutModule, NbSidebarService } from '@nebular/theme';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { UserVarifiedComponent } from './user-varified/user-varified.component';
import { VerificationComponent } from './verification/verification.component';
import { ReportedUserComponent } from './reported-user/reported-user.component';
import { PostListComponent } from './post-list/post-list.component';
import { RequestComponent } from './request/request.component';
import { ApprovedPostComponent } from './approved-post/approved-post.component';
import { ApprovedStoryComponent } from './approved-story/approved-story.component';
import { OrderListComponent } from './order-list/order-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { TagsComponent } from './tags/tags.component';
import { LoginComponent } from './login/login.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { ConfirmOtpComponent } from './confirm-otp/confirm-otp.component';
import { UserPasswordComponent } from './user-password/user-password.component';
import { ReportedDetailComponent } from './reported-detail/reported-detail.component';
import { NgbdModal1Content } from './user-detail/user-detail.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { AdminPasswordComponent } from './admin-password/admin-password.component';
import { ImagePopUpComponent } from './image-pop-up/image-pop-up.component';
import { StoryPopUpComponent } from './story-pop-up/story-pop-up.component';
import { StoryListComponent } from './story-list/story-list.component';
import { StoryDetailComponent } from './story-detail/story-detail.component';
import { ReportedPostComponent } from './reported-post/reported-post.component';
import { ReportedStoryComponent } from './reported-story/reported-story.component';
import { BlockUserComponent } from './block-user/block-user.component'
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UsersComponent,
    UserVarifiedComponent,
    VerificationComponent,
    ReportedUserComponent,
    PostListComponent,
    RequestComponent,
    ApprovedPostComponent,
    ApprovedStoryComponent,
    OrderListComponent,
    UserDetailComponent,
    PostDetailComponent,
    TagsComponent,
    LoginComponent,
    ConfirmEmailComponent,
    ConfirmOtpComponent,
    UserPasswordComponent,
    ReportedDetailComponent,
    NgbdModal1Content,
    OrderDetailComponent,
    AdminPasswordComponent,
    ImagePopUpComponent,
    StoryPopUpComponent,
    StoryListComponent,
    StoryDetailComponent,
    ReportedPostComponent,
    ReportedStoryComponent,
    BlockUserComponent
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    NbThemeModule.forRoot(),
    NbLayoutModule,
    NbSidebarModule,
    AppRoutingModule,
    NbCardModule,
    NbLayoutModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbMenuModule.forRoot(),
    NbUserModule,
    NbActionsModule,
    NbSearchModule,
    NbSidebarModule,
    NbCheckboxModule,
    NbPopoverModule,
    NbContextMenuModule,
    NbProgressBarModule,
    NbTooltipModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NbInputModule,
    NbButtonModule,
    NbDialogModule,
    NbListModule,
    NgbModule,
    HttpClientModule,
    CarouselModule,
    NbAlertModule,
    NbSpinnerModule,
    NbBadgeModule,
    NbAccordionModule,
    NbToastrModule.forRoot(),
  ],
  providers: [
    NbSidebarService,Constants,AllService,NbMenuService
  ],
  entryComponents: [NgbdModal1Content],
  bootstrap: [AppComponent]
})
export class AppModule { }
