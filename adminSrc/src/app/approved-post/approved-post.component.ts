import { Component, OnInit } from '@angular/core';
import { AllService } from "../all.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-approved-post',
  templateUrl: './approved-post.component.html',
  styleUrls: ['./approved-post.component.css']
})
export class ApprovedPostComponent implements OnInit {
  details;
  page=0;
  amount = [];
  toggleWarning:boolean=false;
  toggleLoader:boolean=true;
  constructor(private all: AllService, private modalService: NgbModal) {
    this.all.changeLoader(true)
  }

  ngOnInit() {
    let i = 0;
    let flag = 0;
    let max = 0;
    let _thiss = this;
    this.all.approvedList("0").subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.details = result.message;
      if(this.details.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log("q",this.details);

      this.details.forEach(function(value) {
        value.bids.forEach(temp => {
          flag = parseInt(temp.bid_amount);
          if (flag < max) {
          } else {
            max = flag;
          }
        });
        console.log("max", max);
        _thiss.amount[i]=max;
        max = 0;
        i++;
        flag = 0;
      });
      this.all.changeLoader(false)
      console.log("hello", this.amount);
    });
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }


  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
    let i = 0;
    let flag = 0;
    let max = 0;
    let _thiss = this;
    this.all.approvedList(a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.details = result.message;
      if(this.details.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log("q",this.details);

      this.details.forEach(function(value) {
        value.bids.forEach(temp => {
          flag = parseInt(temp.bid_amount);
          if (flag < max) {
          } else {
            max = flag;
          }
        });
        console.log("max", max);
        _thiss.amount[i]=max;
        max = 0;
        i++;
        flag = 0;
      });
      this.all.changeLoader(false)
      console.log("hello", this.amount);
    });
  }
  }

  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;
    let i = 0;
    let flag = 0;
    let max = 0;
    let _thiss = this;
    this.all.approvedList(a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      
      if(result.message.length==0){
        this.all.changeLoader(false)
        a--;
        this.page--;
      }else{
        this.details = result.message;
        this.toggleWarning=false;
      
      console.log("q",this.details);

      this.details.forEach(function(value) {
        value.bids.forEach(temp => {
          flag = parseInt(temp.bid_amount);
          if (flag < max) {
          } else {
            max = flag;
          }
        });
        console.log("max", max);
        _thiss.amount[i]=max;
        max = 0;
        i++;
        flag = 0;
      });
      this.all.changeLoader(false)
      console.log("hello", this.amount);
    }
    });
    
  }



  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }
}
