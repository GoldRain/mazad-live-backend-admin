import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovedStoryComponent } from './approved-story.component';

describe('ApprovedStoryComponent', () => {
  let component: ApprovedStoryComponent;
  let fixture: ComponentFixture<ApprovedStoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovedStoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovedStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
