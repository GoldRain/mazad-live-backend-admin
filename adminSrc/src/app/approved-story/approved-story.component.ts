import { Component, OnInit } from '@angular/core';
import { AllService } from "../all.service";
import { ActivatedRoute,Router,ParamMap } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'app-approved-story',
  templateUrl: './approved-story.component.html',
  styleUrls: ['./approved-story.component.css']
})
export class ApprovedStoryComponent implements OnInit {

  details;
  page=0;
  amount = [];
  imageDetail=[];
  toggleWarning:boolean=false;
  toggleBid:boolean=false;
  toggleStory:boolean=false;
  toggleLoader:boolean=true;

  constructor(private all: AllService,private route: ActivatedRoute,private router: Router,private modalService: NgbModal) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    let i = 0;
    let flag = 0;
    let max = 0;
    let _thiss = this;
    this.all.approvedStoryList("0").subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.details = result.message;
      if(this.details.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log("q",this.details);

      this.details.forEach(function(value) {
        value.bidsAndComments.forEach(temp => {
          flag = parseInt(temp.amount);
          if (flag < max) {
          } else {
            max = flag;
          }
        });
        console.log("max", max);
        _thiss.amount[i]=max;
        max = 0;
        i++;
        flag = 0;
      });

      console.log("hello", this.amount);
      this.all.changeLoader(false)
    });
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }


  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
    let i = 0;
    let flag = 0;
    let max = 0;
    let _thiss = this;
    this.all.approvedStoryList(a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.details = result.message;
      if(this.details.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log("q",this.details);

      this.details.forEach(function(value) {
        value.bidsAndComments.forEach(temp => {
          flag = parseInt(temp.amount);
          if (flag < max) {
          } else {
            max = flag;
          }
        });
        console.log("max", max);
        _thiss.amount[i]=max;
        max = 0;
        i++;
        flag = 0;
      });
      this.all.changeLoader(false)
      console.log("hello", this.amount);
    });
  }
  }

  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;
    let i = 0;
    let flag = 0;
    let max = 0;
    let _thiss = this;
    this.all.approvedStoryList(a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      
      if(result.message.length==0){
        this.all.changeLoader(false)
        a--;
        this.page--;
      }else{
        this.details = result.message;
        this.toggleWarning=false;

      console.log("q",this.details);

      this.details.forEach(function(value) {
        value.bidsAndComments.forEach(temp => {
          flag = parseInt(temp.amount);
          if (flag < max) {
          } else {
            max = flag;
          }
        });
        console.log("max", max);
        _thiss.amount[i]=max;
        max = 0;
        i++;
        flag = 0;
      });
      this.all.changeLoader(false)
      console.log("hello", this.amount);
    }
    });
  }

  togStory(){
    this.toggleStory=true;
    this.toggleBid=false;
  }

  togBid(){
    this.toggleStory=false;
    this.toggleBid=true;
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  sendBundleStory(story){
    this.imageDetail.push(story)  
    this.all.changeStory(this.imageDetail);
    // console.log("storysend",this.imageDetail);
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  showOverview(){
    this.router.navigate(['story-pop-up'],{relativeTo : this.route});
  }


}
