import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute,Router,ParamMap } from "@angular/router";
import { AllService } from '../all.service';


@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {
  adminEmail;
  title;
  name;
  toggleSuccess:boolean;
  toggleError:boolean;
  toggleLoader:boolean=false;
  toggleApi:boolean=true;
  loading:boolean=false;
  constructor(private route: ActivatedRoute,private router: Router,private all:AllService) {
    this.all.changeLoader(false)
   }

  ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('name');
    if(this.name=="forget")
    {
      this.title="Forget Password";
    }else{
      this.title="Recovery";
    }
    this.toggleApi=false;
    console.log("hello")
    
    }

  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  onSubmit(){
    this.loading=true;
    this.toggleApi=true;
    console.log(this.adminEmail);
    this.all.changeLoader(true)
    sessionStorage.setItem("adminEmail", this.adminEmail);
    this.all.sendEmail(this.adminEmail).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      console.log(result);
      if(result.message == "Success")
      {
          this.all.changeLoader(false)
          this.toggleSuccess = true;
          this.toggleError = false;
          this.loading = false;
          sessionStorage.setItem('confirm-otp','true')
          this.router.navigate(['confirm-otp'])
      }else{
          this.all.changeLoader(false)
          this.toggleSuccess = false;
          this.toggleError = true;
          this.loading = false;
      }
      this.toggleApi=false;
    })
  }

}
