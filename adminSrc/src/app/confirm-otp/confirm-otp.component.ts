import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';
import { ActivatedRoute,Router,ParamMap } from "@angular/router";

@Component({
  selector: 'app-confirm-otp',
  templateUrl: './confirm-otp.component.html',
  styleUrls: ['./confirm-otp.component.css']
})
export class ConfirmOtpComponent implements OnInit {

  adminOTP;
  adminEmail=sessionStorage.getItem('adminEmail');
  toggleSuccess:boolean;
  toggleError:boolean;
  toggleNothing:boolean;
  toggleIncorrect:boolean;
  toggleLoader:boolean=false;
  toggleApi:boolean=true;

  constructor(private route: ActivatedRoute,private router: Router,private all:AllService) {
    this.all.changeLoader(false)
   }

  ngOnInit() {
    console.log("hello");
    if(sessionStorage.getItem('confirm-otp')!="true")
    {
      this.router.navigate(['confirm-email'])
    }
    this.toggleApi=false;
    
  }

  resendOTP(){
    this.toggleApi=true;
    console.log("resendOTP",this.adminEmail);
    this.all.changeLoader(true)
    this.all.sendEmail(this.adminEmail).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      console.log(result);
      if(result.message == "Success")
      {
        this.all.changeLoader(false)
        this.toggleSuccess = true;
        this.toggleError = false;
        this.toggleNothing = false;
        this.toggleIncorrect = false;
      }else{
        this.all.changeLoader(false)
        this.toggleSuccess = false;
        this.toggleError = true;
        this.toggleNothing = false;
        this.toggleIncorrect = false;
      }
      this.toggleApi=false;
    })
  }

  verifyOTP(){
    this.toggleApi=true;
    this.all.changeLoader(true)
    this.all.verifyAdminOtp(this.adminEmail,this.adminOTP).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.message == "Success")
      {
        this.toggleSuccess = true;
        this.toggleError = false;
        this.toggleNothing = false;
        this.toggleIncorrect = false;
        sessionStorage.setItem('admin-password','true')
        this.router.navigate(['admin-password'])
        this.all.changeLoader(false)
      }
      else{
        this.toggleSuccess = false;
        this.toggleError = false;
        if(this.adminOTP==undefined){
          this.toggleNothing = true;
          this.toggleIncorrect = false;
        }else if(this.adminOTP==""){
          this.toggleNothing = true;
          this.toggleIncorrect = false;
        }else{
          this.toggleIncorrect = true;
          this.toggleNothing = false;
        }
        this.all.changeLoader(false)
      }
      this.toggleApi=false;
    })
  }

}
