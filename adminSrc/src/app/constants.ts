export class Constants {
    // port = "http://localhost:8082";
    // port = "http://mazadlive.com:8082";
    // port = "http://192.168.0.120:8082";
    // port = "http://18.221.218.175:8082";
    // port = "http://192.168.0.166:8082";
    port = "http://18.218.27.17:8082";
    // port = "https://mazadlive.com:8084";
    version = "1.1";
    getPort() {
      return this.port;
    }
    getVersion(){
      return this.version;
    }
  }
  