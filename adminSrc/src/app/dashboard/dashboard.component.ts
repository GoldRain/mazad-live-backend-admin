import { Component, OnInit, ViewChild } from '@angular/core';
import { AllService } from '../all.service';
import { NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {

  @ViewChild('snav') snav: NbSidebarService;

  total_user:number=0;
  android_user:number = 0;
  ios_user:number = 0;
  postCount:number = 0;
  requested:number = 0;
  order:number = 0;
  analyse="";
  toggleLoader:boolean=true;
  tempLoader=0;

  constructor(private all:AllService,private sidebarService: NbSidebarService) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.all.seeUserNumber().subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.total_user = result.description.length
      this.tempLoader++;
      if(this.tempLoader==6){
        
        this.all.changeLoader(false)
      }
    })
    this.all.totalDeviceList("android").subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      this.android_user = result.description.length;
      this.tempLoader++;
      if(this.tempLoader==6){
        
        this.all.changeLoader(false)
      }
    })
    this.all.totalDeviceList("ios").subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      this.ios_user = result.description.length;
      this.tempLoader++;
      if(this.tempLoader==6){
        
        this.all.changeLoader(false)
      }
    })
    this.all.postCount().subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      if(result.message.length != 0)
      {this.postCount = result.message[0].Counting;}
      else
      {this.postCount = 0;}
      this.tempLoader++;
      if(this.tempLoader==6){
        
        this.all.changeLoader(false)
      }
    })
    this.all.countRequest().subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      
      if(result.message.length != 0)
      {this.requested = result.message[0].Counting;}
      else
      {this.requested  = 0;}
      this.tempLoader++;
      if(this.tempLoader==6){
        
        this.all.changeLoader(false)
      }
    })
    this.all.orderCount().subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      
      if(result.message.length != 0)
      {this.order = result.message[0].Counting;}
      else
      {this.order = 0;}

      this.tempLoader++;
      if(this.tempLoader==6){
        
        this.all.changeLoader(false)
      }
    })
    // this.all.analyseDailyPost().subscribe(data => {
    //   let result = JSON.parse(JSON.stringify(data));
    //   this.analyse = result.data;
    //   this.tempLoader++;
    //   if(this.tempLoader==6){
        
    //     this.all.changeLoader(false)
    //   }

    // })


    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');   
  }

}
