import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';

@Component({
  selector: 'app-image-pop-up',
  templateUrl: './image-pop-up.component.html',
  styleUrls: ['./image-pop-up.component.css']
})
export class ImagePopUpComponent implements OnInit {
  Img;
  flag="";
  temp="";
  constructor(private all:AllService) { }


  ngOnInit() {
    this.all.currentImage.subscribe(img => {this.Img = img;
    console.log(this.Img[0].is_video)})
    if(this.Img[0].is_video==true)
    {
      this.flag=this.Img[0].image_url.lastIndexOf(".");
      this.temp=this.Img[0].image_url.substring(0,this.flag);
      this.temp =this.temp + ".mp4";
      console.log("hello",this.temp);
    }
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');

  }

}
