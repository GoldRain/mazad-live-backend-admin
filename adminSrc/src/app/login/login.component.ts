import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AllService } from '../all.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  adminEmail;
  adminPassword;
  toggleLogin:boolean = true;
  toggleEmail:boolean = false;
  toggleApi:boolean = true;
  loading:boolean = false;
  loginError:boolean =false;
  constructor(private all:AllService,private router: Router) {
    this.all.changeLoader(false)
   }

  ngOnInit() {
    // this.all.changeLoader(true)
    console.log("Login");
    this.all.currentLogin.subscribe(value => {this.toggleLogin = value;})
    if(this.toggleLogin == true)
    {this.router.navigate(['dashboard'])}
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
    localStorage.setItem('toggleFix','0');
    this.toggleApi=false;
    // this.all.changeLoader(false)
  }

  onSubmit(){
    this.loading=true;
    console.log("here log");
    this.all.changeLoader(true)
    this.toggleApi=true;
    this.all.loginAdmin(this.adminEmail,this.adminPassword).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      console.log(result);
      if(result.error)
      {
        // error msg
        this.loginError=true;
        this.loading=false;
      }else{
        this.loginError=false;
        this.loading=false;
        console.log("result",result.message[0]._id);
        localStorage.setItem("admin_id", result.message[0]._id);
        localStorage.setItem('NavItem','Dashboard');
        this.router.navigate(['dashboard'])
        this.all.setLoggedIn(true)
      }
      this.toggleApi=false;
      this.all.changeLoader(false)
    })
  }

  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  password = new FormControl('',[Validators.required]);

  getErrorPassword(){
    return this.password.hasError('required') ? 'You must enter a value' :
            '';
  }

}
