import { NgModule } from "@angular/core";
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

@NgModule({
    imports: [
        MatInputModule,
        MatButtonModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatSlideToggleModule
    ],
    exports: [
        MatInputModule,
        MatButtonModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatSlideToggleModule
    ],
})
export class MaterialModule{}