import { Component, OnInit } from '@angular/core';
import { AllService } from "../all.service";
import { ActivatedRoute,Router,ParamMap } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {

  // Note: Change Payment Status is changed so it is commented

  id=null;
  detail;
  address;
  toggleLoader=true;
  payId=null;
  togglePay:boolean=false;
  toggleStry:boolean=false;
  textareaItemNgModel="";
  toggleComp=false;
  togglePayError:boolean=false;
  togglePayAction:boolean=false;
  togglePayStatus:boolean=false;
  constructor(private all: AllService,private route: ActivatedRoute,private router: Router,private modalService: NgbModal) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.all.orderDetail(this.id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.detail = result.message[0];
      console.log(this.detail);
      console.log("check>>",this.detail.addressArray[0].addresses);
      for(let i=0;i<this.detail.addressArray[0].addresses.length;i++)
      {
        if(this.detail.address_id==this.detail.addressArray[0].addresses[i]._id)
        {
          this.address = this.detail.addressArray[0].addresses[i];
        }else{
          this.address = "Not Been Set";
        }
      }
      console.log("add->",this.address);
      this.payId=this.detail.paymentReqDetails[0]._id;
      this.all.changeLoader(false)
    })
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  sendBundleStory(story){
    console.log(">>>>>>>>",story)
    let temp:any = [];
    temp.push(story);
    console.log("checktempStorypop>>",temp)
    this.all.changeStory(temp);
    // console.log("Hello",story);
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  showOverview(){
    this.router.navigate(['story-pop-up'],{relativeTo : this.route});
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  togPay(){
    this.togglePay=true
    this.toggleStry=false;
    this.toggleComp=false
    this.togglePayAction=false
  }

  togStory(){
    console.log("toggleStry");
    this.toggleStry=true;
    this.togglePay=false;
    this.toggleComp=false
    this.togglePayAction=false
  }
  
  togComp(){
    this.toggleStry=false;
    this.togglePay=false;
    this.toggleComp=true
    this.togglePayAction=false
  }

  // this function is removed from Payment status in user detail
  togPayAction(){
    this.toggleStry=false;
    this.togglePay=false;
    this.toggleComp=false;
    this.togglePayAction=true
  }

  //change action is removed 

  // changeAction(order_id){
  //   this.all.ChangePaymentStatus(order_id).subscribe(data => {
  //     let result = JSON.parse(JSON.stringify(data));
  //     if(result.error == false){
  //       // hadle success
  //       window.location.reload();
  //       this.togglePayStatus=false
  //       console.log("success");
  //     }else{
  //       this.togglePayStatus=true
  //       // handle error
  //       console.log("fail")
  //     }
  //   })
  // }

  payment(){
    console.log(this.textareaItemNgModel)
    this.all.PassPaymentFromAdmin(this.payId,this.textareaItemNgModel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      console.log(result)
      if(result.error == false){
        // hadle success
        window.location.reload();
        this.togglePayError=false
        console.log("success");
      }else{
        this.togglePayError=true
        // handle error
        console.log("fail")
      }
    })
  }

}
