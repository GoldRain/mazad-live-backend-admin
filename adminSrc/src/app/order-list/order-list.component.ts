import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  
  
  list;
  page=0;
  toggleWay="";
  orderFilter="";
  typeCancel="";
  MsgAboveFilter="Showing All Orders";
  toggleError:boolean=false;
  toggleLoader:boolean=true;
  constructor(private all:AllService) {
    this.all.changeLoader(true)
  }

  ngOnInit() {
    this.all.orderList(0,this.toggleWay,this.orderFilter,this.typeCancel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data))
      this.list = result.message;
      if(result.message.length==0)
      {this.toggleError=true;}
      else
      {this.toggleError=false;}
      this.all.changeLoader(false)
      console.log(result.message);
    })
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  reset(){
    this.all.changeLoader(true)
    this.toggleWay="";
    this.orderFilter="";
    this.typeCancel="";
    this.MsgAboveFilter="Showing All Orders";
    this.all.orderList(0,this.toggleWay,this.orderFilter,this.typeCancel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data))
      this.list = result.message;
      if(result.message.length==0)
      {this.toggleError=true;}
      else
      {this.toggleError=false;}
      this.all.changeLoader(false)
      console.log(result.message);
    })
  }

  Filter(){
    this.all.changeLoader(true)
    this.page=0;
    this.all.orderList(0,this.toggleWay,this.orderFilter,this.typeCancel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data))
      this.list = result.message;
      if(result.message.length==0)
      {this.toggleError=true;}
      else
      {this.toggleError=false;}
      console.log(result.message);
      this.all.changeLoader(false)
    })
    this.orderFilter="";
  }

  togCancel(){
    this.toggleWay="";
    this.typeCancel="rejected";
    this.orderFilter="";
    this.MsgAboveFilter="Showing Canceled Orders";
    this.page=0;
    this.all.changeLoader(true)
    this.all.orderList(0,this.toggleWay,this.orderFilter,this.typeCancel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data))
      this.list = result.message;
      if(result.message.length==0)
      {this.toggleError=true;}
      else
      {this.toggleError=false;}
      console.log(result.message);
      this.all.changeLoader(false)
    })
  }

  wayCOD(){
    this.toggleWay="cod";
    this.typeCancel="";
    this.orderFilter="";
    this.MsgAboveFilter="Showing Cash On Delivery Orders";
    this.page=0;
    this.all.changeLoader(true)
    this.all.orderList(0,this.toggleWay,this.orderFilter,this.typeCancel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data))
      this.list = result.message;
      if(result.message.length==0)
      {this.toggleError=true;}
      else
      {this.toggleError=false;}
      console.log(result.message);
      this.all.changeLoader(false)
    })
  }

  wayCC(){
    this.toggleWay="card";
    this.typeCancel="";
    this.orderFilter="";
    this.MsgAboveFilter="Showing Card Orders";
    this.page=0;
    this.all.orderList(0,this.toggleWay,this.orderFilter,this.typeCancel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data))
      this.list = result.message;
      if(result.message.length==0)
      {this.toggleError=true;}
      else
      {this.toggleError=false;}
      console.log(result.message);
    })
  }

  wayALL(){
    this.all.changeLoader(true)
    this.toggleWay="";
    this.typeCancel="";
    this.orderFilter="";
    this.MsgAboveFilter="Showing All Orders";
    this.page=0;
    this.all.orderList(0,this.toggleWay,this.orderFilter,this.typeCancel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data))
      this.list = result.message;
      if(result.message.length==0)
      {this.toggleError=true;}
      else
      {this.toggleError=false;}
      this.all.changeLoader(false)
      console.log(result.message);
    })
  }

  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
    this.all.orderList(a,this.toggleWay,this.orderFilter,this.typeCancel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data))
      this.list = result.message;
      if(result.message.length==0)
      {this.toggleError=true;}
      else
      {this.toggleError=false;}
      this.all.changeLoader(false)
      console.log(result.message);
    })
  }
  }


  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;
    this.all.orderList(a,this.toggleWay,this.orderFilter,this.typeCancel).subscribe(data => {
    let result = JSON.parse(JSON.stringify(data))
    if(result.message.length==0){
      this.all.changeLoader(false)
      a--;
      this.page--;
      
    }else{
      this.list = result.message;
      this.toggleError=false;
    
    this.all.changeLoader(false)
  }
  })

  }

}
