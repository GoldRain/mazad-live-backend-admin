export class PostDetailModel{
    error : any
    message  : any
    description : [{
        post_images : [{
            _id : any,
            image_url : String,
            is_video : Boolean
        }],
        sellerDetail : [
            {
                _id : any,
                profilePhoto : String,
                username : String,
                email : String
            }
        ],
        description : String,
        place_name : String,
        amount : String,
        bids : [
            {
                status : String,
                _id : any,
                bid_amount : String,
                user_name : String,
                url : String,
                bid_user_id : any
            }
        ],
        post_like :[
            {
                post_id : any
            }
        ],
        vote :[
            {
                user_id : any
            }
        ]
    }]
}