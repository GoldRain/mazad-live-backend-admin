import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';
import { ActivatedRoute,Router,ParamMap } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PostDetailModel } from './PostDetailModel';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  id=null;
  toggleBid:boolean=false;
  toggleView:boolean=false;
  toggleLike:boolean=false;
  toggleVote:boolean=false;
  toggleWarning:boolean=false;
  toggleLoader:boolean=true;
  mDetails : PostDetailModel;
  name : String;
  profilePhoto : String;
  email : String;
  post_images : any;
  is_verified : Boolean = false;
  bids : any;
  post_like :any;
  vote :any;
  bids_length:any;
  post_like_length:any;
  vote_length:any;
  video_check:Boolean;
  details:String;
  place_name:String;
  amount:String;
  sellerId:any;

  constructor(private all: AllService,private route: ActivatedRoute,private router: Router,private modalService: NgbModal) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.all.postDetail(this.id).subscribe(data => {
      this.mDetails = data;
      console.log("mdata is here ", this.mDetails.description);
      this.name = this.mDetails.description[0].sellerDetail[0].username;
      this.profilePhoto = this.mDetails.description[0].sellerDetail[0].profilePhoto;
      this.sellerId = this.mDetails.description[0].sellerDetail[0]._id;
      this.email = this.mDetails.description[0].sellerDetail[0].email;
      this.post_images = this.mDetails.description[0].post_images;
      this.bids = this.mDetails.description[0].bids;
      this.post_like = this.mDetails.description[0].post_like;
      this.vote = this.mDetails.description[0].vote;
      this.bids_length = this.bids.length;
      this.post_like_length = this.post_like.length;
      this.vote_length = this.vote.length;
      this.video_check = this.mDetails.description[0].post_images[0].is_video;
      this.details = this.mDetails.description[0].description;
      this.place_name = this.mDetails.description[0].place_name;
      this.amount = this.mDetails.description[0].amount;
      this.all.changeLoader(false)
    })
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  sendBundle(img){
    this.all.changeImage(img);
    // console.log("Hello",this.Img);
  }

  showOverview(){
    this.router.navigate(['pop-up'],{relativeTo : this.route});
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  togBid(){
    this.toggleBid = true;
    this.toggleView = false;
    this.toggleLike = false;
    this.toggleVote = false;
  }

  togView(){
    this.toggleBid = false;
    this.toggleView = true;
    this.toggleLike = false;
    this.toggleVote = false;
  }

  togLike(){
    this.toggleBid = false;
    this.toggleView = false;
    this.toggleLike = true;
    this.toggleVote = false;
  }

  togVote(){
    this.toggleBid = false;
    this.toggleView = false;
    this.toggleLike = false;
    this.toggleVote = true;
  }

}
