import { Component, OnInit } from '@angular/core';
import { AllService } from "../all.service";
import { ActivatedRoute,Router,ParamMap } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  id=null;
  posts;
  Img;
  page=0;
  toggleBid:boolean=false;
  toggleView:boolean=false;
  toggleLike:boolean=false;
  toggleVote:boolean=false;
  toggleWarning:boolean=false;
  toggleLoader:boolean=true;
    constructor(private all: AllService,private route: ActivatedRoute,private router: Router,private modalService: NgbModal) {
      this.all.changeLoader(true)
    }

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    if(this.id != null)
    {
    this.all.userPost(this.id,this.page).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.posts = result.message;
      if(this.posts.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log(this.posts);
      this.all.changeLoader(false)
    })
    }else{
      this.all.allPost(0).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.posts = result.message;
        if(this.posts.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        console.log(this.posts);
        this.all.changeLoader(false)
      }); 
    }

    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  sendBundle(img){
    this.all.changeImage(img);
    // console.log("Hello",this.Img);
  }

  showOverview(){
    this.router.navigate(['pop-up'],{relativeTo : this.route});
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
    if(this.id != null){
      this.all.userPost(this.id,a).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.posts = result.message;
        if(this.posts.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        console.log(this.posts);
        this.all.changeLoader(false)
      })
    }else{
      this.all.allPost(a).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.posts = result.message;
        if(this.posts.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        console.log(this.posts);
        this.all.changeLoader(false)
      });
    }
  }
  }

  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;
    if(this.id != null)
    {
      this.all.userPost(this.id,a).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        if(result.message.length==0){
          a--;
          this.page--;
        }else{
          this.posts = result.message;
          this.toggleWarning=false;
        }
        console.log(this.posts);
        this.all.changeLoader(false)
      })
    }else{
      this.all.allPost(a).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        if(result.message.length==0){
          a--;
          this.page--;
        }else{
          this.posts = result.message;
          this.toggleWarning=false;
        }
        console.log(this.posts);
        this.all.changeLoader(false)
      });
    }
  }
  togBid(){
    this.toggleBid = true;
    this.toggleView = false;
    this.toggleLike = false;
    this.toggleVote = false;
  }

  togView(){
    this.toggleBid = false;
    this.toggleView = true;
    this.toggleLike = false;
    this.toggleVote = false;
  }

  togLike(){
    this.toggleBid = false;
    this.toggleView = false;
    this.toggleLike = true;
    this.toggleVote = false;
  }

  togVote(){
    this.toggleBid = false;
    this.toggleView = false;
    this.toggleLike = false;
    this.toggleVote = true;
  }
}
