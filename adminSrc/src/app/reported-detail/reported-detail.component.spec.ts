import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportedDetailComponent } from './reported-detail.component';

describe('ReportedDetailComponent', () => {
  let component: ReportedDetailComponent;
  let fixture: ComponentFixture<ReportedDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportedDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportedDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
