import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router,ParamMap } from "@angular/router";
import { AllService } from "../all.service";
import { NbToastrService } from '@nebular/theme';


@Component({
  selector: 'app-reported-detail',
  templateUrl: './reported-detail.component.html',
  styleUrls: ['./reported-detail.component.css']
})
export class ReportedDetailComponent implements OnInit {

  toggleBlock:boolean=true;
  toggleLoader:boolean=false;
  id;
  admin_id;
  id_tobeBlocked;
  detail;
  reportedPerson;
  // toggleReportError:boolean=false;
  // toggleReportSuccess:boolean=false
  constructor(private all: AllService,private route: ActivatedRoute,private router: Router,private toastrService: NbToastrService) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.all.reportUser(this.id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      console.log(result);
      this.detail = result.description;
      this.reportedPerson = result.reportedPerson;
      this.toggleBlock = result.reportedPerson.is_admin_block;
      this.id_tobeBlocked = result.reportedPerson._id;
      console.log(this.reportedPerson)
      // console.log(result.reportedPerson.is_admin_block);
      // console.log(result);
      this.all.changeLoader(false)
    })
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  showToast(status,heading,msg) {
    this.toastrService.show(msg, `${heading}`, { status });
  }

  fnBlock(){
    this.all.changeLoader(true)
    this.toggleBlock = !this.toggleBlock;
    console.log(this.toggleBlock);
    this.admin_id = localStorage.getItem("admin_id");
    console.log("this",this.admin_id);
    this.all.adminBlock(this.admin_id,this.id_tobeBlocked,this.toggleBlock).subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      if(result.message == "Success"){
        if(this.toggleBlock){
          this.showToast('success',"Blocked",'Blocked Successful')
        }else{
          this.showToast('success',"Unblocked",'Unblocked Successful')
        }
        this.toggleBlock = this.toggleBlock;
      }else{
        this.showToast('danger',"Error",'Action Failed Retry')
        this.toggleBlock = !this.toggleBlock;
      }
      this.all.changeLoader(false)
    })
  }

  unReport(check,reported_id,reported_user_id){
    console.log(check)
    console.log(reported_id)
    console.log(reported_user_id)
    this.all.changeLoader(true)
    this.all.unReportUser(check,reported_id,reported_user_id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.message == "Success"){
        if(check == 'all'){
          this.router.navigate(['/reported-user'])
          this.all.changeLoader(false)
        }
        else{
          console.log("hello")
          this.all.reportUser(this.id).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          console.log(result);
          this.detail = result.description;
          if(this.detail.length==0)
          {
            this.router.navigate(['/reported-user'])
          }
          this.reportedPerson = result.reportedPerson;
          this.toggleBlock = result.reportedPerson.is_admin_block;
          this.id_tobeBlocked = result.reportedPerson._id;
          // this.toggleReportSuccess=true;
          // this.toggleReportError=false;
          this.showToast('success',"UnReport",'UnReport Successfull')
          this.all.changeLoader(false)
          })
        }
      }else{
        this.showToast('danger',"UnReport",'UnReport Failed Retry')
        // this.toggleReportError=true;
        // this.toggleReportSuccess=false;
        this.all.changeLoader(false)
      }
    })
  }

}
