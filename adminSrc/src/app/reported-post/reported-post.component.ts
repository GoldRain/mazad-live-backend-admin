import { Component, OnInit } from '@angular/core';
import { AllService } from "../all.service";
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-reported-post',
  templateUrl: './reported-post.component.html',
  styleUrls: ['./reported-post.component.css']
})
export class ReportedPostComponent implements OnInit {
  details;
  page=0;
  toggleWarning:boolean=false;
  toggleLoader:boolean=true;
  // toggleDiscardError:boolean=false;
  // toggleDeleteError:boolean=false;
  // toggleDiscardSuccess:boolean=false;
  // toggleDeleteSuccess:boolean=false;
  constructor(private all: AllService,private toastrService: NbToastrService) {
    this.all.changeLoader(true)
  }

  ngOnInit() {
    
    this.all.reportedPostList("0").subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.details = result.message;
      console.log(this.details)
      if(this.details.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log("q",this.details);

      this.all.changeLoader(false)
    });
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }


  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
    this.all.reportedPostList(a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.details = result.message;
      if(this.details.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log("q",this.details);

      this.all.changeLoader(false)

    });
  }
  }

  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;
    this.all.reportedPostList(a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      
      if(result.message.length==0){
        this.all.changeLoader(false)
        a--;
        this.page--;
      }else{
        this.details = result.message;
        this.toggleWarning=false;
      
      console.log("q",this.details);

      this.all.changeLoader(false)
    }
    }); 
  }

  showToast(status,heading,msg) {
    this.toastrService.show(msg, `${heading}`, { status });
  }

  delete(post_id,report_id){
    this.all.changeLoader(true)
    console.log('delete');
    let check = "all"
    this.all.deleteReportedPost(post_id,report_id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.error == false)
      {
        // this.all.discardReportedPost(post_id,report_id,check).subscribe(data => {
        //   let result = JSON.parse(JSON.stringify(data));
        //   if(result.error == false)
        //   {
            // this.toggleDeleteError=false
            // this.toggleDiscardError=false
            this.all.reportedPostList(this.page).subscribe(data => {
              let result = JSON.parse(JSON.stringify(data));
              this.details = result.message;
              if(result.message.length==0){
                this.page=0;
                this.all.reportedPostList(this.page).subscribe(data => {
                  let result = JSON.parse(JSON.stringify(data));
                  this.details = result.message;
                  if(this.details.length==0){
                    this.toggleWarning=true;
                  }else{
                    this.toggleWarning=false;
                  }
                  console.log("q",this.details);
                  this.showToast('success',"Post Delete",'Delete Successful')
                  this.all.changeLoader(false)
                });
              }else{
                this.details = result.message;
                this.toggleWarning=false;
              
              console.log("q",this.details);
              this.showToast('success',"Post Delete",'Delete Successful')
              this.all.changeLoader(false)
            }
            });
        //   }else{
        //     this.toggleDeleteError=false
        //     this.toggleDiscardError=true
        //   }
        // })

        // this.toggleDeleteError=false
        // this.toggleDiscardError=false
        // this.toggleDeleteSuccess=true
        // this.toggleDiscardSuccess=false
   
      }else{
        // this.toggleDeleteError=true
        // this.toggleDiscardError=false
        // this.toggleDeleteSuccess=false
        // this.toggleDiscardSuccess=false
        this.showToast('danger',"Post Delete",'Delete Failed Retry')
        this.all.changeLoader(false)
      }
    })
  }

  discard(post_id,report_id,check){
    this.all.changeLoader(true)
    console.log("discard");
    this.all.discardReportedPost(post_id,report_id,check).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.error == false)
      {
        this.all.reportedPostList(this.page).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          this.details = result.message;
          if(result.message.length==0){
            this.page=0;
            this.all.reportedPostList(this.page).subscribe(data => {
              let result = JSON.parse(JSON.stringify(data));
              this.details = result.message;
              if(this.details.length==0){
                this.toggleWarning=true;
              }else{
                this.toggleWarning=false;
              }
              console.log("q",this.details);
              this.showToast('success',"Report Discard",'Report Discard Successful')
              this.all.changeLoader(false)
            });
          }else{
            this.details = result.message;
            this.toggleWarning=false;
          
          console.log("q",this.details);
          this.showToast('success',"Report Discard",'Report Discard Successful')
          this.all.changeLoader(false)
        }
        });
        // this.toggleDeleteError=false
        // this.toggleDiscardError=false
        // this.toggleDeleteSuccess=false
        // this.toggleDiscardSuccess=true
        // this.showToast('success',"Report Discard",'Report Discard Successful')
      }else{
        // this.toggleDeleteError=false
        // this.toggleDeleteSuccess=false
        // this.toggleDiscardSuccess=false
        // this.toggleDiscardError=true
        this.showToast('danger',"Report Discard",'Report Discard Failed Retry')
        this.all.changeLoader(false)
      }
    })
  }

}
