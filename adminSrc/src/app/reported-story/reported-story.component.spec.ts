import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportedStoryComponent } from './reported-story.component';

describe('ReportedStoryComponent', () => {
  let component: ReportedStoryComponent;
  let fixture: ComponentFixture<ReportedStoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportedStoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportedStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
