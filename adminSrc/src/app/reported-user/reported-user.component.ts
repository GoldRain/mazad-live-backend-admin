import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';

@Component({
  selector: 'app-reported-user',
  templateUrl: './reported-user.component.html',
  styleUrls: ['./reported-user.component.css']
})
export class ReportedUserComponent implements OnInit {
  list;
  page=0;
  toggleWarning:boolean=false;
  toggleLoader:boolean=true;
  constructor(private all:AllService) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.all.changeLoader(true)
    this.all.reportList("0").subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      this.list = result.description;
      if(this.list.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log(result);
      this.all.changeLoader(false)
    })
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
    this.all.reportList(a).subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
      console.log(result);
      this.all.changeLoader(false)
    })
  }
  }
  
  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;
    this.all.reportList(a).subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      if(result.description.length==0){
        a--;
        this.page--;
      }else{
        this.list = result.description;
        this.toggleWarning=false;
      }
      this.all.changeLoader(false)
      console.log(result);
    })
  }
}
