import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';
import { ActivatedRoute,Router,ParamMap } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {

  page=0;
  details;
  toggleWarning:boolean=false;
  toggleStory:boolean=false;
  toggleIban:boolean=false;
  toggleLoader:boolean=true;
  togglePay:boolean=false;
  togglePayError:boolean=false;
  textareaItemNgModel="";
  
  constructor(private all:AllService,private route: ActivatedRoute,private router: Router,private modalService: NgbModal) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.all.requestList("0").subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      console.log(">>>>>>> ", result)
      this.details = result.message;
      if(this.details.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      this.sortRequestData();
      console.log(this.details);
      this.all.changeLoader(false)
    })
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  sortRequestData(){
    this.details.sort(function(a,b){
      return b.created_at - a.created_at
    })
    console.log("this.detail after sort>>",this.details)
  }

  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a <= 0)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }
    else{
      this.all.requestList(a).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.details = result.message;
        console.log("dec",this.details);
        if(this.details.length==0){
          this.toggleWarning=true;
        }else{
          this.sortRequestData();
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })
    }
  }

  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;
    this.all.requestList(a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      
      console.log("inc",result.message.length);
      if(result.message.length==0){
        a--;
        this.page--;
      }else{
        this.details = result.message;
        this.sortRequestData();
        this.toggleWarning=false;
      }
      this.all.changeLoader(false)
    })
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  sendBundleStory(story){
    this.all.changeStory(story);
    console.log("Hello",story);
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  showOverview(){
    this.router.navigate(['story-pop-up'],{relativeTo : this.route});
  }



  togStory(){
    this.toggleStory=true;
    this.toggleIban=false;
    this.togglePay=false
  }
  togIban(){
    this.toggleStory=false;
    this.toggleIban=true;
    this.togglePay=false;
  }

  togPay(){
    this.toggleStory=false;
    this.toggleIban=false;
    this.togglePay=true
  }

  payment(payId){
    console.log(this.textareaItemNgModel)
    this.all.PassPaymentFromAdmin(payId,this.textareaItemNgModel).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      console.log(result)
      if(result.error == false){
        window.location.reload();
        this.togglePayError=false
        console.log("success");
      }else{
        // handle error
        this.togglePayError=true
        console.log("fail")
      }
    })
  }


}
