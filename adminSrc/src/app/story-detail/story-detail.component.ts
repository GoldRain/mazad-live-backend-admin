import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';
import { ActivatedRoute,Router,ParamMap } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-story-detail',
  templateUrl: './story-detail.component.html',
  styleUrls: ['./story-detail.component.css']
})
export class StoryDetailComponent implements OnInit {

  id=null;
  toggleBid:boolean=false;
  toggleStory:boolean=false;
  toggleWarning:boolean=false;
  toggleLoader:boolean=true;
  detail

  constructor(private all: AllService,private route: ActivatedRoute,private router: Router,private modalService: NgbModal) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.all.storyDetail(this.id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data))
      this.detail = result.message[0];
      console.log("data is here ", this.detail);
      this.all.changeLoader(false)
    })
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  sendBundleStory(story){
    console.log("story",story)
    let temp:any = [];
    temp.push(story);
    this.all.changeStory(temp);
    // console.log("Hello",story);
  }

  showOverview(){
    this.router.navigate(['story-pop-up'],{relativeTo : this.route});
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  togBid(){
    this.toggleBid = true;
    this.toggleStory = false;
  }

  togStory(){
    this.toggleBid = false;
    this.toggleStory = true;
  }

}
