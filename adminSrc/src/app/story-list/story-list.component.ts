import { Component, OnInit } from '@angular/core';
import { AllService } from "../all.service";
import { ActivatedRoute,Router,ParamMap } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-story-list',
  templateUrl: './story-list.component.html',
  styleUrls: ['./story-list.component.css']
})
export class StoryListComponent implements OnInit {
  stories;
  page=0;
  toggleBid:boolean=false;
  toggleStory:boolean=false;
  toggleWarning:boolean=false;
  toggleLoader:boolean=true;
  id;
    constructor(private all: AllService,private route: ActivatedRoute,private router: Router,private modalService: NgbModal) {
      this.all.changeLoader(true)
    }

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get('id');

    if(this.id != null)
    {
    this.all.userStories(this.id,this.page).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.stories = result.message;
      if(this.stories.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log(this.stories);
      this.all.changeLoader(false)
    })
    }else{
      this.all.AllStory(0).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.stories = result.message;
        if(this.stories.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        console.log(this.stories);
        this.all.changeLoader(false)
      }); 
    }


    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  sendBundleStory(story){
    let temp:any = [];
    temp.push(story);
    this.all.changeStory(temp);
    // console.log("Hello",story);
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  showOverview(){
    this.router.navigate(['story-pop-up'],{relativeTo : this.route});
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
      if(this.id != null)
    {
    this.all.userStories(this.id,a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.stories = result.message;
      if(this.stories.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      console.log(this.stories);
      this.all.changeLoader(false)
    })
    }else{
      this.all.AllStory(a).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.stories = result.message;
        if(this.stories.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        console.log(this.stories);
        this.all.changeLoader(false)
      });
    }
    }
  }

  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;

    if(this.id != null)
    {
    this.all.userStories(this.id,a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.message.length==0){
        a--;
        this.page--;
      }else{
        this.stories = result.message;
        this.toggleWarning=false;
      }
      console.log(this.stories);
      this.all.changeLoader(false)
    })
    }else{
      this.all.AllStory(a).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        if(result.message.length==0){
          a--;
          this.page--;
        }else{
          this.stories = result.message;
          this.toggleWarning=false;
        }
        console.log(this.stories);
        this.all.changeLoader(false)
      });
    }
  }
  togBid(){
    this.toggleBid = true;
    this.toggleStory = false;
  }

  togStory(){
    this.toggleBid = false;
    this.toggleStory = true;
  }
}
