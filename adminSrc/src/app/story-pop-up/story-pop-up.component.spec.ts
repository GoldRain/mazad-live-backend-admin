import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoryPopUpComponent } from './story-pop-up.component';

describe('StoryPopUpComponent', () => {
  let component: StoryPopUpComponent;
  let fixture: ComponentFixture<StoryPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoryPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoryPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
