import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';

@Component({
  selector: 'app-story-pop-up',
  templateUrl: './story-pop-up.component.html',
  styleUrls: ['./story-pop-up.component.css']
})
export class StoryPopUpComponent implements OnInit {

  Stry;
  flag="";
  temp="";
  constructor(private all:AllService) { }

  ngOnInit() {
    this.all.currentStory.subscribe(data => {this.Stry = data;

      console.log(this.Stry)
      console.log(this.Stry[0].is_video)
      if(this.Stry[0].is_video==true)
      {
        this.flag=this.Stry[0].url.lastIndexOf(".");
        this.temp=this.Stry[0].url.substring(0,this.flag);
        this.temp =this.temp + ".mp4";
        console.log("hello",this.temp);
      }
    })
      sessionStorage.removeItem('confirm-otp');
      sessionStorage.removeItem('admin-password');
  }
}
