import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {

  TagCreate="";

  TagUpdate="";
  selectedTag_id;

  TagGroup="";

  TagBeenDeleted="";

  checkLoader=0;

  constructor(private all:AllService,private toastrService: NbToastrService) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.all.Tags('','read','').subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.TagGroup = result.description;
      console.log(this.TagGroup);
      this.checkLoader++;
      if(this.checkLoader==2){
        this.all.changeLoader(false)
      }
    })
    this.all.Tags('','readDeleted','').subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.TagBeenDeleted = result.description
      this.checkLoader++;
      if(this.checkLoader==2){
        this.all.changeLoader(false)
      }
    })
  }

  showToast(status,heading,msg) {
    this.toastrService.show(msg, `${heading}`, { status });
  }

  create(){
    this.all.changeLoader(true)
    console.log(this.TagCreate);
    if(this.TagCreate == ''){
      this.showToast('danger',"Create Tag",'Create Tag Failure Enter SomeValue')
      this.all.changeLoader(false)
    }else{
    this.all.Tags('','create',this.TagCreate).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.message=='Done')
      {
        this.all.Tags('','read','').subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          this.TagGroup = result.description
          this.showToast('success',"Create Tag",'Create Tag Success')
        })
      }else{
        // handle error
        this.showToast('danger',"Create Tag",'Create Tag Failure Retry')
      }
      this.TagCreate='';
      this.all.changeLoader(false)
    })
  }
  }

  resetCreate(){
    this.all.changeLoader(true)
    this.TagCreate='';
    this.showToast('info',"Reset",'Reset Create Inbox Successfully')
    this.all.changeLoader(false)
  }

  delete(tag_id){
    console.log(tag_id);
    this.all.changeLoader(true)
    this.all.Tags(tag_id,'delete','').subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.message == 'Tag deleted')
      {
        this.all.Tags('','read','').subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          this.TagGroup = result.description
          this.all.Tags('','readDeleted','').subscribe(data => {
            let result = JSON.parse(JSON.stringify(data));
            this.TagBeenDeleted = result.description
            this.all.changeLoader(false)
            this.showToast('success',"Delete Tag",'Delete Tag Success')
          })
        })
      }else{
        // handle error
        this.showToast('danger',"Delete Tag",'Delete Tag Failure Retry')
      }
    })
  }

  select(tag_id,tag_name){
    this.all.changeLoader(true)
    this.selectedTag_id=tag_id;
    this.TagUpdate=tag_name;
    this.showToast('success',"Select Tag",'Selected a Tag For Update')
    console.log(this.TagUpdate);
    this.all.changeLoader(false)
  }

  update(){
    this.all.changeLoader(true)
    if(this.TagUpdate == ''){
      this.showToast('danger',"Update Tag",'Failure Value Cant be Empty' )
      this.all.changeLoader(false)
    }else{
      this.all.Tags(this.selectedTag_id,'update',this.TagUpdate).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      console.log("message is: ", result.message);
      if(result.message == 'Tag updated')
      {
        this.all.Tags('','read','').subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          this.TagGroup = result.description
          this.showToast('success',"Update Tag",'Update Tag Successfully')
        })
      }else if(result.message == "Already Exist"){
        this.showToast('danger',"Update Tag",'This name of tag already exist')
      }else{
        // handle error
        this.showToast('danger',"Update Tag",'Update Tag Failure Retry')
      }
      this.selectedTag_id=null;
      this.TagUpdate="";
      this.all.changeLoader(false)
    })
  }
  }
  resetUpdate(){
    this.selectedTag_id=null;
    this.TagUpdate="";
    this.showToast('info',"Reset",'Reset Update Inbox Successfully')
  }

  undo(tag_id){
    console.log(tag_id)
    this.all.changeLoader(true)
    this.all.Tags(tag_id,'undo','').subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.message == 'Tag Undo')
      {
        this.all.Tags('','read','').subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          this.TagGroup = result.description
          this.all.Tags('','readDeleted','').subscribe(data => {
            let result = JSON.parse(JSON.stringify(data));
            this.TagBeenDeleted = result.description;
            this.all.changeLoader(false)
            this.showToast('success',"Undo Tag",'Undo Tag Successfully')
          })
        })
      }else{
        // handle error
        this.showToast('danger',"Undo Tag",'Undo Tag Failure Retry')
      }
    })
  }

}
