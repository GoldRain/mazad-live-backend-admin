import { Component, OnInit } from '@angular/core';
import { NgbModal,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AllService } from '../all.service';
import { Router,ActivatedRoute } from '@angular/router';
import { NbToastrService } from '@nebular/theme';

const foo = { DocImg: "" };
@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  toggleManage:boolean=false;
  toggleFollower:boolean=false;
  toggleFollowing:boolean=false;
  toggleBlock:boolean=true;
  toggleIban:boolean=false;
  toggleVerification:boolean=false;
  toggleBadgeControlSet:boolean=false; //togSetVerif()
  toggleProfile:boolean=false; 
  toggleVerifyStatus:boolean=null;
  toggleLoader:boolean=true;
  FollowerNumber;
  FollowingNumber;
  PostNumber=0;
  StoryNumber=0;
  countryInfo;
  verifyReqOrNot="";
  user:any;
  id;
  admin_id;
  toggleSuccessManage:boolean=false;
  toggleErrorManage:boolean=false;
  BadgeError:boolean=false;
  ManageErrorMsg='Something went wrong';
  manageType="Nothing is selected";
  manageNumber=0;
  manageTitle="Nothing is selected";
  checkBadge=false;
  profilePicError:boolean=false;
  constructor(private all: AllService,private modalService: NgbModal,private route: ActivatedRoute,private router: Router,private toastrService: NbToastrService) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
      this.id = this.route.snapshot.paramMap.get('id');
      console.log(this.id);
      this.all.userDetail(this.id).subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        this.user = result.description[0];
        this.FollowerNumber=this.user.followers.length;
        this.FollowingNumber=this.user.following.length;
        this.countryInfo=this.user.countryDetails[0];
        this.toggleBlock = this.user.is_admin_block;
        console.log(this.user.verification);
        if(this.user.verification == undefined){
          this.verifyReqOrNot="No Record";
          this.toggleVerifyStatus=null;
        }else{
        if(this.user.verification.status==undefined){
          this.verifyReqOrNot="No Record";
          this.toggleVerifyStatus=null;
        }else if(this.user.verification.status=="pending"){
          this.verifyReqOrNot="Pending";
          this.toggleVerifyStatus=false;
        }else if(this.user.verification.status=="approved"){
          this.verifyReqOrNot="Approved";
          this.toggleVerifyStatus=true;
        }
      }
        this.checkBadge=this.user.is_verified;
        if(this.user.verification == undefined){

        }else{
          foo.DocImg=this.user.verification.image_url;
        }
      //  foo.DocImg=this.user.verification.image_url;
        this.admin_id=localStorage.getItem('admin_id');
        // console.log(">>>",this.toggleBlock);
        this.all.changeLoader(false)
        console.log("hello",this.user);
      })
      this.all.getPostNo(this.id).subscribe(data=>{
        let result = JSON.parse(JSON.stringify(data));
        this.PostNumber = result.description.length;
      })
      this.all.userStoriesCount(this.id).subscribe(data=>{
        let result = JSON.parse(JSON.stringify(data));
        console.log(result)
        if(result.message.length!=0)
        {this.StoryNumber = result.message[0].count;}
        else{
          this.StoryNumber = 0
        }
      })
      
      sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  openBackDropCustomClass(content) {
    this.modalService.open(content, {backdropClass: 'light-blue-backdrop'});
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  open() {
    this.modalService.open(NgbdModal1Content, { size: 'lg' });
  }

  manage(){
    this.toggleFollower=false;
    this.toggleFollowing=false;
    this.toggleIban=false;
    this.toggleVerification=false;
    this.toggleManage=true;
    this.toggleBadgeControlSet=false
    this.toggleProfile=false;
  }

  FolloweOr(){
    this.toggleFollower=true;
    this.toggleFollowing=false;
    this.toggleIban=false;
    this.toggleVerification=false;
    this.toggleManage=false;
    this.toggleBadgeControlSet=false
    this.toggleProfile=false;
  }
  FollwingOr(){
    this.toggleFollower=false;
    this.toggleFollowing=true;
    this.toggleIban=false;
    this.toggleVerification=false;
    this.toggleManage=false;
    this.toggleBadgeControlSet=false
    this.toggleProfile=false;
  }

  togIban(){
    this.toggleFollower=false;
    this.toggleFollowing=false;
    this.toggleIban=true;
    this.toggleVerification=false;
    this.toggleManage=false;
    this.toggleBadgeControlSet=false
    this.toggleProfile=false;
  }

  togVerification(){
    this.toggleFollower=false;
    this.toggleFollowing=false;
    this.toggleIban=false;
    this.toggleVerification=true;
    this.toggleManage=false;
    this.toggleBadgeControlSet=false
    this.toggleProfile=false;
  }

  togSetVerif(){
    this.toggleFollower=false;
    this.toggleFollowing=false;
    this.toggleIban=false;
    this.toggleVerification=false;
    this.toggleManage=false;
    this.toggleBadgeControlSet=true
    this.toggleProfile=false;
  }

  togProfile(){
    this.toggleFollower=false;
    this.toggleFollowing=false;
    this.toggleIban=false;
    this.toggleVerification=false;
    this.toggleManage=false;
    this.toggleBadgeControlSet=false;
    this.toggleProfile=true;
  }

  showToast(status,heading,msg) {
    this.toastrService.show(msg, `${heading}`, { status });
  }

  removeProfilePicAction(user_id){
    this.all.changeLoader(true)
    this.all.removeProfilePhoto(user_id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.error == false){
        this.profilePicError=false;
        window.location.reload();
        this.all.changeLoader(false)
      }else{
        this.profilePicError=true;
        this.all.changeLoader(false)
      }
    })
  }

  ActionProceed(){
    this.all.changeLoader(true)
    console.log("checked",this.checkBadge);
    this.all.SetBadge(this.id,this.checkBadge).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.error == false)
      {
        this.BadgeError=false;
        this.all.changeLoader(false)
        window.location.reload();
      }else{
        this.BadgeError=true;
        this.all.changeLoader(false)
      }
    })
  }

  fnBlock(){
    this.all.changeLoader(true)
    this.toggleBlock = !this.toggleBlock;
    console.log(this.toggleBlock);
    this.admin_id = localStorage.getItem("admin_id");
    console.log("this",this.admin_id);
    this.all.adminBlock(this.admin_id,this.id,this.toggleBlock).subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      if(result.message == "Success"){
        this.toggleBlock = this.toggleBlock;
        if(this.toggleBlock == true)
        {
          this.showToast('success',"Action Block",'Success')
        }
        else{
          this.showToast('success',"Action Unblock",'Success')
        }
      }else{
        this.toggleBlock = !this.toggleBlock;
        this.showToast('danger',"Action",'Failure')
      }
      this.all.changeLoader(false)
    })
  }

  postManage(){
    this.manageType="post"
    this.manageTitle="Enter Post Count";
    this.toggleSuccessManage=false;
    this.toggleErrorManage=false;
  }

  storyManage(){
    this.manageType="story"
    this.manageTitle="Enter Story Count";
    this.toggleSuccessManage=false;
    this.toggleErrorManage=false;
  }

  liveManage(){
    this.manageType="live"
    this.manageTitle="Enter Live Count";
    this.toggleSuccessManage=false;
    this.toggleErrorManage=false;
  }

  doneManage(){
    console.log("hello");
    this.all.UpdateCount(this.id,this.manageNumber,this.manageType).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.error == false)
      {
        this.toggleSuccessManage=true;
        this.toggleErrorManage=false;
      }else{
        this.toggleSuccessManage=false;
        this.toggleErrorManage=true;
        this.ManageErrorMsg=result.message;
      }
    })
  }

  rejects(id){
    this.all.changeLoader(true)
    let user_id=id;
    this.all.verificationAction(user_id,"reject",this.admin_id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.error == false){
        this.all.changeLoader(false)
        window.location.reload();
      }
    })
  }

  accepts(id){
    console.log(id);
    this.all.changeLoader(true)
    let user_id=id;
    this.all.verificationAction(user_id,"approved",this.admin_id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.error == false){
        this.all.changeLoader(false)
        window.location.reload();
      }
    })
  }

}

@Component({
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Document</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <img [src]=ar alt="" class="col-lg-12 col-md-12 col-sm-12" style="border-radius: 25px;height:600px;object-fit: cover">
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModal1Content  implements OnInit {
  constructor(private modalService: NgbModal, public activeModal: NgbActiveModal) {}
  ar;
  ngOnInit() {
    if(foo.DocImg==""){
      this.ar="https://www.industowers.com/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png";
    }
    else{
      this.ar=foo.DocImg
    }
  }
}
