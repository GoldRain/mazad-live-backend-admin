import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AllService } from '../all.service';

@Component({
  selector: 'app-user-password',
  templateUrl: './user-password.component.html',
  styleUrls: ['./user-password.component.css']
})
export class UserPasswordComponent implements OnInit {

  detail;
  password="";
  confirm="";
  id;
  admin=true;
  answer;
  show=true;
  no=true;
  toggleLoader:boolean=true;
  constructor(private all: AllService,private route: ActivatedRoute) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.all.userDetail(this.id).subscribe(data=>{
      let result = JSON.parse(JSON.stringify(data));
      this.detail = result.description[0];
      this.all.changeLoader(false)
    });
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  onSubmit(){
    this.all.changeLoader(true)
    if(this.password.length<8)
    {
      this.answer = "something went wrong";
      this.no = false; 
      console.log("out");
      this.show=true;
      this.all.changeLoader(false)
      return false;
    }
    // if(this.password==this.confirm)
    if(this.password.localeCompare(this.confirm)==0 ){
      console.log("In")
      this.all.changeUserPassword(this.id,this.password,this.admin).subscribe(data=>{
        let result = JSON.parse(JSON.stringify(data));
        this.answer = result.message;
        this.show = result.error;
        this.no = true;
        this.all.changeLoader(false)
        this.password="";
        this.confirm="";
      })
    }
  }

  
}
