import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserVarifiedComponent } from './user-varified.component';

describe('UserVarifiedComponent', () => {
  let component: UserVarifiedComponent;
  let fixture: ComponentFixture<UserVarifiedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserVarifiedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserVarifiedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
