import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';

@Component({
  selector: 'app-user-varified',
  templateUrl: './user-varified.component.html',
  styleUrls: ['./user-varified.component.css']
})
export class UserVarifiedComponent implements OnInit {

  list;
  page=0;
  toggleWarning:boolean=false;
  userFilter="";
  toggleLoader:boolean=true;

  constructor(private all:AllService) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.all.userListVerified(0).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        console.log(result);
        this.all.changeLoader(false)
    })

    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }

  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
    if(this.userFilter != "")
    {
      this.all.userListVerifiedRegex(this.userFilter,a).subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
          if(this.list.length==0){
            this.toggleWarning=true;
          }else{
            this.toggleWarning=false;
          }
          this.all.changeLoader(false)
      })
    }
    else{
    this.all.userListVerified(a).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
    })
  }
}
  }

  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;
    if(this.userFilter != "")
    {
      this.all.userListVerifiedRegex(this.userFilter,a).subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
        if(result.description.length==0){
          a--;
          this.page--;
        }else{
          this.list = result.description;
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })
    }
    else{
    this.all.userListVerified(a).subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      if(result.description.length==0){
        a--;
        this.page--;
      }else{
        this.list = result.description;
        this.toggleWarning=false;
      }
      this.all.changeLoader(false)
    })
  }
  }

  filter(){
    this.all.changeLoader(true)
    this.all.userListVerifiedRegex(this.userFilter,0).subscribe(data =>{
      let result = JSON.parse(JSON.stringify(data));
      this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
    })
  }

  reset(){
    this.all.changeLoader(true)
    this.userFilter="";
    this.page=0;
    this.all.userListVerified(0).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
    })
  }

}
