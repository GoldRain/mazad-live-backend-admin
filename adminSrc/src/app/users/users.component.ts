import { Component, OnInit } from '@angular/core';
import { AllService } from '../all.service';
import { ActivatedRoute,Router,ParamMap } from "@angular/router";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  name=null;
  list;
  page=0;
  toggleWarning:boolean=false;
  toggleLoader:boolean=true;
  userFilter="";
  constructor(private all:AllService,private route: ActivatedRoute,private router: Router) {
    this.all.changeLoader(true)
   }

  ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('id');

    if(this.name != null)
    {
      this.all.deviceList(this.page,this.name).subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })
    }else{
      this.all.userList("0").subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
        // this.countUser=this.list.length;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
        console.log(result);
      })

    }

    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
  }
  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
    if(this.userFilter!="")
    {
      if(this.name != null)
    {
      this.all.deviceListRegex(this.userFilter,this.page,this.name).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })
    }else{
      this.all.userListRegex(this.userFilter,a).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
          if(this.list.length==0){
            this.toggleWarning=true;
          }else{
            this.toggleWarning=false;
          }
          this.all.changeLoader(false)
      })
    }
    }
    else{
    if(this.name != null)
    {
      this.all.deviceList(this.page,this.name).subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })

    }else{

      this.all.userList(a).subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })

    }
  }
}
}

  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;

    if(this.userFilter!="")
    {
      if(this.name != null)
      {
        this.all.deviceListRegex(this.userFilter,this.page,this.name).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          if(result.description.length==0){
            a--;
            this.page--;
          }else{
            this.list = result.description;
            this.toggleWarning=false;
          }
          this.all.changeLoader(false)
        })
      }else{
        this.all.userListRegex(this.userFilter,a).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          this.list = result.description;
          if(result.description.length==0){
            a--;
            this.page--;
          }else{
            this.list = result.description;
            this.toggleWarning=false;
          }
          this.all.changeLoader(false)
        })
      }
    }else{
    if(this.name != null)
    {
      this.all.deviceList(this.page,this.name).subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        
        if(result.description.length==0){
          a--;
          this.page--;
        }else{
          this.list = result.description;
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })

    }else{

      this.all.userList(a).subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        
        if(result.description.length==0){
          a--;
          this.page--;
        }else{
          this.list = result.description;
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })

    }
  }
  }

  filter(){
    this.all.changeLoader(true)
    if(this.name != null)
    {
      this.all.deviceListRegex(this.userFilter,0,this.name).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })
    }else{
      this.all.userListRegex(this.userFilter,0).subscribe(data => {
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
          if(this.list.length==0){
            this.toggleWarning=true;
          }else{
            this.toggleWarning=false;
          }
          this.all.changeLoader(false)
      })
    }
  }

  reset(){
    this.all.changeLoader(true)
    this.userFilter="";
    this.page=0;
    if(this.name != null)
    {
      this.all.deviceList(this.page,this.name).subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
      })

    }else{

      this.all.userList("0").subscribe(data =>{
        let result = JSON.parse(JSON.stringify(data));
        this.list = result.description;
        // this.countUser=this.list.length;
        if(this.list.length==0){
          this.toggleWarning=true;
        }else{
          this.toggleWarning=false;
        }
        this.all.changeLoader(false)
        console.log(result);
      })

    }
  }
}
