import { Component, OnInit} from '@angular/core';
import { AllService } from '../all.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {

  constructor(private all:AllService,private modalService: NgbModal,private toastrService: NbToastrService) {
    this.all.changeLoader(true)
   }

  list;
  page=0;
  toggleWarning:boolean=false;
  admin_id;
  actionApproved:boolean=false;
  actionRejected:boolean=false;
  toggleLoader:boolean=true;
  temp=0;

  ngOnInit() {
    this.all.verificationList(0).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      console.log(">>>>>>> ", result)
      this.list = result.description;
      if(this.list.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      this.all.changeLoader(false)
    })
    sessionStorage.removeItem('confirm-otp');
    sessionStorage.removeItem('admin-password');
    this.admin_id=localStorage.getItem('admin_id');
    console.log(this.admin_id)
  }

  decPage(){
    this.all.changeLoader(true)
    let a = --this.page;
    if(a==-1)
    {
      this.all.changeLoader(false)
      this.page=0;
      a=0;
    }else{
    this.all.verificationList(this.page).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      this.list = result.description;
      if(this.list.length==0){
        this.toggleWarning=true;
      }else{
        this.toggleWarning=false;
      }
      this.all.changeLoader(false)
    })
  }
  }

  incPage(){
    this.all.changeLoader(true)
    let a = ++this.page;
    this.all.verificationList(this.page).subscribe(data => {
    let result = JSON.parse(JSON.stringify(data));
    if(result.description.length==0){
      a--;
      this.page--;
    }else{
      this.list = result.description;
      this.toggleWarning=false;
    }
    this.all.changeLoader(false)
    })
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  showToast(status,heading,msg) {
    this.toastrService.show(msg, `${heading}`, { status });
  }

  waiting(){
    this.showToast('warning',"Waiting",'Waiting For Tap Payment')
  }

  accepts(id){
    this.all.changeLoader(true)
    let user_id=id;
    this.all.verificationAction(user_id,"approved",this.admin_id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.error == false){
        this.actionApproved=true;
        this.actionRejected=false;
        this.showToast('success',"Verification",'Verification Accepted Successful')
        this.all.verificationList(this.page).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          this.list = result.description;
          if(this.list.length==0){
            let a = --this.page;
            if(a==-1)
            {
              this.page=0;
              a=0;
            }
            this.all.verificationList(this.page).subscribe(data => {
              let result = JSON.parse(JSON.stringify(data));
              this.list = result.description;
              if(this.list.length==0){
                this.toggleWarning=true;
              }else{
                this.toggleWarning=false;
              }
            })
          }else{
            this.toggleWarning=false;
          }
          this.all.changeLoader(false)
        })
      }else{
        this.showToast('danger',"Verification",'Accepting Failed Retry')
      }
    })
  }

  rejects(id){
    this.all.changeLoader(true)
    let user_id=id;
    this.all.verificationAction(user_id,"reject",this.admin_id).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data));
      if(result.error == false){
        this.actionRejected=true;
        this.actionApproved=false;
        this.showToast('success',"Verification",'Verification Rejected Successful')
        this.all.verificationList(this.page).subscribe(data => {
          let result = JSON.parse(JSON.stringify(data));
          this.list = result.description;
          if(this.list.length==0){
            let a = --this.page;
            if(a==-1)
            {
              this.page=0;
              a=0;
            }
            this.all.verificationList(this.page).subscribe(data => {
              let result = JSON.parse(JSON.stringify(data));
              this.list = result.description;
              if(this.list.length==0){
                this.toggleWarning=true;
              }else{
                this.toggleWarning=false;
              }
            })
          }else{
            this.toggleWarning=false;
          }
          this.all.changeLoader(false)
        })
      }else{
        this.showToast('danger',"Verification",'Verification Rejected Failed Retry')
      }
    })
  }

}
