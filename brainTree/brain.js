var braintree = require("braintree");

var gateway = braintree.connect({
    environment: braintree.Environment.Sandbox,
    merchantId: "z56kcp9bxkt7858k",
    publicKey: "hydd2q8t3ffp23dq",
    privateKey: "cdb473e182b85a85d8cdd53090abda74"
});

exports.client_token = function (req, res) {
    console.log("client_token")
    gateway.clientToken.generate({}, function (err, response) {
        res.send({ token: response.clientToken });
    });
}

exports.checkout = function (req, res) {
    var nonceFromTheClient = req.body.payment_method_nonce;
    // var amount = req.body.amount;
    // Use payment method nonce here
    gateway.transaction.sale({
        amount: "10.00",
        paymentMethodNonce: nonceFromTheClient,
        options: {
            submitForSettlement: true
        }
    }, function (err, result) {
        if(err) {
            res.json({ error: true, message: "Something went wrong while generating the payment.", description: err })
        } else {
            res.json({ error: false, message: "Done", description: result });
        }
    });
}