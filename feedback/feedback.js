var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var schedule = require('node-schedule');

Post = require('../models/posts');
Funn = require('../functions/function');
Order = require('../models/orders');
Story = require('../models/story');

exports.pendingFeedbackOrder = function (req, res) {                                // TO RETURN THE POSTS, STORIES AND STREAM ON WHICH BUYER FEEDBACK IS PENDING
    let user_id = req.body.user_id;
    Order.find({ user_id: ObjectId(user_id), feedback: 'pending' }, function (error, alldata) {
        if (error) {
            res.json({ error: true, message: "Error while getting pending order list", description: error });
        } else if (alldata.length == 0) {
            res.json({ error: true, message: "No data found" });
        } else {
            let wholeData = [];
            for (let i = 0; i < alldata.length; i++) {
                console.log("i: ", i)
                let type = alldata[i].type;

                if (type == "post") {
                    type = "allposts";
                } else if (type == "story") {
                    type = "stories";
                } else if (type == "stream") {
                    type = "streams";
                }

                console.log("type is here  >>> ", type);
                Order.aggregate([{ $match: { _id: ObjectId(alldata[i]._id) } },
                { $lookup: { from: type, localField: 'type_id', foreignField: "_id", as: "type_data" } },
                { $lookup: { from: "users", localField: 'seller_id', foreignField: "_id", as: "sellerDetails" } }], function (error, data) {
                    if (error) {
                        console.log("1111111111111 and i is ", i, " and err is ", error);

                    } else if (data.length == 0) {
                        console.log("2")

                    } else {
                        console.log("3")

                        wholeData.push(data[0]);
                        if (wholeData.length == alldata.length) {
                            console.log("aya")
                            res.json({ error: false, message: "Done", description: wholeData })
                        }
                    }
                })
                console.log("length:", alldata.length)
            }
        }
    })
};

exports.addFeedbackOnPost = function (req, res) {                                   //  BUYER GAVE HIS FEEDBACK ON HIS ORDERS
    let type_id = req.body.type_id, user_id = req.body.user_id, rating = req.body.rating, comment = req.body.comment, order_id = req.body.order_id, time = new Date().getTime();

    Order.findOne({ _id: ObjectId(order_id) }, function (error, orderDetails) {
        if (error) {
            res.json({ error: true, message: "Error while getting order details", description: error });
        } else if (!orderDetails) {
            res.json({ error: true, message: "No order found" })
        } else {
            if (orderDetails.feedback == "pending") {
                Funn.userDetails(user_id, (userData) => {
                    let userDetails = userData.description;
                    console.log(">>>>>> ", userDetails)
                    Funn.userCountry(userDetails.country_code, (country) => {
                        let userCountry = country.description;
                        let obj = {
                            user_id: ObjectId(user_id), user_name: userDetails.username, user_country: userCountry, user_profile: userDetails.profilePhoto, comment: comment,
                            order_id: ObjectId(order_id), created_at: time, rating: rating
                        }
                        Post.findOneAndUpdate({ _id: ObjectId(type_id) }, { $push: { feedback: obj } }, function (error, data) {
                            if (error) {
                                res.json({ error: true, message: "Error while adding feedback on a post", description: error });
                            } else if (!data) {
                                res.json({ error: true, message: "Post not found" });
                            } else {
                                Order.findOneAndUpdate({ _id: ObjectId(order_id) }, { feedback: "Done" }, function (error, orderDetails2) {
                                    if (error) {
                                        ({ error: true, message: "Error while getting order details but feedback done", description: error });
                                    } else if (!orderDetails2) {
                                        res.json({ error: true, message: "No order found but feedback done" })
                                    } else {
                                        Funn.postDetails(type_id, (postData) => {
                                            res.json(postData);
                                        })

                                    }
                                })
                            }
                        })
                    })
                });
            } else {
                res.json({ error: true, message: "You have already given your feedback on this post." })
            }
        }
    })

};

exports.addFeedbackOnStory = function (req, res) {                                  //  TO GAVE FEEDBACK BY BUYER ON STORIES
    let id = req.body.type_id, user_id = req.body.user_id, rating = req.body.rating, comment = req.body.comment, order_id = req.body.order_id, time = new Date().getTime();

    Order.findOne({ _id: ObjectId(order_id), feedback: 'pending' }, function (error, orderDetails) {
        if (error) {
            res.json({ error: true, message: "Error while getting order details", description: error });
        } else if (!orderDetails) {
            res.json({ error: true, message: "You have already given your feedback on this post" })
        } else {
            Funn.userDetails(user_id, (userData) => {
                let userDetails = userData.description;
                console.log(">>>>>> ", userDetails)
                Funn.userCountry(userDetails.country_code, (country) => {
                    let userCountry = country.description;
                    let obj = {
                        user_id: ObjectId(user_id), user_name: userDetails.username, user_country: userCountry, user_profile: userDetails.profilePhoto, comment: comment,
                        order_id: ObjectId(order_id), created_at: time, rating: rating
                    }
                    Story.findOneAndUpdate({ _id: ObjectId(id) }, { $push: { feedback: obj } }, function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while adding feedback on a post", description: error });
                        } else if (!data) {
                            res.json({ error: true, message: "Post not found" });
                        } else {
                            Order.findOneAndUpdate({ _id: ObjectId(order_id) }, { feedback: "Done" }, function (error, orderDetails2) {
                                if (error) {
                                    ({ error: true, message: "Error while getting order details but feedback done", description: error });
                                } else if (!orderDetails2) {
                                    res.json({ error: true, message: "No order found but feedback done" })
                                } else {
                                    Funn.storyDetails(id, (storyData) => {
                                        res.json(storyData);
                                    })
                                }
                            })
                        }
                    })
                })
            });
        }
    })
};

exports.feedbackReceived = function (req, res) {                                    //  TO RETURN THE FEEDBACKS ON POSTS AND STORIES TO SELLER
    console.log("feedbackReceived");
    let user_id = req.body.user_id, filteredPosts = [], filteredStories = [], send = true;
    Post.find({ post_user_id: ObjectId(user_id) }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting all psot", description: error });
        } else if (data.length == 0) {
            res.json({ error: true, message: "No data found" });
        } else {
            for (var i = 0; i < data.length; i++) {
                if (data[i].feedback.length > 0) {
                    filteredPosts.push(data[i])
                }
                if (i == data.length - 1) {
                    console.log("in the stories")
                    Story.find({ user_id: ObjectId(user_id) }, function (error, story) {
                        if (error) {
                            console.log({ error: true, message: "Error after getting all the posts with count > 1", description: error })
                        } else if (story.length == 0) {
                            res.json({ error: false, message: "Done but stories not found", posts: filteredPosts });
                        } else {
                            for (let j = 0; j < story.length; j++) {
                                console.log("stories got")
                                if (story[j].feedback.length > 0) {
                                    filteredStories.push(story[j])
                                }
                                if (j == story.length - 1) {
                                    console.log("response send")
                                    res.json({ error: false, message: "Done", posts: filteredPosts, stories: filteredStories })
                                }
                            }
                        }
                    })
                }
            }
        }
    })
};