var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var schedule = require('node-schedule');

Send = require('../notification/notify');
Notify = require('../models/notification');
Country = require('../models/countries');
Post = require('../models/posts');
Story = require('../models/story');
Wallet = require('../models/paymentRequests');
Admin = require('../models/admin');
Order = require('../models/orders');
Otp = require('../models/otps');

const curl = new (require('curl-request'))();
const nodemailer = require('nodemailer');

const accountSid = 'AC288df15a07e81a44c24a3a156f416da0';
const authToken = '01c9e6b8d8e1d9cec4f03ee8c4d98d8d';
const client = require('twilio')(accountSid, authToken);


exports.notification = function (sender_id, receiver_id, msg, type, typeDetails) { //  TO GENERATE NOTIFICATION 
    console.log("function called id1: ", sender_id, " - id2: ", receiver_id, " type:", type, " message: ", msg)
    let message = String;
    User.findOne({ _id: ObjectId(sender_id) }, function (error, result) {
        if (!error) {
            message = result.username + ' ' + msg;
            let notify = new Notify({
                message: message,
                sender_id: sender_id,
                receiver_id: receiver_id,
                type: type,
                typeDetails: typeDetails
            });

            notify.save(function (error, data) {
                if (error) {
                    console.log("Something went wrong will generating notification", error);
                    // res.json({ error: true, message: "Something went wrong will generating notification", description: error });
                } else {
                    // console.log("Data saved.");
                    User.findOne({ _id: ObjectId(receiver_id) }, function (error, user) {
                        if (error) {
                            console.log('Error after notification generated', error)
                        } else if (!user) {
                            console.log("User not found but notification generated.")
                        } else {
                            // console.log("userFound: ", user)
                            if (user.playerId != "" && user.playerId != null && user.is_online == false) {
                                // console.log("check checked")
                                Notify.findOne({ _id: ObjectId(data._id) }, function (error, finalData) {
                                    if (error) {
                                        console.log("Notification generated user found but Error while getting notification data.")
                                    } else {
                                        // console.log("userFound")
                                        let isNotification = user.push_notification;
                                        // console.log("push notification : ", isNotification)
                                        if (isNotification == true) {
                                            let pid = user.playerId;
                                            let type = user.user_type;
                                            // console.log("pid: ", pid, " msg: ", data)
                                            if (type == "android") {
                                                // console.log("android notification function called in function.js on line 50")
                                                Send.sendNotification([pid], data);
                                            } else {
                                                // console.log("IOS function of notification called")
                                                Send.sendNotificationForIOS([pid], data, data.message, "message", data.type);
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    })
                }
            })
        } else {
            console.log("Error in notification.")
        }
    })

};

exports.adminNotification = function (id, receiver_id, msg, type, typeDetails) { //  TO GENERATE NOTIFICATION 
    console.log("function called id1: ", id, " - id2: ", receiver_id)
    let message = String;
    Admin.findOne({ _id: ObjectId(id) }, function (error, result) {
        if (!error) {
            message = result.username + ' ' + msg;
            let notify = new Notify({
                message: message,
                sender_id: id,
                receiver_id: receiver_id,
                type: type,
                typeDetails: typeDetails
            });

            notify.save(function (error, data) {
                if (error) {
                    console.log("Something went wrong will generating notification", error);
                    // res.json({ error: true, message: "Something went wrong will generating notification", description: error });
                } else {
                    console.log("Data saved.");
                    User.findOne({ _id: ObjectId(receiver_id) }, function (error, user) {
                        if (error) {
                            console.log('Error after notification generated', error)
                        } else if (!user) {
                            console.log("User not found but notification generated.")
                        } else {
                            // console.log("userFound: ", user)
                            if (user.playerId != "" && user.playerId != null && user.is_online == false) {
                                console.log("check checked")
                                Notify.findOne({ _id: ObjectId(data._id) }, function (error, finalData) {
                                    if (error) {
                                        console.log("Notification generated user found but Error while getting notification data.")
                                    } else {
                                        console.log("userFound")
                                        let isNotification = user.push_notification;
                                        console.log("push notification : ", isNotification)
                                        if (isNotification == true) {
                                            let pid = user.playerId;
                                            let type = user.user_type;
                                            console.log("pid: ", pid, " msg: ", data)
                                            if (type == "android") {
                                                console.log("android notification function called in function.js on line 121")
                                                Send.sendNotification([pid], data);
                                            } else {
                                                console.log("IOS function of notification called")
                                                Send.sendNotificationForIOS([pid], data, data.message, "message", data.type);
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    })
                    // res.json({ error: false, message: "Data saved.", description: data });
                }
            })
        } else {
            console.log("Error in notification.")
        }
    })

};

exports.activity = function (user_id, msg, type, type_id) { //    TO GENERATE ALL THE ACTIVITIES OF A USER 
    console.log("activity function called")

    User.findOne({ _id: ObjectId(user_id) }, function (error, data) {
        if (data.is_activity == true) {
            if (type == "post") {
                let time = new Date().getTime();
                let activity = new Activity({
                    message: msg,
                    user_id: ObjectId(user_id),
                    type: type,
                    type_id: type_id,
                    created_at: time
                });

                activity.save(function (error, data) {
                    if (error) {
                        console.log("Something went wrong will saving activity in the database.", error);
                    } else {
                        console.log("activity recorded.");
                    }
                })
            }
        }
    })
};

exports.otp = function (country_code, email, phone, req, res) { //  TO SEND OTP
    console.log("sendOtp countryCode: ", country_code, " - email: ", email, " - phone: ", phone)
    let otp = Math.floor(100000 + Math.random() * 900000)

    Otp.remove({ phone: phone }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while deleting previous otps", description: error });
        } else if (data.length == 0) {
            send();
        } else {
            send();
        }
    })

    function send() {
        console.log("check in send function");
        // let otp = 123456;
        let registerOTP = new Otp({
            country_code: country_code,
            email: email,
            phone: phone,
            otp: otp
        })

        phoneNumber = "+" + country_code + phone;

        registerOTP.save(function (err, otpData) {
            if (err) {
                res.json({ error: true, message: "Something went wrong, Please try again", data: err })
            } else {
                str = "Your OTP for mazad is: " + otp;
                // request               
                client.messages
                    .create({
                        body: str,
                        from: '+12519298355',
                        to: phoneNumber
                    })
                    .then(message => {
                        console.log(">>>>>>>>> ", message)
                    });

                res.json({ error: false, message: "OTP has been sent.", data: otpData })
            }
        })
    }
};

exports.sms = function (country_code, email, phone, msg) { //  TO SEND SMS
    console.log("sendOtp countryCode: ", country_code, " - email: ", email, " - phone: ", phone)

    str = msg;
    phoneNumber = "+" + country_code + phone;
    // request
    client.messages
        .create({
            body: str,
            from: '+12519298355',
            to: phoneNumber
        })
        .then(message => {
            console.log(">>>>>>>>> ", message)
        });
    console.log({ error: false, message: "SMS has been sent." })
};

exports.authenticate = function (req, res, done) { //  TO ADDITIONAL SECURITY
    if (req.header('API_KEY') == constant.API_KEY) {
        if (req.body.version == constant.VERSION) {
            done();
        } else {
            res.json({ error: true, message: "Your version of app is older, Please update" });
        }
    } else {
        res.json({ message: "Wrong API key" });
    }
};

exports.userDetails = function (id, done) { //  IT WILL GET ALL DETAILS OF A USER
    console.log("user details function called")
    User.findOne({ _id: ObjectId(id) }, function (error, data) {
        if (error) {
            done({ error: true, message: "Error in userDetails function", description: error });
        } else if (!data) {
            done({ error: true, message: "according to userDetails function data not found" });
        } else {
            done({ error: false, message: "Data found", description: data })
        }
    })
};

exports.userCountry = function (p1, done) { //  TO GET THE DETAILS OF A USER COUNTRY
    console.log("user country function called")
    Country.findOne({ countryCode: p1 }, function (error, data) {
        if (error) {
            done({ error: true, message: "Error in userCountry function", description: error });
        } else if (!data) {
            done({ error: true, message: "according to userCountry function data not found" });
        } else {
            console.log("country: ", data)
            done({ error: false, message: "Data found", description: data })
        }
    })
};

exports.postDetails = function (post_id, done) { //  TO GET THE DETAILS OF A POST
    console.log("postDetails function called: ", post_id)
    Post.findOne({ _id: ObjectId(post_id) }, function (error, data) {
        if (error) {
            console.log("aa")
            done({ error: true, message: "Error while getting post details from postDetails function", description: error })
        } else if (!data) {
            console.log("bb")
            done({ error: true, message: "Post not found from postDetails function" })
        } else {
            console.log("cc")
            done({ error: false, message: "Done", description: data });
            console.log("gone")
        }
    })
};

exports.storyDetails = function (story_id, done) { //  TO GET THE DETAILS OF STORY
    Story.findOne({ _id: ObjectId(story_id) }, function (error, data) {
        if (error) {
            done({ error: true, message: "Error while getting story details from storyDetails function", description: error })
        } else if (!data) {
            done({ error: true, message: "Story not found from storyDetails function" })
        } else {
            done({ error: false, message: "Done", description: data });
        }
    })
};

exports.changeWalletStatus = function (id, done) { //  TO CHANGE THE STATUS OF WALLED
    Wallet.findOneAndUpdate({ order_id: ObjectId(id) }, { status: "requested" }, function (error, data) {
        if (error) {
            done({ error: true, message: "Error while updating wallet status", description: error })
        } else if (!data) {
            done({ error: true, message: "Wallet not found" })
        } else {
            Wallet.findOne({ order_id: ObjectId(id) }, function (error, newdata) {
                if (error) {
                    done({ error: true, message: "Error after updating wallet status", description: error })
                } else if (!newdata) {
                    done({ error: true, message: "Wallet not found but updated" })
                } else {
                    done({ error: false, message: "Done", description: newdata });
                }
            })
        }
    })
};

exports.completePostDetails = function (id, done) {
    Post.aggregate([{ $match: { _id: ObjectId(id) } },
    { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
    { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
    { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } }
    ], function (err, data) {
        if (err) {
            done({ error: true, message: "Something went wrong in the function.", data: err });
        } else if (!data) {
            done({ error: false, message: 'No data found, user id might not be matched in the function' })
        } else {
            let post_like = [],
                feedback = [];

            for (let i = 0; i < data[0].post_like.length; i++) {
                // console.log("i: ", i)

                aboutUser(data[0].post_like[i].user_id, (userData) => {
                    // console.log("response: >>>>>>>>>>>>", userData)
                    let obj = { _id: ObjectId(data[0].post_like[i]._id), post_id: ObjectId(data[0].post_like[i].post_id), userDetails: userData }
                    post_like.push(obj);

                    if (i == data[0].post_like.length - 1) {
                        // console.log("inside")
                        console.log("length: ", data[0].feedback.length)
                        for (let j = 0; j < data[0].feedback.length; j++) {
                            aboutUser(data[0].feedback[j].user_id, (userData2) => {
                                aboutOrder(data[0].feedback[j].order_id, (orderData) => {
                                    let obj2 = {
                                        _id: data[0].feedback[j]._id,
                                        userDetails: userData2,
                                        comment: data[0].feedback[j].comment,
                                        order_info: orderData,
                                        created_at: data[0].feedback[j].created_at,
                                        rating: data[0].feedback[j].rating
                                    };
                                    feedback.push(obj2);

                                    if (j == data[0].feedback.length - 1) {
                                        let obj = {
                                            _id: data[0]._id,
                                            tags: data[0].tags,
                                            available_country_id: data[0].available_country_id,
                                            post_images: data[0].post_images,
                                            bids: data[0].bids,
                                            post_like: post_like,
                                            feedback: feedback,
                                            vote: data[0].vote,
                                            details: data[0].details,
                                            latitude: data[0].latitude,
                                            longitude: data[0].longitude,
                                            place_name: data[0].place_name,
                                            post_user_id: data[0].post_user_id,
                                            is_instagram: data[0].is_instagram,
                                            is_twitter: data[0].is_twitter,
                                            amount: data[0].amount,
                                            is_auction: data[0].is_auction,
                                            buy_now: data[0].buy_now,
                                            whatsapp_only: data[0].whatsapp_only,
                                            whatsapp_and_call: data[0].whatsapp_and_call,
                                            payment_type: data[0].payment_type,
                                            language: data[0].language,
                                            only_user_from_selected_country: data[0].only_user_from_selected_country,
                                            post_created_at: data[0].post_created_at,
                                            is_deleted: data[0].is_deleted,
                                            status: data[0].status,
                                            is_sold: data[0].status,
                                            is_story: data[0].is_story
                                        }
                                        done(obj)
                                    }
                                })
                            })
                        }
                    }
                })
            }
        }
    })
};

exports.stopStream = function (id, done) {
    Streams.update({ user_id: ObjectId(id), status: "running" }, { status: 'stopped' }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Something went wrong while fetching running streams of a user to stop.", description: error });
        } else if (data.length == 0) {
            res.json({ error: false, message: "No match found." });
        } else {
            for (let i = 0; i < data.length; i++) {
                console.log("in the loop")
                Streams.findOne({ _id: data[i]._id }, function (error, stream) {
                    if (error) {
                        console.log("Something went wrong while getting stream details in stopStream socket.", error);
                    } else if (!stream) {
                        console.log("Stream updated but after that might be stream not found.");
                    } else {
                        // res.json({ error: false, message: "Streams stopped" })
                        User.find(function (error, users) {
                            if (error) { } else if (users.length == 0) { } else {
                                let streamer
                                for (let i = 0; i < users.length; i++) {
                                    if (users[i]._id.toString() == data.user_id) {
                                        streamer = users[i];
                                    }
                                }
                                for (let i = 0; i < users.length; i++) {
                                    io.to(users[i].socket_id).emit("updateStream", { error: false, stream: stream, userDetails: streamer });
                                }
                                done()
                            }
                        })
                    }
                })
            }
            done(console.log("outside the loop"))
        }
    })
};

exports.updateCount = function (id, type, done) {
    User.findOne({ _id: ObjectId(id) }, function (error, userData) {
        if (error) {
            done({ error: true, message: "Error in userDetails function", description: error });
        } else if (!userData) {
            done({ error: true, message: "according to userDetails function data not found" });
        } else {
            let user = userData,
                count = 0;
            if (type == "post") {
                count = user.unlimited_post_count.count;
                if (!user.unlimited_post_count.status) {
                    if (count != 0) {
                        let updatedUnlimitedPostObject = { status: user.unlimited_post_count.status, activation: user.unlimited_post_count.activation, expiry: user.unlimited_post_count.expiry, count: --count }

                        User.findOneAndUpdate({ _id: ObjectId(id) }, { unlimited_post_count: updatedUnlimitedPostObject }, function (error, data) {
                            if (error) {
                                done({ error: true, message: "Error while updating count of post", description: error })
                            } else if (!data) {
                                done({ error: true, message: "Post not found" })
                            } else {
                                User.findOne({ _id: ObjectId(id) }, function (error, UpdateData) {
                                    if (error) {
                                        done({ error: true, message: "Error in userDetails function", description: error });
                                    } else if (!UpdateData) {
                                        done({ error: true, message: "according to userDetails function data not found" });
                                    } else {
                                        done({ error: false, message: "Done", description: UpdateData })
                                    }
                                })
                            }
                        })
                    }
                }
            } else if (type == "story") {
                count = user.unlimited_story_count.count;

                if (!user.unlimited_story_count.status) {
                    let updatedUnlimitedStoryObject = { status: user.unlimited_story_count.status, activation: user.unlimited_story_count.activation, expiry: user.unlimited_story_count.expiry, count: --count }
                    if (count != 0) {
                        User.findOneAndUpdate({ _id: ObjectId(id) }, { unlimited_story_count: updatedUnlimitedStoryObject }, function (error, data) {
                            if (error) {
                                done({ error: true, message: "Error while updating count of story", description: error })
                            } else if (!data) {
                                done({ error: true, message: "story not found" })
                            } else {
                                User.findOne({ _id: ObjectId(id) }, function (error, UpdateData) {
                                    if (error) {
                                        done({ error: true, message: "Error in userDetails function", description: error });
                                    } else if (!UpdateData) {
                                        done({ error: true, message: "according to userDetails function data not found" });
                                    } else {
                                        done({ error: false, message: "Done", description: UpdateData })
                                    }
                                })
                            }
                        })
                    }
                }

            } else {

                count = user.unlimited_live_count.count;

                if (!user.unlimited_live_count.status) {
                    let updatedUnlimitedStoryObject = { status: user.unlimited_live_count.status, activation: user.unlimited_live_count.activation, expiry: user.unlimited_live_count.expiry, count: --count }

                    if (count != 0) {
                        User.findOneAndUpdate({ _id: ObjectId(id) }, { unlimited_live_count: updatedUnlimitedStoryObject }, function (error, data) {
                            if (error) {
                                done({ error: true, message: "Error while updating count of stream", description: error })
                            } else if (!data) {
                                done({ error: true, message: "stream not found" })
                            } else {
                                User.findOne({ _id: ObjectId(id) }, function (error, UpdateData) {
                                    if (error) {
                                        done({ error: true, message: "Error in userDetails function", description: error });
                                    } else if (!UpdateData) {
                                        done({ error: true, message: "according to userDetails function data not found" });
                                    } else {
                                        done({ error: false, message: "Done", description: UpdateData })
                                    }
                                })
                            }
                        })
                    }
                }

            }
        }
    })
};

exports.updatePostCount = function (id, done) {
    this.postDetails(id, (postData) => {
        let post = postData.description;
        Post.findOneAndUpdate({ _id: ObjectId(id) }, { orders: ++post.orders }, function (error, data) {
            if (error) {
                done({ error: true, message: "Error while updating count of orders on post", description: error })
            } else if (!data) {
                done({ error: true, message: "Post not found" })
            } else {
                done({ error: false, message: "Done" });
            }
        })
    })
};

function convertBase64ToImage(imgString, username) { //  NOT WORKING BUT IMPORTANT
    // console.log("imageString: ",imgString, "  length is: ", imgString.length);
    // var base64Data = imgString.replace(/^data:image\/png;base64,/, "");
    // let fileName = "/images/" + username + "_" + Date.now() + ".jpg"
    // require("fs").writeFile(fileName, base64Data, 'base64', function (err) {
    //     console.log(err);
    // });
    // return fileName;
};

function aboutUser(id, done) {
    User.findOne({ _id: ObjectId(id) }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting the userDetails", description: error });
        } else if (!data) {
            done({ error: true, message: "No data found" });
        } else {
            done({ error: false, message: "Done", description: data })
        }
    })
};

function aboutOrder(id, done) {
    Order.findOne({ _id: ObjectId(id) }, function (error, data) {
        if (error) {
            done({ error: true, message: "Error while getting the order details", description: error });
        } else if (!data) {
            done({ error: true, message: "No data found", description: "" });
        } else {
            done({ data });
        }
    })
};

exports.update_post_count = function (user_id, obj, done) {
    // Fun.authenticate(req, res, () => {
    console.log("update_post_count");
    console.log(">>>>>> count: ", typeof (count))

    User.findOneAndUpdate({ _id: ObjectId(user_id) }, obj, function (error, data) {
        if (error) {
            done({ error: true, message: "Error while updating post count of user", description: error });
        } else if (!data) {
            done({ error: true, message: "No user found" });
        } else {
            User.findOne({ _id: ObjectId(user_id) }, function (error, data) {
                if (error) {
                    done({ error: true, message: "Error in userDetails function", description: error });
                } else if (!data) {
                    done({ error: true, message: "according to userDetails function data not found" });
                } else {
                    done({ error: false, message: "Data found", description: data })
                }
            })
        }
    })
    // })
};

exports.update_live_count = function (user_id, obj, done) {
    // Fun.authenticate(req, res, () => {
    console.log("update_post_count");
    User.findOneAndUpdate({ _id: ObjectId(user_id) }, obj, function (error, data) {
        if (error) {
            done({ error: true, message: "Error while updating post count of user", description: error });
        } else if (!data) {
            done({ error: true, message: "No user found" });
        } else {
            User.findOne({ _id: ObjectId(user_id) }, function (error, data) {
                if (error) {
                    done({ error: true, message: "Error in userDetails function", description: error });
                } else if (!data) {
                    done({ error: true, message: "according to userDetails function data not found" });
                } else {
                    done({ error: false, message: "Data found", description: data })
                }
            })
        }
    })
    // })
};

exports.update_story_count = function (user_id, obj, done) {
    // Fun.authenticate(req, res, () => {
    console.log("update_story_count");
    User.findOneAndUpdate({ _id: ObjectId(user_id) }, obj, function (error, data) {
        if (error) {
            done({ error: true, message: "Error while updating story count of user", description: error });
        } else if (!data) {
            done({ error: true, message: "No user found" });
        } else {
            User.findOne({ _id: ObjectId(user_id) }, function (error, data) {
                if (error) {
                    done({ error: true, message: "Error in userDetails function", description: error });
                } else if (!data) {
                    done({ error: true, message: "according to userDetails function data not found" });
                } else {
                    done({ error: false, message: "Data found", description: data })
                }
            })
        }
    })
    // })
};

exports.sendMail = function (id, text, title, orderId, done) {
    console.log("sendMail");
    // const output = `<p>` + text + `</p>`;
    let date = new Date();
    const output = `<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>VERIFICATION E-MAIL</title>
      <style type="text/css">
        #outlook a {padding:0;}
        body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
        #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
        p {margin:0;}
        a img {border:none;}
        .image_fix {display:block;}
      </style>
    </head>
    <body style="background-color: #F7F8FA;">
    
    <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#F7F8FA">
      <tr>
        <td valign="top" align="center">
        <table id="wrapper" style="max-width: 600px; padding:20px 10px;" cellpadding="0" cellspacing="0" border="0" width="100%">
    
          <tr><td valign="top" height="20"></td></tr>
          <tr>
            <td valign="top" align="center">
              
              <table style="
                border-left: 1px solid #ebebeb;
                border-right: 1px solid #ebebeb;
                border-bottom: 1px solid #ebebeb;
                border-radius: 0 0 5px 5px; background-size:200px 200px; background: whitesmoke"   cellpadding="0" cellspacing="0" border="0" width="100%" >
                <tr>
                  <td valign="top" align="center">
                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
                      <tr>
                        <td width="30"></td>
                        <td>
                          <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr><td valign="top" height="30"></td></tr>
                            <tr>
                              <td valign="top" align="center">
                                <h1 style="
                                font-family:'Avenir Next','Helvetica Neue', Helvetica, Arial, sans-serif;
                                  font-size: 22px;
                                  text-align: center;
                                  line-height: 1.5em;
                                  margin: 0 0 10px 0;
                                  font-weight: 400;
                                  color: #202020;">
                                  <b>Mazad Live</b>
                                </h1>
                              </td>
                            </tr>
                            <tr>
                              <td>
                              
                                  <span style="display: block; margin-bottom: 20px; text-align: center">
                                    <img style="box-shadow: 0 0 0 0 rgba(0,0,0,.4);" src="https://s3.us-east-2.amazonaws.com/mazadpostimages/adminImages/mazadlogo.png" width="100px" height="auto" >
                                  </span>
                                </a>
                              </td>
                            </tr>
                            <tr>
                              <td>

                                <p style="
                                  font-family:'Avenir Next','Helvetica Neue', Helvetica, Arial, sans-serif;
                                  font-size: 14px;
                                  line-height: 1.5em;
                                  margin: 0 0 20px 0;
                                  font-weight: 400;
                                  color: #303030;">
                                  <!-- Mail body -->
                                  ` + text + `
                                </p>
                              
                                 <p style="
                                  font-family:'Avenir Next','Helvetica Neue', Helvetica, Arial, sans-serif;
                                  font-size: 14px;
                                  line-height: 1.5em;
                                  margin: 0 0 40px 0;
                                  font-weight: 400;
                                  color: #303030;">
                                  <!-- Mail body -->
                                  Order id is: ` + orderId + `
                                </p> 

                                <p style="
                                  font-family:'Avenir Next','Helvetica Neue', Helvetica, Arial, sans-serif;
                                  font-size: 14px;
                                  line-height: 1.5em;
                                  margin: 0 0 40px 0;
                                  font-weight: 400;
                                  color: #303030;">
                                  <!-- Mail body -->
                                  Date:  ` + date + `
                                </p>

                              </td>
                            </tr>
                            
                            <tr><td valign="top" height="60"></td></tr>
                          </table>
                        </td>
                        <td width="30"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr><td valign="top" height="20"></td></tr>
          <tr>
            <td valign="top" align="center">
              
            </td>
          </tr>
        
    </table>
    <!-- End of wrapper table -->
    </body>
    </html>`

    User.findOne({ _id: ObjectId(id) }, function (error, data) {
        if (error) {
            done({ error: true, message: "Error in userDetails function", description: error });
        } else if (!data) {
            done({ error: true, message: "according to userDetails function data not found" });
        } else {
            let transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 465,
                secure: true, // true for 465, false for other ports
                auth: {
                    user: "alrabiahi053@gmail.com", // generated ethereal user
                    pass: "Ib27620683" // generated ethereal password
                },
                tls: {
                    // do not fail on invalid certs
                    rejectUnauthorized: false
                }
            });

            // setup email data with unicode symbols
            let mailOptions = {
                from: '"mazadAdmin" <alrabiahi053@gmail.com>', // sender address
                to: data.email, // list of receivers
                subject: title, // Subject line
                text: 'Hello world?', // plain text body
                html: output // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error);
                    done({ error: true, message: "Something wend wrong" });
                } else {
                    done({ error: false, message: "Success" });
                }
                // console.log('Message sent: %s', info.messageId);
                // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            });
        }
    })
};

exports.updateUnlimitedCount = function (user_id, obj, done) {
    console.log("updateUnlimitedCount");
    User.findOneAndUpdate({ _id: ObjectId(user_id) }, obj, function (error, data) {
        if (error) {
            done({ error: true, message: "Error while updating unlimited counts of user", description: error });
        } else if (!data) {
            done({ error: true, message: "No user found" });
        } else {
            User.findOne({ _id: ObjectId(user_id) }, function (error, data) {
                if (error) {
                    done({ error: true, message: "Error in userDetails function", description: error });
                } else if (!data) {
                    done({ error: true, message: "according to userDetails function data not found" });
                } else {
                    done({ error: false, message: "Data found", description: data })
                }
            })
        }
    })
}