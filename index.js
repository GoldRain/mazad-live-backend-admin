var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app);
var route = require('./route/route');
var socket = require('socket.io');
var streams = require('./socket/stream');
// var pathNode = require('path')
const path = require('path');
const v8 = require('v8');

// for ssl server
var https = require("https");
var fs = require("fs");


var privateKey = fs.readFileSync("sslcert/privateserver.key", "utf8");
var certificate = fs.readFileSync("sslcert/mycert.pem", "utf8");
var credentials = {
    key: privateKey,
    cert: certificate
};

var port = 8082;

app.use("/public", express.static(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, 'public/index')));
app.use(express.static(path.join(__dirname, 'public/mazadTerms')));
app.use(express.static(path.join(__dirname, 'public/mazadTermsArabic')));
app.use(express.static(path.join(__dirname, 'public/mazadPrivacy')));
app.use(express.static(path.join(__dirname, 'public/mazadPrivacyArabic')));

// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: true }))

app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:5000');
    // res.setHeader('Access-Control-Allow-Origin', 'http://18.218.27.17');
    res.setHeader('Access-Control-Allow-Origin', '*');
    // res.setHeader('Access-Control-Allow-Origin', 'http://mazadlive.com');                                                  //  STAR TO GO WILL ALL URL

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headerchange for ioss you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent  
    // to the API (e.g. in case you use sessions)    
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


// app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// app.use(express.bodyParser({limit: '50mb'}));
// console.log("limit is: ", limit);

// app.use(bodyParser.json({ limit: '100mb', extended: true }));
// // app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
// app.use(bodyParser.urlencoded({ limit: "100mb", extended: true, parameterLimit: 100000 }));

// bodyParser = {
//     json: { limit: '100mb', extended: true },
//     urlencoded: { limit: '100mb', extended: true }
// };


// app.use( bodyParser.json({limit: '50mb'}) );
// app.use(bodyParser.urlencoded({
//   limit: '50mb',
//   extended: true,
//   parameterLimit:50000
// }));

app.use(express.static(__dirname + '/images'));
app.use("/images", express.static(__dirname + '/images'));

// app.use("/", express.static(path.join(__dirname, '/mazadAdmin')));
app.use(express.static(path.join(__dirname, 'TestingNebular')));

app.use(express.static('socket'));
var io = socket(http);

streams(io);
route(app, io);

app.get('/', function (req, res) {
    res.json({ message: "Welcome to mazad api" });
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'TestingNebular/index.html'));
});



// io.on('connection', function(socket) {
//     console.log("socket connected")
// })

//App setup
app.use(express.static('socket'));

//just for test 
http.listen(port, function () {
    // console.log("Listening on port " + port, v8.getHeapStatistics())
    console.log("Listening on port " + port)
})

// var httpsServer = https.createServer(credentials, app);
// httpsServer.listen('8084', function () {
//     console.log("Listening on port https 8084")
// });