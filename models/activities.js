var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var ActivitySchema = new Schema({
    message: {type: String, default: ""},
    user_id: Object,
    type: {type: String, default: ""},
    type_id: Object,
    created_at: String
});

var Activity = mongoose.model('activities', ActivitySchema );

module.exports = Activity;