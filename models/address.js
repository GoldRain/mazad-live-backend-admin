var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var AddressSchema = new Schema({
    user_id: Object,
    addresses: [{
        addressLineOne: {type: String, default: ""},
        addressLineTwo: {type: String, default: ""},
        city: {type: String, default: ""},
        state: {type: String, default: ""},
        CountryDetails: {},
        pincode: {type: String, default: ""},
        is_active: Boolean,
        created_at: {type: String, default: ""},
        name: String,
        phone: String
    }]
})

var Address = mongoose.model('addressInfo', AddressSchema);

module.exports = Address;