var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/mazad', { useNewUrlParser: true });

var AdminSchema = new Schema({
    username: { type: String, required: true },
    phone: { type: String, default: "" },
    email: { type: String, default: "" },
    password: { type: String, default: "" },
    profilePhoto: { type: String, default: "" }
})

var Admin = mongoose.model('admins', AdminSchema);

module.exports = Admin;