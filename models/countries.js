var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var CountrySchema = new Schema({
    countryName: String,
    countryCode: String,    
    countryAbbreviation: String,
    flagImageUrl: String,
    createdAt: String
})

var Country = mongoose.model('countries', CountrySchema);

module.exports = Country;