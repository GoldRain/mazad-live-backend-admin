var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var InAppPurchaseSchema = new Schema({
    user_id: Object,
    apple_receipt: { type: String, default: "" },
    user_type: String,
    android_response: [],
    ios_response: []
})

var InAppPurchase = mongoose.model('inAppPurchase', InAppPurchaseSchema);

module.exports = InAppPurchase;