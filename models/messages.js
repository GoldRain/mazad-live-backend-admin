var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;


var MessagesSchema = new Schema({
  sender_id: Object,
  room_id: Object,
  message: String,
  message_type: String,
  created_at: String
});

var Message = mongoose.model('messages', MessagesSchema);

module.exports = Message;