var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var NotifySchema = new Schema({
    message: {type: String, default: ""},
    sender_id: Object,
    receiver_id: Object,
    type: {type: String, default: ""},
    typeDetails: Object
});

var Notify = mongoose.model('notifications', NotifySchema );

module.exports = Notify;