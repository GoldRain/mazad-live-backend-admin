var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var OrderSchema = new Schema({
    user_id: Object,
    type: { type: String, default: "" },
    type_id: Object,
    status: { type: String, default: "pending" },                               //  VALUES CAN BE PENDING(DEFAULT), CONFIRMED
    address_id: Object,
    user_payment_status: { type: String, default: "" },
    transaction_id: { type: String, default: "" },
    amount: { type: String, default: "0" },
    feedback: { type: String, default: "pending" },
    payment_method: { type: String, default: "" },
    created_at: { type: String, default: ""},
    display_id: String, 
    seller_id: Object,
    user_name: String,
    user_country: {},
    transaction_details: {}
})

var Order = mongoose.model('ordersInfo', OrderSchema);

module.exports = Order;