var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;


var OTPSchema = new Schema({
  email : {type: String, default: ""},
  country_code : {type: String, default: ""},
  phone : {type: String, default: ""},
  otp : {type : String, default : ""}
});

var Otp = mongoose.model('otps', OTPSchema);

module.exports = Otp;