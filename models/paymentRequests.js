var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var RequestSchema = new Schema({
    seller_id: Object,
    buyer_id: Object,
    message: { type: String, default: "" },
    order_id: Object,
    status: { type: String, default: "pending" },
    payment_method: { type: String, default: "" },
    created_at: { type: String, default: ""},
    type: String, 
    type_id: Object,
    seller_name: String, 
    seller_country: {}
})

var paymentRequests = mongoose.model('paymentRequests', RequestSchema);

module.exports = paymentRequests;