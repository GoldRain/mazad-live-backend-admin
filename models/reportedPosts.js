var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var ReportSchema = new Schema({
    user_id: Object,
    post_id: Object, 
    reason: String, 
    description: String, 
    created_at: String
});

var Report = mongoose.model('reportedPosts', ReportSchema );

module.exports = Report;