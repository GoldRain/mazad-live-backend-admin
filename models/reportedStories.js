var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var ReportStorySchema = new Schema({
    user_id: Object,
    story_id: Object, 
    reason: String, 
    description: String, 
    created_at: String
});

var ReportStories = mongoose.model('reportedStories', ReportStorySchema );

module.exports = ReportStories;