var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var ReportUserSchema = new Schema({
    user_id: Object,
    reported_user_id:  Object,
    reason: String, 
    description: String, 
    created_at: String
});

var ReportUser = mongoose.model('reportedUsers', ReportUserSchema );

module.exports = ReportUser;