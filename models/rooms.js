var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var RoomSchema = new Schema({
    user: [ Object ],
    created_at: String
});

var Rooms = mongoose.model('rooms', RoomSchema );

module.exports = Rooms;