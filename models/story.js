var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var StorySchema = new Schema({
    url: { type: String, default: "" },
    // url: [{type: String, default: ""}],
    created_at: { type: String, default: "" },
    is_buyNow: Boolean,
    buyNowPrice: String,
    whatsapp_only: { type: Boolean, default: false },
    whatsapp_and_call: { type: Boolean, default: false },
    is_video: Boolean,
    status: { type: String, default: "pending" },
    user_id: Object,
    seller_id: Object,
    payment_type: {type: String, default: "cash,cc"},
    is_sold: { type: Boolean, default: false },
    bidsAndComments: [{ story_id: Object, user_id: Object, amount: String, comment: String, status: { type: String, default: "pending" }, created_at: String, user_name: String, url: String, country: {} }],
    feedback: [{ user_id: Object, user_name: String, user_country: {}, user_profile: String, rating: String, comment: String, order_id: Object, created_at: String }],
    product_serial: String,
    product_description: String
});

var Story = mongoose.model('stories', StorySchema);

module.exports = Story;