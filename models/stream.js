var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var StreamSchema = new Schema({
    user_id: { type: Object, default: "" },
    stream_name: { type: String, default: "" },
    status: { type: String, default: "" },
    viewerCount: { type: Number, default: 0 },
    thumbUrl: { type: String, default: "" },
    comment: [{ user_id: Object, comment: String, amount: String, created_at: String, details: Object, is_marked: Boolean, status: { type: String, default: "pending" } }],
    country: [],
    language: String,
    created_at: String,
    current_product: { stream_id: Object, image_url: String, product_serial: String, price: String, payment_type: String, is_sold: Boolean, description: String },
    start_price: {type: String, default: "0"}
});

var Streams = mongoose.model('stream', StreamSchema);

module.exports = Streams;