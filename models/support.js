var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.Promise = global.Promise;

var SupportSchema = new Schema({
    user_id: Object,
    description: String,
    message: String
});

var Support = mongoose.model('support', SupportSchema);

module.exports = Support;