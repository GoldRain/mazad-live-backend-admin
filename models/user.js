var mongoose = require('mongoose');
//var ObjectId = mongoose.Types.ObjectId;
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/mazad', { useNewUrlParser: true });

var UserSchema = new Schema({
  username: { type: String, required: true, default: "" },
  country_code: { type: String, default: "" },
  country: {},
  phone: { type: String, default: "" },
  email: { type: String, default: "", unique: true },
  password: { type: String, default: "" },
  type: { type: String, default: "" },
  profilePhoto: { type: String, default: "" },
  favPost: [{ post_id: Object }],
  followers: [{ created_at: String, user_id: Object }],
  following: [{ created_at: String, user_id: Object }],
  socket_id: String,
  is_online: Boolean,
  is_live: String,
  description: { type: String, default: "" },
  is_auction: { type: Boolean, default: true },
  is_buyNow: { type: Boolean, default: false },
  is_whatsappOnly: { type: Boolean, default: true },
  is_whatsappAndCall: { type: Boolean, default: false },
  IBAN: { country_code: String, banNumber: String, bankName: String, phoneNumber: String },
  block_list: [{ type: Object }],
  posts_block_list: [{ type: Object }],
  stories_block_list: [{ type: Object }],
  user_type: String,
  playerId: String,
  push_notification: { type: Boolean, default: true },
  email_and_notification: { type: Boolean, default: false },
  is_activity: { type: Boolean, default: true },
  is_deleted: { type: Boolean, default: false },
  country: {},
  tap_customer_id: { type: String, default: "" },
  last_login: { type: String, default: "" },
  is_admin_block: { type: Boolean, default: false },
  show_ads: { type: Boolean, default: true },
  available_post_count: { type: Number, default: 10 },
  available_live_count: { type: Number, default: 5 },
  available_story_count: { type: Number, default: 10 },
  is_verified: { type: Boolean, default: false },
  verification: { type: Object, default: {} },
  verification_payment: { default: {} },
  unlimited_post_count: { status:{ type: Boolean, default: false }, activation: String, expiry: String , count: { type: Number, default: 10 } },
  unlimited_live_count: { status:{ type: Boolean, default: false }, activation:  String, expiry: String , count: { type: Number, default: 5 } },
  unlimited_story_count: { status:{ type: Boolean, default: false }, activation:  String, expiry: String , count: { type: Number, default: 10 } },
  transaction_identifier: { type : String, default: "" }
});

var User = mongoose.model('users', UserSchema);

module.exports = User;