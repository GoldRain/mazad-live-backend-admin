exports.sendNotification = function (ids, messageDetail) {
    console.log("notificaton function called : ", ids, ' : messagedetails : ', messageDetail)
    var data = {
        app_id: "144eccd5-4016-4a53-b05a-afa5f971ddf3",
        content_available: true,
        data: { "message": messageDetail, "type": "message" },
        include_player_ids: ids,
        ios_badgeType: "Increase",
        ios_badgeCount: "1"
    };

    var headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Basic MzNhNmViYTMtMzdkMC00ZmMyLWI4MWQtNzg5ZThmMjViZDUy"
    };

    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };


    var https = require('https');
    var req = https.request(options, function (res) {
        res.on('data', function (data) {
            console.log("Response:>>>>>>>>>>>>>>>>>>>", data);
            console.log(JSON.parse(data));
        });
    });

    req.on('error', function (e) {
        console.log("ERROR:");
        console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
};

exports.sendNotificationForIOS = function (ids, messageDetail, messageToBeShown, type = "message", titleForIOS) {
    console.log("IOS notification called ids: ", ids, " - messageToBeShown: ", messageToBeShown," - type: ", type, " - title: ", titleForIOS)
    var data = {
        app_id: "144eccd5-4016-4a53-b05a-afa5f971ddf3",
        contents: { "en": messageToBeShown },
        headings: { "en": titleForIOS },
        data: { "message": messageDetail, "type": type, "sound": "ring.mp3" },
        include_player_ids: ids
    };

    var headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Basic MzNhNmViYTMtMzdkMC00ZmMyLWI4MWQtNzg5ZThmMjViZDUy"
    };

    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };


    var https = require('https');
    var req = https.request(options, function (res) {
        res.on('data', function (data) {
            console.log("Response:");
            console.log(JSON.parse(data));
        });
    });

    req.on('error', function (e) {
        console.log("ERROR:");
        console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
};

// module.exports = sendNotificationForIOS;
// module.exports = sendNotification;