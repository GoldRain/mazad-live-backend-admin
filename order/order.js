Address = require('../models/address')
User = require('../models/user')
Order = require('../models/orders')
Country = require('../models/countries')
Route = require('../route/route')
Post = require('../models/posts')
Fun = require('../functions/function')
RequestPayment = require('../models/paymentRequests')
Streams = require('../models/stream')
Story = require('../models/story')
Tag = require('../models/tags')

var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var schedule = require('node-schedule');

exports.addAddress = function (req, res) {                                              //   TO MAKE CRUD OPERATIONS WITH ADDRESS
    Fun.authenticate(req, res, () => {
        console.log("addAddress")
        let user_id = req.body.user_id,
            l1 = req.body.addressLineOne,
            l2 = req.body.addressLineTwo,
            city = req.body.city,
            state = req.body.state,
            address_id = req.body.address_id,
            country_id = req.body.country_id,
            pincode = req.body.pincode,
            is_active = req.body.is_active,
            created_at = new Date().getTime(),
            type = req.body.type,
            name = req.body.name,
            phone = req.body.phone;
        // console.log("phone : ", phone)

        let obj = {
            addressLineOne: l1,
            addressLineTwo: l2,
            city: city,
            state: state,
            CountryDetails: "",
            pincode: pincode,
            is_active: is_active,
            created_at: created_at,
            phone: phone,
            name: name
        }

        switch (type) {
            case 'create':
                console.log("create address")
                Address.findOne({ user_id: ObjectId(user_id) }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while adding address in the database.", description: error });
                    } else if (!data) {

                        Country.findOne({ _id: ObjectId(country_id) }, function (error, countryData) {
                            if (error) {
                                res.json({ error: true, message: "Error while getting country details.", description: error });
                            } else if (!countryData) {
                                res.json({ error: true, message: "Country id is wrong." });
                            } else {
                                obj.CountryDetails = countryData;
                                // User.findOne({ _id: ObjectId(user_id)}, function (error, userData)  {
                                //     if(error) {
                                //         res.json({ error: true, message: "Error while getting user details.", description: error });
                                //     } else if(!userData) {
                                //         res.json({ error: true, message: "User id is not matched."});
                                //     } else {
                                let add = new Address({
                                    user_id: ObjectId(user_id),
                                    // userName: name,
                                    // phone: phone,
                                    addresses: obj
                                });

                                add.save(function (error, data) {
                                    if (error) {
                                        res.json({ error: true, message: "Error while adding addrees in the database first time.", description: error });
                                    } else {
                                        res.json({ error: false, message: "Done", description: data });
                                    }
                                })
                                //     }
                                // })

                            }
                        })
                    } else {
                        Country.findOne({ _id: ObjectId(country_id) }, function (error, countryData) {
                            if (error) {
                                res.json({ error: true, message: "Error while getting country details.", description: error });
                            } else if (!countryData) {
                                res.json({ error: true, message: "Country id is wrong." });
                            } else {
                                obj.CountryDetails = countryData;
                                Address.findOneAndUpdate({ user_id: ObjectId(user_id) }, { $push: { addresses: obj } }, function (error, data) {
                                    if (error) {
                                        res.json({ error: true, message: "Error while adding new address into existing address.", description: error });
                                    } else if (!data) {
                                        res.json({ error: true, message: "No data found." });
                                    } else {
                                        Address.findOne({ user_id: ObjectId(user_id) }, function (error, finalData) {
                                            if (error) {
                                                res.json({ error: true, message: "Error after new address added.", description: error });
                                            } else if (!finalData) {
                                                res.json({ error: true, message: "Address added but could not found." });
                                            } else {
                                                res.json({ error: false, message: "Done", description: finalData });
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })

                break;

            case 'update':
                console.log("update address")
                Country.findOne({ _id: ObjectId(country_id) }, function (error, countryData) {
                    if (error) {
                        res.json({ error: true, message: "Error while getting country details.", description: error });
                    } else if (!countryData) {
                        res.json({ error: true, message: "Country id is wrong." });
                    } else {
                        obj.CountryDetails = countryData;
                        Address.findOneAndUpdate({ user_id: ObjectId(user_id), 'addresses._id': ObjectId(address_id) }, {
                            'addresses.$.addressLineOne': l1,
                            'addresses.$.addressLineTwo': l2,
                            'addresses.$.city': city,
                            'addresses.$.state': state,
                            'addresses.$.pincode': pincode,
                            'addresses.$.is_active': is_active,
                            'addresses.$.created_at': created_at,
                            'addresses.$.phone': phone,
                            'addresses.$.name': name
                        }, function (error, data) {
                            if (error) {
                                res.json({ error: true, message: "Error while updating existing address", description: error });
                            } else if (!data) {
                                res.json({ error: true, message: "No data found to update" });
                            } else {
                                res.json({ error: false, message: "Address updated successfully." });
                            }
                        })
                    }
                })

                // Address.findOne({ user_id: ObjectId(user_id), addresses: [{ _id: ObjectId(address_id) }] },
                //     // { addressLineOne: l1, addressLineTwo: l2, city: city, state: state, pincode: pincode, is_active: is_active, created_at: created_at}, 
                //     function (error, data) {
                //         if (error) {
                //             res.json({ error: true, message: "Error while updating existing address", description: error });
                //         } else if (!data) {
                //             res.json({ error: true, message: "No data found to update" });
                //         } else {
                //             res.json({ error: false, message: "Address updated successfully.", description: data });
                //         }
                //     })

                break;

            case 'read':
                console.log("read address")
                Address.findOne({ user_id: ObjectId(user_id) }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while getting address list of a user.", description: error });
                    } else if (!data) {
                        res.json({ error: true, message: "Data not found" });
                    } else {
                        res.json({ error: false, message: "Done", description: data })
                    }
                })

                break;

            case 'delete':

                console.log("create address")
                console.log("active", is_active)
                Address.findOneAndUpdate({ user_id: ObjectId(user_id), 'addresses._id': ObjectId(address_id) }, { $set: { 'addresses.$.is_active': is_active } }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while deleting address", description: error })
                    } else if (!data) {
                        res.json({ error: true, message: "No data found." });
                    } else {
                        Address.findOne({ user_id: ObjectId(user_id) }, function (error, finalData) {
                            if (error) {
                                res.json({ error: true, message: "Error while getting address list after deleting anyone.'\n'", description: error });
                            } else if (!finalData) {
                                res.json({ error: true, message: "No data found but deleted" });
                            } else {
                                res.json({ error: false, message: "Done", description: finalData });
                            }
                        })
                    }
                })

                break;
        }
    })
};

exports.createOrder = function (req, res) {                                             //   TO CREATE ORDER
    Fun.authenticate(req, res, () => {
        console.log("createOrder");
        let user_payment_status, transaction_id, amount, type = req.body.type,
            time = new Date().getTime();
        let obj = {
            chargeid: req.body.chargeid,
            crd: req.body.crd,
            crdtype: req.body.crdtype,
            hash: req.body.hash,
            payid: req.body.payid,
            ref: req.body.ref,
            result: req.body.result,
            trackid: req.body.trackid
        }
        console.log("user_pay: ", req.body.payment_method, " amount: ", req.body.amount, " transID: ", req.body.transaction_id, " type : ", type)
        let number = Math.floor(10000000 + Math.random() * 900000)

        if (req.body.payment_method == "card") {
            user_payment_status = "completed", transaction_id = req.body.transaction_id, amount = req.body.amount;
        } else {
            user_payment_status = "pending", transaction_id = "", amount = req.body.amount;
        }

        if (type == "post") {
            Fun.userDetails(req.body.user_id, (userData2) => {
                let user2 = userData2.description;
                Fun.userCountry(user2.country_code, (user2_country) => {
                    console.log("post id is: >>>>>>>>>>>>> ", req.body)
                    Fun.postDetails(req.body.type_id, (postData) => {
                        console.log("moved from here: ", user2_country)
                        let order = new Order({
                            user_id: ObjectId(req.body.user_id),
                            user_name: user2.username,
                            user_country: user2_country,
                            type: type,
                            type_id: ObjectId(req.body.type_id),
                            address_id: ObjectId(req.body.address_id),
                            user_payment_status: user_payment_status,
                            transaction_id: transaction_id,
                            amount: amount,
                            payment_method: req.body.payment_method,
                            created_at: time,
                            display_id: number,
                            seller_id: ObjectId(req.body.seller_id),
                            transaction_details: obj
                        })

                        console.log("a")
                        if (postData.description.is_deleted == false) {
                            console.log("b")
                            order.save(function (error, data) {
                                if (error) {
                                    res.json({ error: true, message: "Error while order saving into database.", description: error });
                                } else {
                                    res.json({ error: false, message: "Done", description: data })
                                    console.log("1")
                                    Fun.notification(req.body.user_id, req.body.seller_id, "created an order", "Order Received", data._id)
                                    console.log("2")
                                    Route.soldEmitToAll(type, req.body.type_id);
                                    console.log("3")
                                    Fun.sendMail(req.body.user_id, "Your order request has been sent to seller, you will be notified soon about your order status.", "Order Request", number, (ok) => { console.log(ok) });
                                    Fun.sendMail(req.body.seller_id, "You have received an order", "Order Request", number, (ok) => { console.log(ok) });
                                    // console.log("response: ", data)
                                    // let type = req.body.type;
                                    Fun.userDetails(req.body.seller_id, (userData) => {
                                        console.log("4")
                                        let user = userData.description;
                                        Fun.userCountry(user.country_code, (countryData) => {
                                            let rp = new RequestPayment({
                                                seller_id: ObjectId(req.body.seller_id),
                                                seller_name: user.username,
                                                seller_country: countryData.description,
                                                buyer_id: ObjectId(req.body.user_id),
                                                order_id: data._id,
                                                payment_method: req.body.payment_method,
                                                status: "pending",
                                                created_at: time,
                                                display_id: number,
                                                type: type,
                                                type_id: ObjectId(req.body.type_id)
                                            });
                                            rp.save(function (error, data) {
                                                if (error) {
                                                    console.log({ error: true, message: "Error while saving request in the database", description: error });
                                                } else {
                                                    console.log({ error: false, message: "Done" });
                                                    let message = "Your order request has been sent to seller. Order no. is " + number + " . you will be notified soon about your order status."
                                                    Fun.sms(user.country_code, user.email, user.phone, message);
                                                    Fun.updatePostCount(req.body.type_id, () => { })
                                                    console.log("post _id is: ", req.body.type_id)
                                                    Funn.postDetails(req.body.type_id, (postData) => {
                                                        if (postData.error == false) {
                                                            let fullPost = postData.description;
                                                            if (fullPost.is_stream == false) {
                                                                console.log("checkkkedddd")
                                                                Post.findOneAndUpdate({ _id: ObjectId(req.body.type_id) }, { is_sold: true }, function (error, data) {
                                                                    if (error)
                                                                        console.log({ error: true, message: "ERRRRRRRRRRRORRRRRRRRRRRR", description: error })
                                                                    else if (!data) {
                                                                        console.log({ error: true, message: "ERRRRRRRRRRRORRRRRRRRRRRR", description: "No post found" })
                                                                    } else {
                                                                        console.log({ error: true, message: "is_sold: true" })
                                                                    }
                                                                })
                                                            } else {
                                                                console.log("user_id is ", req.body.user_id)
                                                                Post.findOneAndUpdate({ _id: ObjectId(req.body.type_id), 'bids.bid_user_id': ObjectId(req.body.user_id) }, { 'bids.$.is_paid': true }, function (error, data) {
                                                                    if (error)
                                                                        console.log({ error: true, message: "ERRRRRRRRRRRORRRRRRRRRRRR", description: error })
                                                                    else if (!data) {
                                                                        console.log({ error: true, message: "ERRRRRRRRRRRORRRRRRRRRRRR", description: "No post found" })
                                                                    } else {
                                                                        console.log({ error: true, message: "is_sold: trrrrrrrue" })
                                                                    }
                                                                })
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        })
                                    })
                                }
                            })
                        }
                    })
                })
            })
        } else {
            console.log("inside the else")
            Fun.userDetails(req.body.user_id, (userData2) => {
                let user2 = userData2.description;
                Fun.userCountry(user2.country_code, (user2_country) => {
                    Fun.storyDetails(req.body.type_id, (storyData) => {
                        if (storyData.message == "Done") {
                            let order = new Order({
                                user_id: ObjectId(req.body.user_id),
                                user_name: user2.username,
                                user_country: user2_country.description,
                                type: type,
                                type_id: ObjectId(req.body.type_id),
                                address_id: ObjectId(req.body.address_id),
                                user_payment_status: user_payment_status,
                                transaction_id: transaction_id,
                                amount: storyData.description.buyNowPrice,
                                payment_method: req.body.payment_method,
                                created_at: time,
                                display_id: number,
                                seller_id: ObjectId(req.body.seller_id),
                                transaction_details: obj
                            })

                            order.save(function (error, data) {
                                if (error) {
                                    res.json({ error: true, message: "Error while order saving into database.", description: error });
                                } else {
                                    res.json({ error: false, message: "Done and saved", description: data })
                                    Fun.notification(req.body.user_id, req.body.seller_id, "created an order", "Order Received", data._id)
                                    Route.soldEmitToAll(type, req.body.type_id);
                                    Fun.sendMail(req.body.user_id, "Your order request has been sent to seller, you will be notified soon about your order status.", "Order Request", number, (ok) => { console.log(ok) })
                                    Fun.sendMail(req.body.seller_id, "You have received an order", "Order Request", number, (ok) => { console.log(ok) })
                                    // let type = req.body.type;
                                    Fun.userDetails(req.body.seller_id, (userData) => {
                                        let user = userData.description;
                                        Fun.userCountry(user.country_code, (countryData) => {
                                            let rp = new RequestPayment({
                                                seller_id: ObjectId(req.body.seller_id),
                                                seller_name: user.username,
                                                seller_country: countryData.description,
                                                buyer_id: ObjectId(req.body.user_id),
                                                order_id: data._id,
                                                payment_method: req.body.payment_method,
                                                status: "pending",
                                                created_at: time,
                                                display_id: number,
                                                type: type,
                                                type_id: ObjectId(req.body.type_id)
                                            });
                                            rp.save(function (error, data) {
                                                if (error) {
                                                    console.log({ error: true, message: "Error while saving request in the database", description: error });
                                                } else {
                                                    console.log({ error: false, message: "Done" })
                                                    let message = "You have received an order. Order no. is " + number + " . Please check your order and update the status."
                                                    Fun.sms(user.country_code, user.email, user.phone, message);
                                                    Story.findOneAndUpdate({ _id: ObjectId(req.body.type_id) }, { is_sold: true }, function (error, data) {
                                                        if (error)
                                                            console.log({ error: true, message: "ERRRRRRRRRRRORRRRRRRRRRRR", description: error })
                                                        else if (!data) {
                                                            console.log({ error: true, message: "ERRRRRRRRRRRORRRRRRRRRRRR", description: "No story found" })
                                                        } else {
                                                            console.log({ error: true, message: "is_sold: true" })
                                                        }
                                                    })
                                                }
                                            })
                                        })
                                    })

                                }
                            })
                        } else {
                            res.json(storyData)
                        }
                    })
                })
            })
        }
    })
};

exports.orderOtp = function (req, res) {                                                //   TO GENERATE OTP BEFORE ORDER CREATING
    Fun.authenticate(req, res, () => {
        let user_id = req.body.user_id;
        User.findOne({ _id: ObjectId(user_id) }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error in orderOtp", description: error });
            } else if (!data) {
                res.json({ error: true, message: "User not found to send otp" });
            } else {
                let phone = data.phone,
                    country_code = data.country_code,
                    email = data.email;
                console.log("phone: ", phone)
                Fun.otp(country_code, email, phone, req, res)
            }
        })
    })
};

exports.verifyOrderOtp = function (req, res) {                                          //   TO VERIFY THE CODE
    Fun.authenticate(req, res, () => {
        console.log("verifyOrderOtp")
        let code = req.body.code;
        let checkStr = "";
        let country_code = "";
        let phone = "";
        country_code = req.body.country_code;
        phone = req.body.phone;
        checkStr = { phone: phone, country_code: '+' + country_code }
        console.log("checkstr: ", checkStr, ' code: ', code)

        Otp.find(checkStr, function (err, data) {
            if (err) {
                res.json({ error: true, message: "Something went wrong", data: err });
            } else {
                if (data.length == 1) {
                    if (code == data[0].otp) {
                        res.json({ error: false, message: "Otp Verified", description: data });
                        Otp.deleteOne({ _id: ObjectId(data[0]._id) }, function (error, data) {
                            if (error) {
                                console.log({ message: "Error while otp deleting", description: error })
                            } else {
                                console.log("otp deleted")
                            }
                        })
                    } else {
                        res.json({ error: true, message: "Wrong otp, Please check and enter again" });
                    }
                } else {
                    console.log('data: ', data)
                    res.json({ error: true, message: "Code not matched" })
                }
            }
        })
    })
};

exports.orderList = function (req, res) {                                               //   TO GETT ALL ORDERS OF A USER( buyer )
    Fun.authenticate(req, res, () => {
        console.log("orderList");
        let user_id = req.body.user_id;

        Order.find({ user_id: ObjectId(user_id) }, function (error, alldata) {
            if (error) {
                res.json({ error: true, message: "Error while getting order details", description: error })
            } else if (alldata.length == 0) {
                res.json({ error: true, message: "No data found" });
            } else {
                // console.log("data:" ,alldata)
                let wholeData = [];
                for (let i = 0; i < alldata.length; i++) {
                    console.log("i: ", i)
                    let type = alldata[i].type,
                        user = "users",
                        address = "addressinfos";

                    if (type == "post") {
                        type = "allposts";
                    } else if (type == "story") {
                        type = "stories";
                    } else if (type == "stream") {
                        type = "streams";
                    }

                    console.log("type is here  >>> ", type);
                    Order.aggregate([{ $match: { _id: ObjectId(alldata[i]._id) } },
                    { $lookup: { from: type, localField: 'type_id', foreignField: "_id", as: "type_data" } },
                    { $lookup: { from: user, localField: 'user_id', foreignField: "_id", as: "user_data" } },
                    { $lookup: { from: "addressinfos", localField: 'address_id', foreignField: "addresses._id", as: "address" } },
                    { $lookup: { from: user, localField: 'seller_id', foreignField: "_id", as: "seller_data" } }
                    ], function (error, data) {
                        if (error) {
                            console.log("1111111111111 and i is ", i, " and err is ", error);

                        } else if (data.length == 0) {
                            console.log("2")

                        } else {
                            console.log("3")

                            wholeData.push(data[0]);
                            if (wholeData.length == alldata.length) {
                                console.log("aya")
                                res.json({ error: false, message: "Done", description: wholeData })
                            }
                        }
                    })
                    console.log("length:", alldata.length)
                }
            }
        })
    })
};

exports.postBidDetails = function (req, res) {                                          //   TO GET ALL DETAILS OF POST
    Fun.authenticate(req, res, () => {
        console.log("postBidDetails");
        let user_id = req.body.user_id,
            type = req.body.type,
            filteredPost = [],
            filteredStory = [];
        if (type == "buyer") {
            console.log("1")

            Post.aggregate([{ $match: { 'bids.bid_user_id': { $in: [ObjectId(user_id)] } } },
            { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'userDetails' } }
            ], function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting bid list.", description: error })
                } else if (data.length == 0) {
                    // res.json({ error: true, message: "No data found" });
                    Story.aggregate([{ $match: { 'bidsAndComments.user_id': { $in: [ObjectId(user_id)] } } },
                    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }
                    ], function (error, storyData) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting bid on stories", description: error });
                        } else if (storyData.length == 0) {
                            res.json({ error: true, message: "No data found" })
                        } else {
                            console.log("a:")
                            for (let j = 0; j < storyData.length; j++) {
                                filteredStory.push(storyData[j])
                                if (j == storyData.length - 1) {
                                    res.json({ error: false, message: "Done but posts not found", post: filteredPost, story: filteredStory })
                                }
                            }
                        }
                    })
                } else {
                    console.log("2: ", data.length)
                    for (let i = 0; i < data.length; i++) { // need a check of post must be > 0
                        filteredPost.push(data[i]);
                        if (i == data.length - 1) {
                            Story.aggregate([{ $match: { 'bidsAndComments.user_id': { $in: [ObjectId(user_id)] } } },
                            { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }
                            ], function (error, storyData) {
                                if (error) {
                                    res.json({ error: true, message: "Error while getting bid on stories", description: error });
                                } else if (storyData.length == 0) {
                                    res.json({ error: false, message: "Done but stories not found", post: filteredPost })
                                } else {
                                    console.log("3: ", storyData.length)
                                    for (let j = 0; j < storyData.length; j++) {
                                        filteredStory.push(storyData[j])
                                        if (j == storyData.length - 1) {
                                            console.log("4: ")
                                            res.json({ error: false, message: "Done", post: filteredPost, story: filteredStory })
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            })
        } else {
            console.log("inside else")
            Post.aggregate([{ $match: { post_user_id: ObjectId(user_id) } },
            { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'userDetails' } }
            ], function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting bid list for seller.", description: error })
                } else if (data.length == 0) {
                    Story.aggregate([{ $match: { user_id: ObjectId(user_id) } },
                    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }
                    ], function (error, storyData) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting bid on stories", description: error });
                        } else if (storyData.length == 0) {
                            res.json({ error: true, message: "No data found." })
                        } else {
                            for (let j = 0; j < storyData; j++) {
                                if (storyData[j].bidsAndComments.length > 0) {
                                    filteredStory.push(storyData[j])
                                }
                                if (j == storyData.length - 1) {
                                    res.json({ error: false, message: "Done but posts not found", post: filteredPost, story: filteredStory })
                                }
                            }
                        }
                    })
                    // res.json({ error: true, message: "No data found" });
                } else {
                    for (let i = 0; i < data.length; i++) { // need a check of post must be > 0
                        if (data[i].bids.length > 0) {
                            filteredPost.push(data[i]);
                        }
                        if (i == data.length - 1) {
                            Story.aggregate([{ $match: { user_id: ObjectId(user_id) } },
                            { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }
                            ], function (error, storyData) {
                                if (error) {
                                    res.json({ error: true, message: "Error while getting bid on stories", description: error });
                                } else if (storyData.length == 0) {
                                    res.json({ error: false, message: "Done but no stories found", post: filteredPost })
                                } else {
                                    console.log("length: ", storyData.length)
                                    for (let j = 0; j < storyData.length; j++) {
                                        if (storyData[j].bidsAndComments.length > 0) {
                                            filteredStory.push(storyData[j])
                                        }
                                        if (j == storyData.length - 1) {
                                            res.json({ error: false, message: "Done.", post: filteredPost, story: filteredStory })
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
    })
};

exports.changeBidStatus = function (req, res) {                                         //   TO APPROVE THE BID OF POST
    Fun.authenticate(req, res, () => {
        console.log("changeBidStatus");
        let post_id = req.body.id,
            bid_id = req.body.bid_id,
            status = req.body.status,
            userDetails, type = req.body.type;
        if (type == "post") {
            console.log("id1: ", post_id, " - id2: ", bid_id)
            if (status == "approved") {
                Post.findOneAndUpdate({ _id: ObjectId(post_id), 'bids._id': ObjectId(bid_id) }, { $set: { 'bids.$.status': status } }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while updating bid status", description: error });
                    } else if (!data) {
                        res.json({ error: true, message: "No match found." });
                    } else {
                        Post.findOne({ _id: ObjectId(post_id) }, function (error, updatedData) {
                            if (error) {
                                res.json({ error: true, message: "Error after Bid status approved", description: error });
                            } else if (!updatedData) {
                                res.json({ error: true, message: "No data found" });
                            } else {
                                res.json({ error: false, message: "Done", description: updatedData });
                                updatedData.bids.forEach(element => {

                                    if (element._id.toString() == bid_id.toString()) {
                                        userDetails = element;
                                        console.log("senderId :", data.post_user_id, " - receiverId : ", userDetails._id)
                                        Fun.notification(data.post_user_id, userDetails.bid_user_id, "approved your bid", "Bid Approved", userDetails)
                                    }
                                });
                            }
                        })
                    }
                })
            } else {
                Post.update({ _id: ObjectId(post_id), 'bids._id': ObjectId(bid_id) }, { $pull: { bids: { _id: ObjectId(bid_id) } } }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while updating bid status", description: error });
                    } else if (!data) {
                        res.json({ error: true, message: "No match found" });
                    } else {
                        res.json({ error: false, message: "Done", description: data });
                    }
                })
            }
        } else {
            if (status == "approved") {
                Story.findOneAndUpdate({ _id: ObjectId(post_id), 'bidsAndComments._id': ObjectId(bid_id) }, { $set: { 'bidsAndComments.$.status': status } }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while updating the status of story", description: error });
                    } else if (!data) {
                        res.json({ error: true, message: "No story found" });
                    } else {
                        Fun.storyDetails(post_id, (storyData) => {
                            res.json(storyData);
                            storyData.description.bidsAndComments.forEach(element => {

                                if (element._id.toString() == bid_id.toString()) {
                                    userDetails = element;
                                    // console.log("senderId :", data.post_user_id, " - receiverId : ", userDetails._id)
                                    Fun.notification(storyData.description.user_id, userDetails.user_id, "approved your bid", "Bid Approved", userDetails)
                                }
                            });
                        })
                    }
                })
            } else {
                Story.update({ _id: ObjectId(post_id), 'bidsAndComments._id': ObjectId(bid_id) }, { $pull: { bidsAndComments: { _id: ObjectId(bid_id) } } }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while updating bid status of story", description: error });
                    } else if (!data) {
                        res.json({ error: true, message: "No match found" });
                    } else {
                        res.json({ error: false, message: "Done", description: data });
                    }
                })
            }
        }
    })
};

exports.walletList = function (req, res) {                                              //   TO GET THE WALLET LIST OF A USER
    Fun.authenticate(req, res, () => {
        console.log("walletList");
        // let user_id = req.body.user_id, message = req.body.message, order_id = req.body.order, type = req.body.type, status = req.body.status, time = new Date().getTime();
        let type = req.body.type,
            type_id = req.body.type_id,
            id;
        if (type == "buyer") {

            RequestPayment.find({ buyer_id: ObjectId(type_id) }, function (error, allData) {
                if (error) {
                    res.json({ error: true, messages: "Error while getting all details of buyer's walled", description: error });
                } else if (allData.length == 0) {
                    res.json({ error: true, message: "No data found" });
                } else {
                    let wholeData = [];
                    for (let i = 0; i < allData.length; i++) {
                        let type = allData[i].type,
                            user = "users",
                            address = "addressinfos";

                        if (type == "post") {
                            type = "allposts";
                        } else if (type == "story") {
                            type = 'stories';
                        }
                        RequestPayment.aggregate([{ $match: { _id: ObjectId(allData[i]._id) } },
                        { $lookup: { from: type, localField: 'type_id', foreignField: "_id", as: "type_data" } },
                        { $lookup: { from: 'ordersinfos', localField: 'order_id', foreignField: "_id", as: "orderDetails" } }
                            // { $lookup: { from: user, localField: 'user_id', foreignField: "_id", as: "user_data" } },
                        ], function (error, activityDetails) {
                            if (error) {
                                console.log("1111111111111 and i is ", i, " and err is ", error);
                            } else if (allData.length == 0) {
                                console.log("2")
                            } else {
                                wholeData.push(activityDetails[0]);
                                if (wholeData.length == allData.length) {
                                    res.json({ error: false, message: "Done", description: wholeData })
                                }
                            }
                        })
                    }
                }
            })
            //     RequestPayment.aggregate([{$match:{ buyer_id: ObjectId(user_id) }},
            //     {$lookup: { from: 'ordersinfos', localField: 'order_id', foreignField: '_id', as: 'orderDetails'}}
            // ], function (error, data) {
            //         if (error) {
            //             res.json({ error: true, message: "Error while getting wallet list", description: error });
            //         } else if (data.length == 0) {
            //             res.json({ error: true, message: 'No data found' });
            //         } else {
            //             res.json({ error: false, message: "Done", description: data });
            //         }
            //     })
        } else {
            RequestPayment.aggregate([{ $match: { seller_id: ObjectId(type_id) } },
            { $lookup: { from: 'ordersinfos', localField: "order_id", foreignField: '_id', as: 'orderDetails' } }
            ], function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting wallet list", description: error });
                } else if (data.length == 0) {
                    res.json({ error: true, message: 'No data found' });
                } else {
                    res.json({ error: false, message: "Done", description: data });
                }
            })
        }
    })
};

exports.deletePost = function (req, res) {                                              //   TO DELETE THE POST BUT NOT FROM DATABASE
    Fun.authenticate(req, res, () => {
        console.log("deletePost");
        let post_id = req.body.post_id,
            is_deleted = req.body.is_deleted;
        Post.findOneAndUpdate({ _id: ObjectId(post_id) }, { is_deleted: is_deleted }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while updating is_deleted", description: error });
            } else if (!data) {
                res.json({ error: true, message: 'No data found' });
            } else {
                Fun.postDetails(post_id, (data) => {
                    res.json(data)
                })
            }
        })
    })
};

exports.is_deletedStatus = function (req, res) {                                        //   TO CHECK THE STATUS OF POST AND STORY
    Fun.authenticate(req, res, () => {
        console.log("is_deletedStatus");
        let type_id = req.body.type_id,
            type = req.body.type;
        if (type == "post") {
            Fun.postDetails(type_id, (postData) => {
                if (postData.description.is_deleted) {
                    res.json({ error: false, message: postData.description.is_deleted })
                } else {
                    res.json({ error: false, message: postData.description.is_deleted })
                }
            })
        } else {
            Fun.storyDetails(type_id, (storyData) => {
                if (storyData.description.status == "running") {
                    res.json({ error: false, message: false })
                } else {
                    res.json({ error: false, message: true })
                }
                // res.json(storyData)
            })
        }
    })
};

exports.sellerOrder = function (req, res) {                                             //   TO CHECK ALL THE ORDER RECEIVED TO (SELLER)
    Fun.authenticate(req, res, () => {
        console.log("sellerOrder");
        let user_id = req.body.user_id;
        Order.find({ seller_id: ObjectId(user_id) }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while getting the order details of a seller", description: error })
            } else if (data.length == 0) {
                res.json({ error: true, message: "No order found" })
            } else {
                let wholeData = [];
                for (let i = 0; i < data.length; i++) {
                    let type = data[i].type;

                    if (type == "post") {
                        type = "allposts";
                    } else if (type == "story") {
                        type = "stories";
                    }
                    Order.aggregate([{ $match: { _id: ObjectId(data[i]._id) } },
                    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'buyerDetails' } },
                    { $lookup: { from: type, localField: 'type_id', foreignField: '_id', as: 'typeData' } },
                    { $lookup: { from: "addressinfos", localField: 'address_id', foreignField: 'addresses._id', as: 'address' } },
                    { $lookup: { from: 'users', localField: 'seller_id', foreignField: '_id', as: 'seller_data' } }
                    ], function (error, activityDetails) {
                        if (error) {
                            console.log("Errorrrrrrrrrrr", error);
                        } else if (activityDetails.length == 0) {
                            console.log("2")
                        } else {
                            wholeData.push(activityDetails[0]);
                            if (wholeData.length == data.length) {
                                res.json({ error: false, message: "Done", description: wholeData })
                            }
                        }
                    })
                }
            }
        })
    })
};

exports.acceptOrderRequestBySeller = function (req, res) {                              //   TO ACCEPT THE ORDERS OF BUYERS BY SELLER
    Fun.authenticate(req, res, () => {
        console.log("acceptOrder");
        let order_id = req.body.order_id,
            status = req.body.status;
        Order.findOneAndUpdate({ _id: ObjectId(order_id) }, { status: status }, function (error, data) {
            if (error) {
                res.json({ error: true, message: 'Error while updting order status', description: error })
            } else if (!data) {
                res.json({ error: true, message: "No data found" })
            } else {
                Order.findOne({ _id: ObjectId(order_id) }, function (error, newData) {
                    if (error) {
                        res.json({ error: true, message: 'Error while updting order status', description: error })
                    } else if (!data) {
                        res.json({ error: true, message: "No data found" })
                    } else {
                        res.json({ error: false, message: "Done", description: newData })
                        Fun.notification(newData.seller_id, newData.user_id, "accepted your order request.", "Order Request", newData)
                    }
                })
            }
        })
    })
};

exports.requestPaymentToAdmin = function (req, res) {                                   //   TO REQUEST THE ADMIN FOR PAYMENT 
    Fun.authenticate(req, res, () => {
        console.log("requestPaymentToBuyer");
        let order_id = req.body.order_id;
        Order.findOne({ _id: ObjectId(order_id) }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while getting order details for payment", description: error });
            } else if (!data) {
                res.json({ error: true, message: "No data found" });
            } else {
                if (data.status == "pending") {
                    res.json({ error: true, message: "Please approve order first" });
                } else if (data.status == "accepted") {
                    let time = new Date().getTime(),
                        sixtyDays = 5184000000;
                    let result = parseInt(time) - parseInt(data.created_at);
                    if (result >= sixtyDays) {
                        Fun.changeWalletStatus(order_id, (orderData) => {
                            res.json(orderData)
                        })
                    } else {
                        res.json({ error: true, message: "Waiting for delivery confirmation please check back later" });
                    }
                } else {
                    Fun.changeWalletStatus(order_id, (orderData) => {
                        res.json(orderData)
                    })
                }
            }
        })
    })
};

exports.deliveryConfirmed = function (req, res) {                                       //   TO CONFIRM THE DELIVERY BY BUYER
    Fun.authenticate(req, res, () => {
        console.log("deliveryConfirmed");
        let order_id = req.body.order_id;
        Order.findOneAndUpdate({ _id: ObjectId(order_id) }, { status: "delivery confirmed", user_payment_status: "completed" }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while updating delivery status by buyer", description: error })
            } else if (!data) {
                res.json({ error: true, message: "No order found" })
            } else {
                Order.findOne({ _id: ObjectId(order_id) }, function (error, newData) {
                    if (error) {
                        res.json({ error: true, message: "Error after delivery status updated", description: error });
                    } else if (!newData) {
                        res.json({ error: true, message: "No data found but status updated" });
                    } else {
                        res.json({ error: false, message: "Done", description: newData })
                    }
                })
            }
        })
    })
};

exports.autoConfirmed = function (req, res) {                                           //   TO AUTO CONFIRM THE DELIVERY AFTER 60 DAYS
    Fun.authenticate(req, res, () => {
        console.log("autoConfirmed");
        let user_id = req.body.user_id;
        Order.find({ user_id: ObjectId(user_id), status: { $in: ["pending" || "accepted"] } }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while getting all pending", description: error });
            } else if (data.length == 0) {
                res.json({ error: true, message: "No data found" });
            } else {
                let time = new Date().getTime(),
                    sixtyDays = 5184000000;
                for (let i = 0; i < data.length; i++) {
                    let result = parseInt(time) - parseInt(data[i].created_at);
                    if (result >= sixtyDays) {
                        Order.findOneAndUpdate({ _id: ObjectId(data[i]._id) }, { status: "auto confirmed" }, function (error, updatedData) {
                            if (error) {
                                console.log({ error: true, message: "Error while updaing 60 days older orders", description: error });
                            } else if (!data) {
                                console.log({ error: true, message: "No data found" });
                            } else {
                                console.log({ error: false, message: "Done" })
                            }
                        })
                    }
                }
            }
        })
    })
};

exports.tapCustomerId = function (req, res) {                                           //   TO UPDATE TAP ID OF CUSTOMER WHICH HELP TO MAKE PAYMENTS
    console.log("tapCustomerId");
    let user_id = req.body.user_id,
        tap_id = req.body.tap_customer_id;
    User.findOneAndUpdate({ _id: ObjectId(user_id) }, { tap_customer_id: tap_id }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Failed to update tap custom id.", description: error });
        } else if (!data) {
            res.json({ error: true, message: "Failed to find user" });
        } else {
            Fun.userDetails(user_id, (userData) => {
                res.json(userData)
            })
        }
    })
};

exports.getCurrentProduct = function (req, res) {                                       //   TO GET THE CURRENT PRODUCT OF LIVE STREAM
    console.log("getCurrentProduct");
    let stream_id = req.body.stream_id;
    Streams.aggregate([{ $match: { _id: ObjectId(stream_id) } },
    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }
    ], function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while getting the current product", description: error });
        } else if (data.length == 0) {
            res.json({ error: true, message: "No data found" });
        } else {
            res.json({ error: false, message: "Done", description: data[0] })
        }
    })
};

exports.deleteStory = function (req, res) {                                             //   TO DELETE THE STORY
    Fun.authenticate(req, res, () => {
        console.log("deleteStory");
        let story_id = req.body.story_id;
        Story.findOneAndUpdate({ _id: ObjectId(story_id) }, { status: "stopped" }, function (error, done) {
            if (error) {
                res.json({ error: true, message: "Error while updating status of story", description: error });
            } else if (!done) {
                res.json({ error: true, message: "No story found" });
            } else {
                Fun.storyDetails(story_id, (storyData) => {
                    res.json(storyData);
                })
            }
        })
    })
};

exports.updateAllCount = function (req, res) {                                          //   TO UPDATE USER'S POST STORY AND LIVE COUNTS
    Fun.authenticate(req, res, () => {
        console.log("updateAllCount: ", req.body.count);
        let user_id = req.body.user_id,
            count = req.body.count,
            type = req.body.type;
        console.log("count: ", count)
        Fun.userDetails(user_id, (userData) => {
            if (userData.error == false) {
                let data = userData.description,
                    newCount;
                let activationDate = new Date().getTime();
                let expiryDate = parseInt(activationDate) + 2592000000;
                if (parseInt(count) < -1) {
                    res.json({ error: true, message: "Negative values found " + count })
                } else {
                    if (type == "post") {
                        if (count == "-1") {
                            let unObj = { status: true, activation: activationDate, expiry: expiryDate, count: data.unlimited_post_count.count }
                            let obj = { unlimited_post_count: unObj }
                            Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                                res.json(yes);
                                if (yes.error == false) {

                                }
                            })
                        } else {
                            newCount = parseInt(data.unlimited_post_count.count) + parseInt(count);
                            let unObj = { status: false, activation: "", expiry: "", count: newCount };
                            Fun.update_post_count(user_id, unObj, (yes) => {
                                res.json(yes)
                            });
                        }

                    } else if (type == "story") {
                        console.log("countagain: ", count)
                        if (count == "-1") {
                            let unObj = { status: true, activation: activationDate, expiry: expiryDate, count: data.unlimited_story_count.count }
                            let obj = { unlimited_story_count: unObj }
                            Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                                res.json(yes);
                            })
                        } else {
                            newCount = parseInt(data.unlimited_story_count.count) + parseInt(count);
                            console.log("countagainagain: ", newCount)
                            let unObj = { status: false, activation: "", expiry: "", count: newCount };
                            Fun.update_story_count(user_id, unObj, (yes) => {
                                res.json(yes)
                            });
                        }

                    } else {
                        if (count == "-1") {
                            let unObj = { status: true, activation: activationDate, expiry: expiryDate, count: data.unlimited_live_count.count }
                            let obj = { unlimited_live_count: unObj }
                            Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                                res.json(yes);
                            })
                        } else {
                            newCount = parseInt(data.unlimited_live_count.count) + parseInt(count);
                            let unObj = { status: false, activation: "", expiry: "", count: newCount }
                            Fun.update_live_count(user_id, unObj, (yes) => {
                                res.json(yes)
                            });
                        }
                    }
                }
            } else {
                res.json({ error: true, message: "Something went wrong while updating count" });
            }
        })
    })
};

exports.updatePromote = function (req, res) {                                           //   TO PROMOTE THE POST
    Fun.authenticate(req, res, () => {
        console.log("updatePrompt");
        let post_id = req.body.post_id,
            is_promote = req.body.is_promote;
        console.log("tell me the type: ", typeof (is_promote))
        Post.findOneAndUpdate({ _id: ObjectId(post_id) }, { is_promoted: is_promote }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while updating is_prompted of post", description: error });
            } else if (!data) {
                res.json({ error: true, message: "No post found" });
            } else {
                Fun.postDetails(post_id, (postData) => {
                    res.json(postData);
                })
            }
        })
    })
};

exports.userVerification = function (req, res) {                                        //   TO GET THE USER VERIFICATION DONE
    Fun.authenticate(req, res, () => {
        console.log("userVerification");
        let time = new Date().getTime();
        let user_id = req.body.user_id,
            id_type = req.body.id_type,
            id_number = req.body.id_number,
            image_url = req.body.image_url;
        let obj = { user_id: user_id, id_type: id_type, id_number: id_number, image_url: image_url, status: "pending", payment_status: "pending", payment_date: null, created_at: time };
        User.findOneAndUpdate({ _id: ObjectId(user_id) }, { $set: { verification: obj } }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while updating verification details", description: error });
            } else if (!data) {
                res.json({ error: true, message: "No user found" });
            } else {
                Fun.userDetails(user_id, (userData) => {
                    res.json(userData);
                })
            }
        })
    })
};

exports.showCounts = function (req, res) {                                              //   TO RETURN ALL COUNTS
    Fun.authenticate(req, res, () => {
        console.log("showCounts");
        let user_id = req.body.user_id;
        User.aggregate([{ $match: { _id: ObjectId(user_id) } },
        { $project: { _id: 1, unlimited_live_count: 1, unlimited_post_count: 1, unlimited_story_count: 1 } }], function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while getting count details of user", description: error });
            } else if (data.length == 0) {
                res.json({ error: true, message: "No user found" });
            } else {
                res.json({ error: false, message: "Done", description: data[0] })
            }
        })
    })
};

exports.popularPost = function (req, res) {                                             //   TO RETURN ALL POPULAR POSTS ACCORDING TO ORDERS AND LIKES
    Fun.authenticate(req, res, () => {
        console.log("popularPost");
        Posts.aggregate([{ $match: { is_deleted: false, is_stream: false } },
        { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
        { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
        { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
        { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
        { "$sort": { "post_like": -1, } }
        ], function (err, data) {
            if (err) {
                res.json({ error: true, message: "Something went wrong.", description: err });
            } else if (!data) {
                res.json({ error: false, message: 'No data found, user id might not be matched' })
            } else {
                // let likedPost = [];
                // for (let i = 0; i < data.length; i++){
                //     if(data[i].post_like.length > 0) {
                //         likedPost.push(data[i]);
                //     }
                // }
                Posts.aggregate([{ $match: { is_deleted: false, is_stream: false } },
                { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
                { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
                { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
                { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
                { "$sort": { "orders": -1, } }
                ], function (err, dataa) {
                    if (err) {
                        res.json({ error: true, message: "Something went wrong.", description: err });
                    } else if (!dataa) {
                        res.json({ error: false, message: 'No data found, user id might not be matched' })
                    } else {
                        res.json({ error: false, message: "Done", sortByOrders: dataa, sortByLikes: data });
                    }
                })
            }
        })
    })
};

exports.notificationAndEmail = function (req, res) {                                    //   TO UPDATE THE CHECK OF EMAIL AND NOTIFICATION
    console.log("notificationAndEmail");
    let user_id = req.body.user_id,
        push_notification = req.body.push_notification,
        email_and_notification = req.body.email_and_notification;
    User.findOneAndUpdate({ _id: ObjectId(user_id) }, { push_notification: push_notification, email_and_notification: email_and_notification }, function (error, data) {
        if (error) {
            res.json({ error: true, message: "Error while updating push notification", description: error });
        } else if (!data) {
            res.json({ error: true, message: "No user found" })
        } else {
            Fun.userDetails(user_id, (userData) => {
                res.json(userData)
            })
        }
    })
};

exports.verificationPayment = function (req, res) {                                     //   TO MAKE PAYMENT FOR VERIFICATION
    Fun.authenticate(req, res, () => {
        console.log("verificationPayment");
        let obj = {
            chargeid: req.body.chargeid,
            crd: req.body.crd,
            crdtype: req.body.crdtype,
            hash: req.body.hash,
            payid: req.body.payid,
            ref: req.body.ref,
            result: req.body.result,
            trackid: req.body.trackid
        };
        let user_id = req.body.user_id,
            time = new Date().getTime();
        User.findOneAndUpdate({ _id: ObjectId(user_id) }, { verification_payment: obj, is_verified: true, 'verification.payment_status': "done", 'verification.payment_date': time }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while updating user verification detail", description: error });
            } else if (!data) {
                res.json({ error: true, message: "User not found" });
            } else {
                Fun.userDetails(user_id, (userData) => {
                    res.json(userData)
                })
            }
        })
    })
};

exports.verificationDetails = function (req, res) {                                     //   TO GET THE VERIFICATION DETAILS
    Fun.authenticate(req, res, () => {
        console.log("verificationDetails")
        Fun.userDetails(req.body.user_id, (userData) => {
            if (userData.error == false) {
                let data = userData.description.verification;
                res.json({ error: false, message: "Done", description: data })
            } else {
                res.json({ error: true, message: "Something went wrong while getting verification details" });
            }
        })
    })
};

exports.noMoreAds = function (req, res) {                                               //   TO DISABLE THE ADS
    Fun.authenticate(req, res, () => {
        console.log("noMoreAds")
        let user_id = req.body.user_id,
            is_ads = req.body.is_ads;
        User.findOneAndUpdate({ _id: ObjectId(user_id) }, { show_ads: is_ads }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while updating show ads", description: error });
            } else if (!data) {
                res.json({ error: true, message: "No user found" });
            } else {
                Fun.userDetails(user_id, (userData) => {
                    res.json({ error: false, message: "Done", description: { show_ads: userData.description.show_ads } });
                })
            }
        })
    })
};

exports.categoryAndCount = function (req, res) {                                        //   TO RETURN COUNT OF ALL POSTS ACCORDING TO TAGS
    Fun.authenticate(req, res, () => {
        console.log("categoryAndCount");
        let name = req.body.name;
        Tag.find(function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while getting tags details", description: error });
            } else if (data.length == 0) {
                res.json({ error: false, message: "No tags found" });
            } else {
                let details = [];
                var count = data.length;
                for (let i = 0; i < data.length; i++) {
                    let obj = { name: String, number: String };

                    Post.find({ 'tags.tag_id': ObjectId(data[i]._id), is_deleted: false }, function (error, dataa) {
                        count = count - 1;
                        if (error) {
                            res.json({ error: true, message: "Error while getting posts according to tag.", description: error });
                            i = data.length;
                        } else if (dataa.length == 0) {
                            obj._id = data[i]._id;
                            obj.name = data[i].name;
                            obj.number = dataa.length;
                            details.push(obj);
                        } else {
                            obj._id = data[i]._id;
                            obj.name = data[i].name;
                            obj.number = dataa.length;
                            details.push(obj)
                        }
                        if (count == 0) {
                            res.json({ error: false, message: "Done", description: details })
                        }
                    })
                }
            }
        })
    })
};

exports.placeAndCount = function (req, res) {                                           //   TO RETURN COUNT OF ALL POSTS ACCORDING TO PLACE NAME
    Fun.authenticate(req, res, () => {
        console.log("placeAndCount");
        Post.distinct('place_name', {}, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while getting the posts according to place name.", description: error });
            } else if (data.length == 0) {
                res.json({ error: true, message: "No data found." });
            } else {
                let details = [],
                    count = data.length;
                for (let i = 0; i < data.length; i++) {
                    let obj = { name: String, number: String };
                    Post.find({ place_name: data[i], is_deleted: false }, function (error, result) {
                        count--;
                        if (error) {
                            res.json({ error: true, message: "Error while getting distinct post by Place Name", description: error });
                        } else if (result.length == 0) {
                            if (data[i] != "") {
                                obj.name = data[i];
                                obj.number = result.length;
                                details.push(obj);
                            }
                        } else {
                            if (data[i] != "") {
                                obj.name = data[i];
                                obj.number = result.length;
                                details.push(obj);
                            }
                        }
                        if (count == 0) {
                            res.json({ error: false, message: "Done", description: details })
                        }
                    })
                }
            }
        })
    })
};

exports.repost = function (req, res) {                                                  //   TO REPOST
    Fun.authenticate(req, res, () => {
        console.log("repost");
        let post_id = req.body.post_id,
            user_id = req.body.user_id;

        User.findOne({ _id: user_id }, (err, userData) => {
            if (err) {
                res.json({ error: true, message: "Something went wrong. " + err.message });
            }
            else {

                if (userData.unlimited_post_count.status) {
                    repostPost(post_id)
                } else {

                    if (userData.unlimited_post_count.count > 0) {
                        //let count = userData.available_post_count - 1;
                        let count = userData.unlimited_post_count.count - 1;
                        let updatedUnlimitedPostObject = { status: userData.unlimited_post_count.status, activation: userData.unlimited_post_count.activation, expiry: userData.unlimited_post_count.expiry, count: count }
                        User.update({ _id: user_id }, { $set: { unlimited_post_count: updatedUnlimitedPostObject } }, (err, dataUpdated) => {
                            if (err) {
                                res.json({ error: true, message: "Somehing went wrong. " + err.message });
                            }
                            else {

                                repostPost(post_id)

                                /*Fun.postDetails(post_id, (postData) => {
                                    let p = postData.description;
                                    let posts = new Posts();

                                    let allTags = []; //  String of tags spliting by ',' and storing into database.
                                    for (let i = 0; i < p.tags.length; i++) {
                                        allTags.push({ tag_is: ObjectId(p.tags[i].tag_id) })
                                        if (i == p.tags.length - 1) {
                                            posts.tags = allTags;
                                        }
                                    }

                                    let countryIds = [];
                                    for (let i = 0; i < p.available_country_id.length; i++) {
                                        countryIds.push({ country_id: ObjectId(p.available_country_id[i].country_id) })
                                        if (i == p.available_country_id.length - 1) {
                                            posts.available_country_id = countryIds;
                                        }
                                    }

                                    let allImageUrl = [];
                                    for (let i = 0; i < p.post_images.length; i++) {
                                        allImageUrl.push({ image_url: p.post_images[i].image_url, is_video: p.post_images[i].is_video });
                                    }

                                    let date = new Date().getTime();
                                    posts.description = p.description;
                                    posts.latitude = p.latitude;
                                    posts.longitude = p.longitude;
                                    posts.place_name = p.place_name;
                                    posts.post_user_id = ObjectId(p.post_user_id);
                                    posts.is_instagram = p.is_instagram;
                                    posts.is_twitter = p.is_twitter;
                                    posts.amount = p.amount;
                                    posts.is_auction = p.is_auction;
                                    posts.buy_now = p.buy_now;
                                    posts.whatsapp_only = p.whatsapp_only;
                                    posts.whatsapp_and_call = p.whatsapp_and_call;
                                    posts.product_serial = p.product_serial;
                                    posts.payment_type = p.payment_type;
                                    posts.language = p.language;
                                    posts.only_user_from_selected_country = p.only_user_from_selected_country;
                                    posts.post_created_at = date;
                                    posts.status = "Available";
                                    posts.is_sold = false;
                                    posts.is_story = p.is_story;
                                    posts.post_images = allImageUrl;
                                    posts.is_promoted = false;
                                    posts.percent = 0;
                                    posts.is_stream = false;
                                    posts.is_deleted = false;

                                    posts.save(function (error, data) {
                                        if (error) {
                                            res.json({ error: true, message: "Could not repost", description: error });
                                        } else {
                                            res.json({ error: false, message: "Repost successully" })
                                            Fun.activity(ObjectId(p.post_user_id), "You repost a post", "post", ObjectId(p._id))
                                        }
                                    })
                                })*/

                            }
                        });
                    } else {
                        res.json({ error: true, message: "You are out of posts." })
                    }

                }
            }

        })
    })

    function repostPost(post_id) {

        Fun.postDetails(post_id, (postData) => {
            let p = postData.description;
            let posts = new Posts();

            let allTags = []; //  String of tags spliting by ',' and storing into database.
            for (let i = 0; i < p.tags.length; i++) {
                allTags.push({ tag_is: ObjectId(p.tags[i].tag_id) })
                if (i == p.tags.length - 1) {
                    posts.tags = allTags;
                }
            }

            let countryIds = [];
            for (let i = 0; i < p.available_country_id.length; i++) {
                countryIds.push({ country_id: ObjectId(p.available_country_id[i].country_id) })
                if (i == p.available_country_id.length - 1) {
                    posts.available_country_id = countryIds;
                }
            }

            let allImageUrl = [];
            for (let i = 0; i < p.post_images.length; i++) {
                allImageUrl.push({ image_url: p.post_images[i].image_url, is_video: p.post_images[i].is_video });
            }

            let date = new Date().getTime();
            posts.description = p.description;
            posts.latitude = p.latitude;
            posts.longitude = p.longitude;
            posts.place_name = p.place_name;
            posts.post_user_id = ObjectId(p.post_user_id);
            posts.is_instagram = p.is_instagram;
            posts.is_twitter = p.is_twitter;
            posts.amount = p.amount;
            posts.is_auction = p.is_auction;
            posts.buy_now = p.buy_now;
            posts.whatsapp_only = p.whatsapp_only;
            posts.whatsapp_and_call = p.whatsapp_and_call;
            posts.product_serial = p.product_serial;
            posts.payment_type = p.payment_type;
            posts.language = p.language;
            posts.only_user_from_selected_country = p.only_user_from_selected_country;
            posts.post_created_at = date;
            posts.status = "Available";
            posts.is_sold = false;
            posts.is_story = p.is_story;
            posts.post_images = allImageUrl;
            posts.is_promoted = false;
            posts.percent = 0;
            posts.is_stream = false;
            posts.is_deleted = false;

            posts.save(function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Could not repost", description: error });
                } else {
                    res.json({ error: false, message: "Repost successully" })
                    Fun.activity(ObjectId(p.post_user_id), "You repost a post", "post", ObjectId(p._id))
                }
            })
        })

    }

};

exports.getOrderDetails = function (req, res) {                                         //   TO GET ALL DETAILS OF A SINGLE ORDER
    Fun.authenticate(req, res, () => {
        console.log("getOrderDetails");
        let order_id = req.body.order_id;
        Order.findOne({ _id: ObjectId(order_id) }, function (err, orderData) {
            if (err) {
                res.json({ error: true, message: "Error while getting order details.", description: err })
            } else if (!orderData) {
                res.json({ error: true, message: "No order found" });
            } else {
                let type;
                if (orderData.type == "post") {
                    type = "allposts";
                } else if (orderData.type == "story") {
                    type = "stories";
                } else if (type == "stream") {
                    type = "streams";
                }

                Order.aggregate([{ $match: { _id: ObjectId(order_id) } },
                { $lookup: { from: type, localField: 'type_id', foreignField: "_id", as: "type_data" } },
                { $lookup: { from: "users", localField: 'user_id', foreignField: "_id", as: "user_data" } },
                { $lookup: { from: "addressinfos", localField: 'address_id', foreignField: "addresses._id", as: "address" } },
                { $lookup: { from: "users", localField: 'seller_id', foreignField: "_id", as: "seller_data" } }
                ], function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while getting an order details", description: error });
                    } else if (data.length == 0) {
                        res.json({ error: true, message: "No order found" });
                    } else {
                        res.json({ error: false, message: "Done", description: data })
                    }
                })
            }
        })
    })
};

exports.basicInfo = function (req, res) {                                               //  TO RETURN BASIC DETAILS
    Fun.authenticate(req, res, () => {
        console.log("basicInfo");
        let obj = {
            POOL_ID: "us-east-2:48f0f72b-9b02-4d68-84ef-4163ce0144d9",
            AUTH_TOKEN: "sk_live_xqfRC2K1i9AcmvOFYke68dUn",
            ENCRYPTION_KEY: "pk_live_fA9qwjrTsbZG6ENo82hlW0IC"
        };
        res.json({ error: false, message: "Done", description: obj })
    })
};