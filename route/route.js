constant = require('./constants')

Otp = require('../models/otps')
Tag = require('../models/tags')
admin = require('../admin/api')
User = require('../models/user')
order = require('../order/order')
Room = require('../models/rooms')
Story = require('../models/story')
Posts = require('../models/posts')
Admin = require('../models/admin')
Order = require('../models/orders')
Streams = require('../models/stream')
Support = require('../models/support')
Messages = require('../models/messages')
Country = require('../models/countries')
Notify = require('../models/notification')
Activity = require('../models/activities')
InApp = require('../models/inAppPurchase')
ReportPost = require('../models/reportedPosts')
ReportUser = require('../models/reportedUsers')
ReportStory = require('../models/reportedStories')
PaymentRequests = require('../models/paymentRequests')

Brain = require('../brainTree/brain')
Funn = require('../functions/function')
FeedBack = require('../feedback/feedback')

const curl = new (require('curl-request'))();

var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var schedule = require('node-schedule');

const appleReceiptVerify = require('node-apple-receipt-verify');

module.exports = function (app, io) {

    app.get('/', function (req, res) {
        // if (req.header('API_KEY') == constant.API_KEY) {
        res.json({ message: "Welcome to mazad api" });
        // } else {
        //     res.json({ message: "Wrong API key" });
        // }
    });

    function authenticate(req, res, done) {
        if (req.header('API_KEY') == constant.API_KEY) {
            if (req.body.version == constant.VERSION) {
                done();
            } else {
                res.json({ error: true, message: "Your version of app is older, Please update" });
            }
        } else {
            res.json({ message: "Wrong API key" });
        }
    };

    function updateUsers(a = "", b = "", c = {}, d = {}, res) {
        let stream = c;
        let userDetails = d;
        User.find(function (error, data) {
            if (error) {
                res.json({ error: false, message: "Something went wrong.", description: error });
            } else {
                for (let i = 0; i < data.length; i++) {
                    // console.log('result  ', data[i].socket_id, '')
                    io.to(data[i].socket_id).emit("updateStream", { error: false, stream: stream, userDetails: userDetails, message: "Done" });
                }
                res.json({ error: false, message: "done", stream: stream, userDetails: userDetails })
            }
        })
    };

    function activity(user_id, msg, type, type_id) { //    TO GENERATE ALL THE ACTIVITIES OF A USER 
        console.log("activity function called")

        User.findOne({ _id: ObjectId(user_id) }, function (error, data) {
            if (data.is_activity == true) {
                if (type == "post") {
                    let time = new Date().getTime();
                    let activity = new Activity({
                        message: msg,
                        user_id: ObjectId(user_id),
                        type: type,
                        type_id: type_id,
                        created_at: time
                    });

                    activity.save(function (error, data) {
                        if (error) {
                            console.log("Something went wrong will saving activity in the database.", error);
                        } else {
                            console.log("activity recorded.");
                        }
                    })
                }
            }
        })
    };

    function doItAfterFetchingUsers(allBids, req, res, result, user_id, stream_id) {
        console.log("funciton calledddddd and here all bids are ", allBids)
        let allImageUrl = [];
        allImageUrl.push({ image_url: req.body.image_url, is_video: false });

        // let obj2 = { user_id: userData.description._id, user_name: userData.description.username }

        let date = new Date().getTime();
        let posts = new Posts();
        posts.description = req.body.description;
        posts.post_user_id = ObjectId(result[0].user_id);
        console.log("user_id is ", user_id)
        posts.amount = req.body.amount;
        posts.status = "approved";
        posts.product_serial = req.body.product_serial;
        posts.payment_type = req.body.payment_type;
        posts.post_created_at = date;
        posts.is_deleted = false;
        posts.is_stream = true;
        posts.post_images = allImageUrl;
        posts.bids = allBids;
        posts.is_sold = false;

        posts.save(function (error, results) {
            if (error) {
                res.json({ error: true, message: "Something went wrong while adding stream as post into database.", description: error });
            } else {
                Posts.aggregate([{ $match: { _id: results._id } },
                { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
                ], function (err, data) {
                    if (err) {
                        res.json({ error: true, message: "Something went wrong.", data: err });
                    } else {
                        if (data) {

                            Streams.aggregate([{ $match: { _id: ObjectId(stream_id) } },
                            { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }, { $unwind: '$comment' },
                            { $lookup: { from: 'users', localField: 'comment.user_id', foreignField: '_id', as: 'comment.user_id' } },
                            {
                                $group: {
                                    _id: {
                                        _id: '$_id',
                                        stream_name: '$stream_name',
                                        user_id: '$userDetails',
                                        status: '$status',
                                        viewerCount: '$viewerCount',
                                        country: '$country',
                                        thumbUrl: '$thumbUrl'
                                    },
                                    'comments': { '$push': '$comment' },
                                }
                            }
                            ], function (error, result) {
                                if (!error) {
                                    User.find(function (error, data) {
                                        if (error) {
                                            console.log("Error while fetching stream.")
                                        } else {
                                            for (let i = 0; i < data.length; i++) {
                                                console.log("emiting")
                                                io.to(data[i].socket_id).emit("updateStream", { error: false, message: "Done", stream: result[0]._id, comments: result[0].comments, userDetails: result[0]._id.user_id[0] });
                                            }
                                        }
                                    })
                                } else {
                                    console.log("Error int the last of addComment");
                                }
                            })
                        }
                    }
                })
            }
        })

        console.log("just above res")
        res.json({ error: false, message: "Bid awarded successfully", description: result })
        console.log("just bellow res")
        Fun.notification(result[0].user_id, user_id, "approved your bid of stream", "Bid Approved", "")

        Streams.aggregate([{ $match: { _id: ObjectId(stream_id) } },
        { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }, { $unwind: '$comment' },
        { $lookup: { from: 'users', localField: 'comment.user_id', foreignField: '_id', as: 'comment.user_id' } },
        {
            $group: {
                _id: {
                    _id: '$_id',
                    stream_name: '$stream_name',
                    user_id: '$userDetails',
                    status: '$status',
                    viewerCount: '$viewerCount',
                    country: '$country',
                    thumbUrl: '$thumbUrl'
                },
                'comments': { '$push': '$comment' },
            }
        }
        ], function (error, result) {
            if (!error) {
                console.log({ error: false, message: "Done" })
            } else {
                console.log("Error int the last of addComment");
            }
        })
    };

    exports.emitToUser = function (socketId, response) {
        console.log("socket called >>>>>>>>>>>>>>>>>", socketId)
        io.to(socketId).emit('idVerification', { error: false, message: "Done", description: response })
    };

    exports.soldEmitToAll = function (type, type_id) {
        console.log("emiting")
        User.find(function (error, data) {
            if (error) {
                console.log({ error: true, message: "Error while emiting to users that product has been sold", description: error });
            } else if (data.length == 0) {
                console.log({ error: true, message: "No user found" });
            } else {
                for (i = 0; i < data.length; i++) {
                    io.to(data[i].socket_id).emit('sold', { type: type, type_id: type_id })
                }
            }
        })
    };

    appleReceiptVerify.config({                                                     //  This is in app purchase credentials
        secret: '16fe22f772044887b07692fd5d6e4cdd'                                  //  Need to be changed after testing
        // environment: ['sandbox']
    });

    schedule.scheduleJob('* * 23 * * *', function () {                               //  This is for check the expiry date of unlimited count
        currentDate = Date.now();
        console.log("current date in public messages ", currentDate);
        InApp.find(function (error, data) {
            if (error) {
                console.log({ error: true, message: "Error while getting all in app purchase data", description: error });
            } else if (data.length == 0) { console.log("nothing found") } else {
                for (let i = 0; i < data.length; i++) {
                    console.log("Data lenght => ", data.length)
                    if (data[i].appple_receipt != "") {
                        console.log("Apple receipt is not Null")
                        Funn.userDetails(data[i].user_id, (yes) => {
                            if (yes.error == false) {
                                let user = yes.description;
                                if (user.unlimited_post_count.status == true) {
                                    if (user.unlimited_post_count.expiry <= currentDate) {
                                        console.log("local expiry is less than current date post")
                                        let obj = { status: false, activation: "", expiry: "", count: user.unlimited_post_count.count };
                                        let newObj = { unlimited_post_count: obj };
                                        changeStatus(user._id, newObj);
                                    }
                                }
                                if (user.unlimited_story_count.status == true) {
                                    if (user.unlimited_story_count.expiry <= currentDate) {
                                        console.log("local expiry is less than current date story")
                                        let obj = { status: false, activation: "", expiry: "", count: user.unlimited_story_count.count };
                                        let newObj = { unlimited_story_count: obj };
                                        changeStatus(user._id, newObj);
                                    }
                                }
                                if (user.unlimited_live_count.status == true) {
                                    if (user.unlimited_live_count.expiry <= currentDate) {
                                        console.log("local expiry is less than current date live")
                                        let obj = { status: false, activation: "", expiry: "", count: user.unlimited_live_count.count };
                                        let newObj = { unlimited_live_count: obj };
                                        changeStatus(user._id, newObj);
                                    }
                                }
                            }
                        })

                        appleReceiptVerify.validate({
                            receipt: data[i].apple_receipt
                        }, function (err, products) {
                            if (err) {
                                return console.error(err);
                            } else {
                                console.log("Apple products are =>>> ", products);

                                for (let a = 0; a < data[i].ios_response.length; a++) {

                                    console.log("A isssss => ", a)

                                    //console.log("1 >>>>>>>>>>>>>> ",data[i].ios_response);

                                    console.log("data[i].ios_response[a].originalTransactionId  >>>>  ", data[i].ios_response[a].originalTransactionId)

                                    let filteredProduct = []; //= products.filter(p => p.originalTransactionId == data[i].ios_response[a].originalTransactionId);

                                    products.forEach(element => {

                                        if (element.originalTransactionId == data[i].ios_response[a].originalTransactionId) {
                                            filteredProduct.push(element);
                                        }
                                    });

                                    console.log("filteredProduct =>>> ", filteredProduct)

                                    if (filteredProduct.length == 0) {
                                        continue;
                                    }

                                    let sortedProduct = filteredProduct.sort((p1, p2) => {
                                        return (p1.expirationDate > p2.expirationDate) ? -1 : 1;
                                    })

                                    console.log("sortedProduct =>>> ", sortedProduct)

                                    let product = sortedProduct[0]

                                    console.log("PRODUCT =>>> ", product)

                                    // for cancellation check
                                    if (product.cancellation_date) {
                                        console.log("Cancellation date Here 1")
                                        if (product.cancellation_date != null || product.cancellation_date != "") {
                                            console.log("Cancellation date is not null")
                                            Funn.userDetails(data[i].user_id, (result) => {
                                                if (result.error == false) {
                                                    let user = result.description;
                                                    console.log("found user in cancellation date")
                                                    console.log("productId in cancellation date =>> ", product.productId)
                                                    switch (product.productId) {

                                                        case "AutoRenewableUnlimitedPostsForOneMonth1":

                                                            let obj = { status: false, activation: "", expiry: "", count: user.unlimited_post_count.count };
                                                            let newObj = { unlimited_post_count: obj }
                                                            changeAferCancellation(data[i].user_id, newObj);

                                                            InApp.update({ _id: ObjectId(data[i]._id), "ios_response.originalTransactionId": product.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": product.originalTransactionId } } }, function (error, updateData) {
                                                                if (error) {
                                                                    console.log("error while pulll in cancellation date post ", error.message);
                                                                }
                                                                else {
                                                                    console.log("successfully pulled in cancellation date post");
                                                                }
                                                            })

                                                            break;

                                                        case "AutoRenewableUnlimitedStoriesForOneMonth1":

                                                            let obj1 = { status: false, activation: "", expiry: "", count: user.unlimited_story_count.count };
                                                            let newObj1 = { unlimited_story_count: obj1 }
                                                            changeAferCancellation(data[i].user_id, newObj1);

                                                            InApp.update({ _id: ObjectId(data[i]._id), "ios_response.originalTransactionId": product.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": product.originalTransactionId } } }, function (error, updateData) {
                                                                if (error) {
                                                                    console.log("error while pulll in cancellation date story", error.message);
                                                                }
                                                                else {
                                                                    console.log("successfully pulled in cancellation date story");
                                                                }
                                                            })

                                                            break;

                                                        case "AutoRenewableUnlimitedLiveStreamsForOneMonth1":

                                                            let obj2 = { status: false, activation: "", expiry: "", count: user.unlimited_live_count.count };
                                                            let newObj2 = { unlimited_live_count: obj2 }
                                                            changeAferCancellation(data[i].user_id, newObj2);

                                                            InApp.update({ _id: ObjectId(data[i]._id), "ios_response.originalTransactionId": product.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": product.originalTransactionId } } }, function (error, updateData) {
                                                                if (error) {
                                                                    console.log("error while pulll in cancellation date live", error.message);
                                                                }
                                                                else {
                                                                    console.log("successfully pulled in cancellation date live");
                                                                }
                                                            })

                                                            break;

                                                        default:

                                                    }
                                                }
                                            })
                                        }
                                    }
                                    // for renew check
                                    if (product.auto_renew_status) {
                                        console.log("Auto renew key Here 2")
                                        if (product.auto_renew_status == "true" || product.auto_renew_status == true) {
                                            console.log("AUTO renew Key is true")
                                            Funn.userDetails(data[i].user_id, (result) => {
                                                if (result.error == false) {

                                                    let user = result.description;

                                                    switch (product.productId) {

                                                        case "AutoRenewableUnlimitedPostsForOneMonth1":

                                                            let obj = { status: true, activation: product.purchaseDate, expiry: product.expirationDate, count: user.unlimited_post_count.count };
                                                            let newObj = { unlimited_post_count: obj }
                                                            changeAferCancellation(data[i].user_id, newObj);

                                                            InApp.update({ _id: ObjectId(data[i]._id), "ios_response.originalTransactionId": product.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": product.originalTransactionId } } }, function (error, updateData) {
                                                                if (error) {
                                                                    console.log("error while pulll ", error.message);
                                                                }
                                                                else {
                                                                    console.log("pull successfully posts auto renew");
                                                                    InApp.findOneAndUpdate({ _id: ObjectId(data[i]._id) }, { $push: { ios_response: product } }, function (err, done) {
                                                                        if (err) {
                                                                            console.log("error while pushing in posts renewcheck ", err.message);
                                                                        }
                                                                        else {
                                                                            console.log("push coplete, Everything is done post renewcheck");
                                                                        }
                                                                    })
                                                                }
                                                            })

                                                            break;

                                                        case "AutoRenewableUnlimitedStoriesForOneMonth1":

                                                            let obj1 = { status: true, activation: product.purchaseDate, expiry: product.expirationDate, count: user.unlimited_story_count.count };
                                                            let newObj1 = { unlimited_story_count: obj1 }
                                                            changeAferCancellation(data[i].user_id, newObj1);

                                                            InApp.update({ _id: ObjectId(data[i]._id), "ios_response.originalTransactionId": product.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": product.originalTransactionId } } }, function (error, updateData) {
                                                                if (error) {
                                                                    console.log("error while pulll story renewcheck ", error.message);
                                                                }
                                                                else {
                                                                    console.log("pull successfully story renewcheck");
                                                                    InApp.findOneAndUpdate({ _id: ObjectId(data[i]._id) }, { $push: { ios_response: product } }, function (err, done) {
                                                                        if (err) {
                                                                            console.log("error while pushing in story renewcheck ", err.message);
                                                                        }
                                                                        else {
                                                                            console.log("push coplete, Everything is done story renewcheck");
                                                                        }
                                                                    })
                                                                }
                                                            })


                                                            break;

                                                        case "AutoRenewableUnlimitedLiveStreamsForOneMonth1":

                                                            let obj2 = { status: true, activation: product.purchaseDate, expiry: product.expirationDate, count: user.unlimited_live_count.count };
                                                            let newObj2 = { unlimited_live_count: obj2 }
                                                            changeAferCancellation(data[i].user_id, newObj2);

                                                            InApp.update({ _id: ObjectId(data[i]._id), "ios_response.originalTransactionId": product.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": product.originalTransactionId } } }, function (error, updateData) {
                                                                if (error) {
                                                                    console.log("error while pulll ", error.message);
                                                                }
                                                                else {
                                                                    console.log("pull successfully live auto renew");
                                                                    InApp.findOneAndUpdate({ _id: ObjectId(data[i]._id) }, { $push: { ios_response: product } }, function (err, done) {
                                                                        if (err) {
                                                                            console.log("error while pushing live renewcheck ", err.message);
                                                                        }
                                                                        else {
                                                                            console.log("push coplete, Everything is done in live renewcheck");
                                                                        }
                                                                    })
                                                                }
                                                            })

                                                            break;

                                                        default:

                                                    }
                                                }
                                            })
                                        }
                                    }

                                    // for expiry date check 
                                    console.log("Apple (product.expirationDate) =>> ", product.expirationDate)
                                    console.log("LOCAL (data[i].ios_response[a].expirationDate) =>> ", data[i].ios_response[a].expirationDate)

                                    if (parseInt(product.expirationDate) > parseInt(data[i].ios_response[a].expirationDate)) {

                                        console.log("FOR EXPIRy DATE CHECK IFFF")
                                        Funn.userDetails(data[i].user_id, (result) => {
                                            console.log("FOR EXPIRy DATE CHECK  3")
                                            if (result.error == false) {
                                                console.log("FOR EXPIRy DATE CHECK  4")
                                                let user = result.description;
                                                console.log("expiry check product id is =>", product.productId)
                                                switch (product.productId) {

                                                    case "AutoRenewableUnlimitedPostsForOneMonth1":

                                                        console.log("FOR EXPIRy DATE CHECK 5")

                                                        let obj = { status: true, activation: product.purchaseDate, expiry: product.expirationDate, count: user.unlimited_post_count.count };
                                                        let newObj = { unlimited_post_count: obj }
                                                        changeAferCancellation(data[i].user_id, newObj);

                                                        InApp.update({ _id: ObjectId(data[i]._id), "ios_response.originalTransactionId": product.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": product.originalTransactionId } } }, function (error, updateData) {
                                                            if (error) {
                                                                console.log("error while pulll in expiry date check post ", error.message);
                                                            }
                                                            else {
                                                                console.log("pull successfully in expiry date check post");
                                                                InApp.findOneAndUpdate({ _id: ObjectId(data[i]._id) }, { $push: { ios_response: product } }, function (err, done) {
                                                                    if (err) {
                                                                        console.log("error while pushing in expiry date check post ", err.message);
                                                                    }
                                                                    else {
                                                                        console.log("push coplete, Everything is done expiry date check post ");
                                                                    }
                                                                })
                                                            }
                                                        })

                                                        break;

                                                    case "AutoRenewableUnlimitedStoriesForOneMonth1":

                                                        let obj1 = { status: true, activation: product.purchaseDate, expiry: product.expirationDate, count: user.unlimited_story_count.count };
                                                        let newObj1 = { unlimited_story_count: obj1 }
                                                        changeAferCancellation(data[i].user_id, newObj1);

                                                        InApp.update({ _id: ObjectId(data[i]._id), "ios_response.originalTransactionId": product.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": product.originalTransactionId } } }, function (error, updateData) {
                                                            if (error) {
                                                                console.log("error while pulll expiry date check story ", error.message);
                                                            }
                                                            else {
                                                                console.log("pull successfully in expiry date check story");
                                                                InApp.findOneAndUpdate({ _id: ObjectId(data[i]._id) }, { $push: { ios_response: product } }, function (err, done) {
                                                                    if (err) {
                                                                        console.log("error while pushing in expiry date check story ", err.message);
                                                                    }
                                                                    else {
                                                                        console.log("push coplete, Everything is done in expiry date check story ");
                                                                    }
                                                                })
                                                            }
                                                        })

                                                        break;

                                                    case "AutoRenewableUnlimitedLiveStreamsForOneMonth1":

                                                        console.log("asfasdf HEREEE")

                                                        let obj2 = { status: true, activation: product.purchaseDate, expiry: product.expirationDate, count: user.unlimited_live_count.count };
                                                        console.log("obj2 =>>>> ", obj2)
                                                        let newObj2 = { unlimited_live_count: obj2 }

                                                        changeAferCancellation(data[i].user_id, newObj2);

                                                        InApp.update({ _id: ObjectId(data[i]._id), "ios_response.originalTransactionId": product.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": product.originalTransactionId } } }, function (error, updateData) {
                                                            if (error) {
                                                                console.log("error while pulll in expiry date check live ", error.message);
                                                            }
                                                            else {
                                                                console.log("pull successfull in expiry date check live");
                                                                InApp.findOneAndUpdate({ _id: ObjectId(data[i]._id) }, { $push: { ios_response: product } }, function (err, done) {
                                                                    if (err) {
                                                                        console.log("error while pushing in expiry date check live ", err.message);
                                                                    }
                                                                    else {
                                                                        console.log("push coplete, Everything is done in expiry date check post");
                                                                    }
                                                                })
                                                            }
                                                        })

                                                        break;

                                                    default:

                                                }
                                            }
                                        })
                                    } else {
                                        console.log("FOR EXPIRY DATE CHECK ELSEEE")
                                    }
                                }

                            }
                        })
                    }
                }
            }
        })
    });

    function changeAferCancellation(id, obj) {
        User.findOneAndUpdate({ _id: ObjectId(id) }, { $set: obj }, function (error, data) { })
    }

    function changeStatus(user_id, obj) {
        User.updateOne({ _id: ObjectId(user_id) }, { $set: obj }, function (error, done) {
            if (error) {
                console.log({ error: true, message: "Error while updating " + data[i].user_id + " user", description: error });
            } else {
                console.log("User unlimited count updated")
            }
        })
    }



    app.post('/tagsA', admin.tags);

    app.post('/allPostA', admin.allPost);

    app.post('/sendMailA', admin.sendMail);

    app.post('/setBadgeA', admin.setBadge);

    app.post('/allStoryA', admin.allStoryA);

    app.post('/userListA', admin.userListA);

    app.post('/allPostsA', admin.allPostsA);

    app.post('/orderListA', admin.orderList);

    app.post('/getMyPostA', admin.getMyPostA);

    app.post('/verifyUserA', admin.verifyUser);

    app.post('/deviceListA', admin.deviceList);

    app.post('/adminLoginA', admin.loginAdmin);

    app.post('/postDetailA', admin.postDetail);

    app.post('/reportListA', admin.reportList);

    app.post('/orderCountA', admin.orderCount);

    app.post('/adminBlockA', admin.adminBlock);

    app.post('/userStoriesA', admin.userStories);

    app.post('/orderDetailA', admin.orderDetail);

    app.post('/userListAllA', admin.userListAll);

    app.post('/storyDetailA', admin.storyDetail);

    app.post('/userDetailIdA', admin.userDetailId);

    app.post('/countRequestA', admin.countRequest);

    app.post('/unReportUserA', admin.unReportUser);

    app.post('/updateCountA', admin.updateAllCount);

    app.post('/userListRegexA', admin.userListRegex);

    app.post('/reportPostListA', admin.reportPostList);

    app.post('/verifyAdminOtpA', admin.verifyAdminOtp);

    app.post('/reportStoryListA', admin.reportStoryList);

    app.post('/totalDeviceListA', admin.totalDeviceList);

    app.post('/requestedToAdminA', admin.requestToAdmin);

    app.post('/deviceListRegexA', admin.deviceListRegex);

    app.post('/userStoriesCountA', admin.userStoriesCount);

    app.post('/analyseDailyPostA', admin.analyseDailyPost);

    app.post('/userListVerifiedA', admin.userListVerified);

    app.post('/approvedPostListA', admin.approvedPostList);

    app.post('/verificationListA', admin.verificationList);

    app.post('/reportUserDetailsA', admin.reportUserDetails);

    app.post('/approvedStoryListA', admin.approvedStoryList);

    app.post('/deleteReportedPostA', admin.deleteReportedPost);

    app.post('/givingPostCountAllA', admin.givingPostCountAll);

    app.post('/removeProfilePhotoA', admin.removeProfilePhoto);

    app.post('/userBlockListRegexA', admin.userBlockListRegex);

    app.post('/discardReportedPostA', admin.discardReportedPost);

    app.post('/ChangePaymentStatusA', admin.ChangePaymentStatus);

    app.post('/UpdateAdminPasswordA', admin.UpdateAdminPassword);

    app.post('/deleteReportedStoryA', admin.deleteReportedStory);

    app.post('/discardReportedStoryA', admin.discardReportedStory);

    app.post('/PassPaymentFromAdminA', admin.PassPaymentFromAdmin);

    app.post('/userListVerifiedRegexA', admin.userListVerifiedRegex);

    app.post('/adminChangesYourPasswordA', admin.adminChangesYourPassword);




    // ------------------------------------------------------------ END OF ADMIN API'S -------------------------------------------------------------------


    app.post('/repost', order.repost);

    app.post('/checkout', Brain.checkout);

    app.post('/orderOtp', order.orderOtp);

    app.post('/basicInfo', order.basicInfo);

    app.post('/noMoreAds', order.noMoreAds);

    app.post('/orderList', order.orderList);

    app.post('/walletList', order.walletList);

    app.post('/deletePost', order.deletePost);

    app.post('/addAddress', order.addAddress);

    app.post('/showCounts', order.showCounts);

    app.post('/popularPost', order.popularPost);

    app.post('/sellerOrder', order.sellerOrder);

    app.post('/createOrder', order.createOrder);

    app.post('/deleteStory', order.deleteStory);

    app.post('/client_token', Brain.client_token);

    app.post('/placeAndCount', order.placeAndCount);

    app.post('/autoConfirmed', order.autoConfirmed);

    app.post('/tapCustomerId', order.tapCustomerId);

    app.post('/updatePromote', order.updatePromote);

    app.post('/verifyOrderOtp', order.verifyOrderOtp);

    app.post('/postBidDetails', order.postBidDetails);

    app.post('/updateAllCount', order.updateAllCount);

    app.post('/changeBidStatus', order.changeBidStatus);

    app.post('/getOrderDetails', order.getOrderDetails);

    app.post('/categoryAndCount', order.categoryAndCount);

    app.post('/userVerification', order.userVerification);

    app.post('/is_deletedStatus', order.is_deletedStatus);

    app.post('/deliveryConfirmed', order.deliveryConfirmed);

    app.post('/getCurrentProduct', order.getCurrentProduct);

    app.post('/feedbackReceived', FeedBack.feedbackReceived);

    app.post('/addFeedbackOnPost', FeedBack.addFeedbackOnPost);

    app.post('/verificationPayment', order.verificationPayment);

    app.post('/verificationDetails', order.verificationDetails);

    app.post('/addFeedbackOnStory', FeedBack.addFeedbackOnStory);

    app.post('/notificationAndEmail', order.notificationAndEmail);

    app.post('/requestPaymentToAdmin', order.requestPaymentToAdmin);

    app.post('/pendingFeedbackOrder', FeedBack.pendingFeedbackOrder);

    app.post('/acceptOrderRequestBySeller', order.acceptOrderRequestBySeller);

    // app.post('/update_post_count', order.update_post_count);





    app.post('/activities', function (req, res) {                                                 //    TO GET ALL THE ACTIVITIES OF A USER
        authenticate(req, res, () => {
            console.log("activities");
            let user_id = req.body.user_id;
            Activity.find({ user_id: ObjectId(user_id) }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting activity list of user.", description: error });
                } else if (data.length == 0) {
                    res.json({ error: true, message: "No activities found" });
                } else {
                    let wholeData = [];
                    for (let i = 0; i < data.length; i++) {
                        let type = data[i].type,
                            user = "users",
                            address = "addressinfos";
                        if (type == "post") {
                            type = "allposts";
                        }
                        Activity.aggregate([{ $match: { _id: ObjectId(data[i]._id) } },
                        { $lookup: { from: type, localField: 'type_id', foreignField: "_id", as: "type_data" } },
                        { $lookup: { from: user, localField: 'user_id', foreignField: "_id", as: "user_data" } },
                        ], function (error, activityDetails) {
                            if (error) {
                                console.log("1111111111111 and i is ", i, " and err is ", error);
                            } else if (data.length == 0) {
                                console.log("2")
                            } else {
                                wholeData.push(activityDetails[0]);
                                if (wholeData.length == data.length) {
                                    res.json({ error: false, message: "Done", description: wholeData })
                                }
                            }
                        })
                    }
                }
            })
        })
    });

    app.post('/activityStatus', function (req, res) {                                             //    TO ON / OFF ACTIVITIES OF A USER
        Funn.authenticate(req, res, () => {
            console.log("activityStatus");
            let user_id = req.body.user_id,
                status = req.body.status;
            User.findOneAndUpdate({ _id: ObjectId(user_id) }, { $set: { is_activity: status } }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while blocking user.", description: error });
                } else if (!data) {
                    res.json({ error: true, message: "User not found." });
                } else {
                    Funn.userDetails(user_id, (userData) => {
                        res.json(userData);
                    })
                }
            })
        })
    });

    app.post('/awardBid', function (req, res) {                                                   //    TO WARD THE BID OF LIVE STREAM(N)
        Fun.authenticate(req, res, () => {
            console.log("awardBid", req.body);
            let stream_id = req.body.stream_id,
                status = "approved",
                user_id = req.body.user_id;

            let allTags = req.body.bid_id.split(",");
            let tagToPut = [];
            for (let i = 0; i < allTags.length; i++) {
                let tagObject = ObjectId(allTags[i]);
                tagToPut.push(tagObject);
            }

            Streams.update({ _id: ObjectId(stream_id) }, { $set: { "comment.$[elem].status": status } }, { arrayFilters: [{ "elem._id": { $in: tagToPut } }] },
                function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while updating award bid.", description: error });
                    } else if (data.length == 0) {
                        res.json({ error: true, message: "No data found " });
                    } else {
                        Streams.aggregate([{ $match: { _id: ObjectId(stream_id) } },
                        { $lookup: { from: "users", localField: "user_id", foreignField: "_id", as: "userDetails" } }
                        ], function (error, result) {
                            if (error) {
                                res.json({ error: true, message: "Failed to award bid.", description: error });

                            } else if (result.length == 0) {
                                res.json({ error: true, message: "Failed to find stream" });
                            } else {
                                let bidData = [];
                                for (let i = 0; i < result[0].comment.length; i++) {
                                    if (result[0].comment[i].status == "approved") {
                                        bidData.push(result[0].comment[i]);
                                    }

                                    if (i == result[0].comment.length - 1) {
                                        let allBids = [];
                                        console.log("here bidData's length is ", bidData.length)

                                        for (let j = 0; j < bidData.length; j++) {
                                            Funn.userDetails(bidData[j].user_id, (userData) => {

                                                if (userData.error == false) {
                                                    let obj = {
                                                        bid_user_id: bidData[j].user_id,
                                                        bid_amount: bidData[j].amount,
                                                        bid_created_at: bidData[j].created_at,
                                                        status: bidData[j].status,
                                                        user_name: userData.description.username,
                                                        url: userData.description.profilePhoto,
                                                        country: userData.description.country,
                                                        is_paid: false
                                                    }
                                                    allBids.push(obj);
                                                    console.log("all bids i am going to check and status is ", allBids)
                                                    if (bidData.length == allBids.length) {
                                                        doItAfterFetchingUsers(allBids, req, res, result, user_id, stream_id);
                                                    }
                                                }
                                            })
                                        }
                                    }
                                }
                            }
                        })
                    }
                })
        })
    });

    app.post('/bidOnStory', function (req, res) {                                                 //    TO MAKE BID ON STORY (N & A)
        authenticate(req, res, () => {
            console.log("bidOnStory")
            let time = new Date().getTime();
            let user_id = req.body.user_id,
                text = req.body.text,
                bid_created_at = time,
                story_id = req.body.story_id,
                type = req.body.type;
            let obj = { user_id: ObjectId(user_id), created_at: time, story_id: ObjectId(story_id) };
            if (type == "comment") {
                obj.comment = req.body.text;
            } else {
                obj.amount = req.body.text;
            }
            Story.findOneAndUpdate({ _id: ObjectId(story_id), status: "running" }, { $pull: { bidsAndComments: { user_id: ObjectId(user_id) } } }, function (error, bidData) {
                if (error) {
                    res.json({ error: true, message: "Error while bidding on story", description: error });
                } else if (!bidData) {
                    makeBid();
                } else {
                    Funn.userDetails(user_id, (userData) => {
                        if (userData.error == false) {
                            let user = userData.description;
                            obj.user_name = user.username, obj.url = user.profilePhoto, obj.country = user.country;

                            Story.findOneAndUpdate({ _id: ObjectId(story_id), status: "running" }, { $push: { bidsAndComments: obj } }, function (error, data) {
                                if (error) {
                                    res.json({ error: true, message: "Error while bidding on story", description: error });
                                } else if (!data) {
                                    res.json({ error: true, message: "No match found" });
                                } else {
                                    Story.findOne({ _id: ObjectId(story_id) }, function (error, result) {
                                        if (error) {
                                            res.json({ error: true, message: "Bid done but cound not found.", description: error })
                                        } else if (!result) {
                                            res.json({ error: true, message: "Bid done but could not found." });
                                        } else {
                                            res.json({ error: false, message: "Done", description: result });
                                            activity(ObjectId(user_id), "You made bid on story on", "Bid on Story", ObjectId(story_id));
                                            Funn.notification(user_id, data.user_id, 'made bid on you story.', 'Bid on story', data)
                                        }
                                    })
                                }
                            })
                        } else {
                            res.json({ error: true, message: "Something went wrong" })
                        }
                    })
                }
            })

            function makeBid() {
                Story.findOneAndUpdate({ _id: ObjectId(story_id), status: "running" }, { $push: { bidsAndComments: obj } }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while bidding on story", description: error });
                    } else if (!data) {
                        res.json({ error: true, message: "No match found" });
                    } else {
                        Story.findOne({ _id: ObjectId(story_id) }, function (error, result) {
                            if (error) {
                                res.json({ error: true, message: "Bid done but cound not found.", description: error })
                            } else if (!result) {
                                res.json({ error: true, message: "Bid done but could not found." });
                            } else {
                                res.json({ error: false, message: "Done", description: result });
                                activity(ObjectId(user_id), "You made bid on story on", "Bid on Story", ObjectId(story_id));
                                Funn.notification(user_id, data.user_id, 'made bid on you story.', 'Bid on story', data)
                            }
                        })
                    }
                })
            }
        })
    });

    app.post('/block', function (req, res) {                                                      //    TO BLOCK OR UNBLOCK USER (A)
        authenticate(req, res, () => {
            console.log("Block")
            let self_id = req.body.self_id,
                his_id = req.body.his_id,
                is_block = req.body.is_block,
                time = new Date(),
                obj = ObjectId(his_id),
                query, query2, message;

            if (self_id.toString() != his_id.toString()) {
                if (is_block == "1") {
                    query = { $push: { block_list: obj }, $pull: { following: { user_id: ObjectId(his_id) }, followers: { user_id: ObjectId(his_id) } } }
                    query2 = { $pull: { followers: { user_id: ObjectId(self_id) }, following: { user_id: ObjectId(self_id) } } }
                    message = "You blocked an user";
                } else {
                    query = { $pull: { block_list: obj } }
                    message = "You Unblocked an user";
                }

                User.findOneAndUpdate({ _id: ObjectId(self_id), block_list: { $nin: [ObjectId(his_id)] } }, query, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while blocking user.", description: error });
                    } else if (!data) {
                        User.findOneAndUpdate({ _id: ObjectId(self_id) }, query, function (error, dataa) {
                            if (error) {
                                res.json({ error: true, message: "Error while blocking user.", description: error });
                            } else if (!dataa) {
                                res.json({ error: true, message: "User not found." });
                            } else {
                                Funn.userDetails(his_id, (userData) => {
                                    Funn.userDetails(self_id, (userData2) => {
                                        res.json({ error: false, message: message });
                                        activity(ObjectId(self_id), message, "block/unblock", ObjectId(obj));
                                    })
                                })
                            }
                        })
                    } else {
                        User.findOneAndUpdate({ _id: ObjectId(his_id) }, query2, function (error, newData) {
                            if (error) {
                                res.json({ error: true, message: "Error while blocking user.", description: error });
                            } else if (!data) {
                                res.json({ error: true, message: "User not found." });
                            } else {
                                Funn.userDetails(his_id, (userData) => {
                                    Funn.userDetails(self_id, (userData2) => {
                                        res.json({ error: false, message: message });
                                        activity(ObjectId(self_id), message, "block/unblock", ObjectId(obj));
                                    })
                                })
                            }
                        })
                    }
                })
            } else {
                res.json({ error: false, messages: "You cannot block yourself" })
            }
        })
    });

    app.post('/blockList', function (req, res) {                                                  //    TO SEE THE BLOCK LIST
        authenticate(req, res, () => {
            console.log("blockList")
            let user_id = req.body.user_id;
            User.aggregate([{ $match: { _id: ObjectId(user_id) } },
            { $lookup: { from: "users", localField: "block_list", foreignField: "_id", as: "block_list" } }
            ], function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting block list.", description: error });
                } else if (!data) {
                    res.json({ error: false, message: "User not found." });
                } else {
                    res.json({ error: false, message: "Done", description: data[0].block_list });
                }
            })
        })
    });

    app.post('/bookMark', function (req, res) {                                                   //    TO BOOKMARK ANY BID ON LIVE STREAM (A)
        authenticate(req, res, () => {
            console.log("bookMark")
            let stream_id = req.body.stream_id,
                comment_id = req.body.comment_id,
                is_bookMarked = req.body.is_bookMarked;
            Streams.findOneAndUpdate({ _id: ObjectId(stream_id), "comment._id": ObjectId(comment_id) }, { $set: { "comment.$.is_marked": is_bookMarked } }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while updating ", description: error });
                } else if (!data) {
                    res.json({ error: false, message: "No data found." });
                } else {
                    res.json({ error: false, message: "Done." })
                    activity(ObjectId(data.user_id), "You book marked a comment of a live stream on", "book mark", ObjectId(comment_id));
                }
            })
        })
    });

    app.post('/changePassword', function (req, res) {                                             //    TO CHANGE THE PASSWORD
        authenticate(req, res, () => {
            console.log("changePassword")
            let password = req.body.password;
            let type = req.body.type;

            if (type == "email") {
                email = req.body.email;
                checkStr = { email: email }
            } else if (type == "phone") {
                country_code = req.body.country_code;
                phone = req.body.phone;
                checkStr = { phone: phone, country_code: '+' + country_code }
            }
            User.findOneAndUpdate(checkStr, { $set: { password: password } }, function (err, updated) {
                if (err) {
                    res.json({ error: true, message: "Something went wrong.", data: err });
                } else if (!updated) {
                    res.json({ error: true, message: "Data not found" })
                } else {
                    res.json({ error: false, message: "Password updated successfully" });
                }
            })
        });
    });

    app.post('/checkEmail', function (req, res) {                                                 //    TO CHECK ALREADY EMAIL EXISTS OR NOT
        authenticate(req, res, () => {
            console.log("checkEmail");
            let email = req.body.email;
            User.find({ email: email }, function (error, found) {
                if (error) {
                    res.json({ error: true, message: "Error while matching email for validation", description: error });
                } else if (found.length == 0) {
                    res.json({ error: false, message: "Done" });
                } else {
                    res.json({ error: true, message: "Email already exists" });
                }
            })
        })
    });

    app.post('/checkLastLogin', function (req, res) {                                             //    TO CHECK LAST LOGIN OF USER
        console.log("checkLastLogin: ", req.body.user_id);
        let user_id = req.body.user_id,
            time = req.body.time;

        Funn.userDetails(user_id, (userData) => {
            if (userData.error == false) {
                let user = userData.description;
                if (user.is_admin_block == false) {
                    if (user.last_login != time) {
                        res.json({ error: true, message: "You have been logged in into another device" });
                    } else {
                        res.json({ error: false, message: "Done" })
                    }
                } else {
                    res.json({ error: true, message: "You are not authorised to login" });
                }

            } else {
                res.json({ error: true, message: "Something went wrong in checkLastLogin" })
            }
        })
    });

    app.post('/countries', function (req, res) {                                                  //    TO ADD THE COUNTRY IN THE LIST 
        authenticate(req, res, () => {
            console.log("countries");
            let type = req.body.type;
            switch (type) {
                case 'create':
                    let name = req.body.name;
                    let code = req.body.code;
                    let Abbrevation = req.body.Abbrevation;
                    let imageUrl = req.body.imageUrl;
                    let createdAt = new Date();
                    Country.findOne({ countryName: name }, function (error, user) {
                        if (error) {
                            res.json({ message: error });
                        } else if (!user) {
                            let country = new Country();
                            country.countryName = name;
                            country.countryCode = code;
                            country.countryAbbrevation = Abbrevation;
                            country.flagImageUrl = imageUrl;
                            country.createdAt = createdAt;

                            country.save(function (error, data) {
                                if (error) {
                                    res.json({ error: true, message: "error while adding country in the list", description: error });
                                } else {
                                    res.json({ error: true, message: "Done", description: data });
                                }
                            })
                        } else {
                            res.json({ error: true, message: "This Country already exits." });
                        }
                    })
                    break;


                case 'read':
                    Country.find(function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while fetching Countries List", description: error });
                        } else {
                            res.json({ error: false, message: "Done", description: data });
                        }
                    })
                    break;
            }
        })
    });

    app.post('/createRoom', function (req, res) {                                                 //    TO CREATE ROOM FOR CHATTING
        authenticate(req, res, () => {
            console.log("createRoom");
            let time = new Date().getTime(),
                userIds = [ObjectId(req.body.user_id1), ObjectId(req.body.user_id2)];
            Room.findOne({ $and: [{ user: ObjectId(req.body.user_id1) }, { user: ObjectId(req.body.user_id2) }] }, function (error, result) {
                if (error) {
                    res.json({ error: true, message: error });
                } else if (!result) {
                    let room = new Room({
                        user: userIds,
                        created_at: time
                    });
                    room.save(function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while creating room", description: error });
                        } else {
                            Room.aggregate([{ $match: { _id: ObjectId(data._id) } },
                            { $lookup: { from: 'users', localField: 'user', foreignField: '_id', as: 'usersDetails' } },
                            { $project: { _id: 1, usersDetails: 1, created_at: 1 } },
                            ], function (error, data) {
                                if (error) {
                                    res.json({ error: true, message: error });
                                } else if (!data) {
                                    res.json({ error: true, message: "Room not found." })
                                } else {
                                    res.json({ error: false, message: "Done", description: data[0] })
                                }
                            })
                        }
                    })
                } else {
                    Room.aggregate([{ $match: { _id: ObjectId(result._id) } },
                    { $lookup: { from: 'users', localField: 'user', foreignField: '_id', as: 'usersDetails' } },
                    { $project: { _id: 1, usersDetails: 1, created_at: 1 } },
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, message: error });
                        } else if (!data) {
                            res.json({ error: true, message: "Room not found." })
                        } else {
                            res.json({ error: false, message: "Done", description: data[0] })
                        }
                    })
                }
            })

        })
    });

    app.post('/createStream', function (req, res) {                                               //    TO CREATE STREAMS  (N & A)
        console.log("createStreamAPI")
        let time = new Date(),
            country = req.body.country_id.split(',');
        countryIds = [];
        for (let i = 0; i < country.length; i++) {
            countryIds.push({ country_id: ObjectId(country[i]) })
        }

        Funn.stopStream(req.body.user_id, () => {
            let stream = new Streams();
            let user_id = ObjectId(req.body.user_id);
            stream.user_id = user_id;
            stream.stream_name = req.body.stream_name;
            stream.status = status = req.body.status;
            stream.thumbUrl = req.body.thumbUrl;
            stream.language = req.body.language;
            stream.created_at = time;
            stream.country = countryIds;
            stream.start_price = req.body.start_price;


            stream.save(function (error, data) {
                if (error) {
                    res.json({ error: true, message: 'Error while saving stream details.', description: error });
                } else {
                    let stream = data;
                    User.findOneAndUpdate({ _id: user_id }, { is_live: status }, function (error, done) {
                        if (error) {
                            res.json({ error: true, message: 'Error while updating is_live running in users collection(database).', description: error });
                        } else if (!done) {
                            res.json({ error: true, message: "No match found while updating is_live running in users collection(database)" });
                        } else {
                            let userDetails = done;
                            User.find(function (error, result) {
                                if (error) {
                                    res.json({ error: true, message: "Error", description: error });
                                } else if (!result) {
                                    res.json({ error: false, message: "No data found." });
                                } else {
                                    for (let i = 0; i < result.length; i++) {
                                        io.to(result[i].socket_id).emit("updateStream", { error: false, stream: stream, userDetails: userDetails });
                                    }
                                    User.findOne({ _id: ObjectId(user_id) }, function (error, userData) {
                                        if (error) {
                                            res.json({ error: "true", message: "Notification could not generate.", description: error });
                                        } else if (!userData) {
                                            res.json({ error: true, message: "User not found while generating notification." });
                                        } else {
                                            userData.followers.forEach(element => {
                                                Funn.notification(ObjectId(user_id), ObjectId(element.user_id), "Started live streaming.", "Live", stream);
                                            });
                                            activity(ObjectId(user_id), "You created this stream on", "stream created", ObjectId(stream._id));
                                        }
                                    })
                                    res.json({ error: false, message: "API response", stream: stream, userDetails: userDetails })
                                    Funn.updateCount(req.body.post_user_id, "stream", (data) => { })
                                }
                            })
                        }
                    })
                }
            })
        })
    });

    app.post('/favPost', function (req, res) {                                                    //    TO ADD FAVOURITE POST IN THE USER LIST 
        authenticate(req, res, () => {
            console.log("favPost");
            let user_id = ObjectId(req.body.user_id);
            let post_id = ObjectId(req.body.post_id);
            let is_fav = req.body.is_fav;
            if (is_fav.toString() == "true") {
                User.findOneAndUpdate({ _id: ObjectId(user_id) }, { $push: { favPost: { post_id: ObjectId(post_id) } } }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while updating favPost in user collection(database)", description: error });
                    } else if (!data) {
                        res.json({ error: false, message: "No data found" });
                    } else {
                        User.findOne({ _id: ObjectId(user_id) }, function (error, result) {
                            if (error) {
                                res.json({ error: true, message: "Something went wrong in the last.", description: error });
                            } else {
                                res.json({ error: false, message: "Done", description: result });
                                activity(ObjectId(user_id), "You marked this as your favourite post", "post", ObjectId(post_id));
                            }
                        })
                    }
                })
            } else {
                User.findOneAndUpdate({ _id: ObjectId(user_id) }, { $pull: { favPost: { post_id: ObjectId(post_id) } } }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while updating favPost in user collection(database)", description: error });
                    } else if (!data) {
                        res.json({ error: false, message: "No data found" });
                    } else {
                        User.findOne({ _id: ObjectId(user_id) }, function (error, result) {
                            if (error) {
                                res.json({ error: true, message: "Something went wrong in the last.", description: error });
                            } else {
                                res.json({ error: false, message: "Done", description: result });
                                activity(ObjectId(user_id), "You removed this post from your favourite list", "post", ObjectId(post_id));
                            }
                        })
                    }
                })
            }
        })
    });

    app.post('/feedBack', function (req, res) {                                                   //    TO GIVE FEEDBACK ON POST(N & A)
        authenticate(req, res, () => {
            console.log("feedBack");
            let time = new Date().getTime();
            let post_id = req.body.post_id,
                user_id = req.body.user_id,
                feedBack = req.body.feedBack;
            let obj = { user_id: ObjectId(user_id), feed_back: feedBack, created_at: time };
            Posts.findOneAndUpdate({ _id: ObjectId(post_id) }, { $push: { feed_back: obj } }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while updating feedbacks of a post", description: error });
                } else if (!data) {
                    res.json({ error: true, message: "No data found" });
                } else {
                    Posts.findOne({ _id: ObjectId(post_id) }, function (error, result) {
                        if (error) {
                            res.json({ error: true, message: "Error after updating the feedBack array", description: error });
                        } else {
                            res.json({ error: false, message: "Done", description: result.feed_back });
                            activity(ObjectId(user_id), "You gave your feedback on this activity on", "feedBack", ObjectId(post_id));
                            Funn.notification(ObjectId(user_id), ObjectId(data[0].post_user_id), "gave his feed back.", "Feed Back", data);
                        }
                    })

                }
            })
        })
    });

    app.post('/feedBackData', function (req, res) {                                               //    TO GET ALL THE FEEDBACKS ON ALL THE POSTS OF A USER
        authenticate(req, res, () => {
            console.log("feedBackData");
            let user_id = req.body.user_id;
            Posts.aggregate([{ $match: { post_user_id: ObjectId(user_id) } },
            { $unwind: '$feed_back' },
            { $lookup: { from: "users", localField: "feed_back.user_id", foreignField: "_id", as: "feed_back.user_id" } }, { $unwind: '$feed_back.user_id' },
            {
                "$group": {
                    "_id": {
                        feed_back: "$feed_back",
                        created_at: "$created_at"
                    },
                    "feedBack": { "$push": "$feed_back" },
                }
            },
            { $project: { _id: 1, feed_back: 1 } },
            ], function (error, data) {
                if (error) {
                    res.json({ error: true, message: error });
                } else {
                    res.json({ error: false, message: data })
                }
            })
        })
    });

    app.post('/follow', function (req, res) {                                                     //    TO FOLLOW AND UNFOLLOW ANYONE  (N & A)
        authenticate(req, res, () => {
            console.log("follow");
            let is_follow = req.body.is_follow;
            let his_id = req.body.his_id;
            let self_id = req.body.self_id;
            let time = new Date();
            let following = { user_id: ObjectId(his_id), created_at: time }
            let followers = { user_id: ObjectId(self_id), created_at: time }
            if (is_follow == 1) {
                User.findOneAndUpdate({ _id: ObjectId(self_id) }, { $push: { following: following } }, function (error, data1) {
                    User.findOneAndUpdate({ _id: ObjectId(his_id) }, { $push: { followers: followers } }, function (error, result) {
                        if (error) {
                            res.json({ error: true, message: "Error while add follower or following in the list.", description: error });
                        } else if (!result) {
                            res.json({ error: false, message: "No user" })
                        } else {
                            User.aggregate([{ $match: { _id: ObjectId(self_id) } },
                            { $lookup: { from: 'users', localField: 'following.user_id', foreignField: '_id', as: 'following' } },
                            { $lookup: { from: 'users', localField: 'followers.user_id', foreignField: '_id', as: 'followers' } }
                            ], function (error, data) {
                                if (data) {
                                    res.json({ error: false, message: "Done", description: data })
                                    Funn.notification(ObjectId(self_id), ObjectId(his_id), "Started following you.", "user", ObjectId(self_id));
                                    activity(ObjectId(self_id), "You started following this person from", "Started following", result);
                                }
                            })
                        }
                    })
                })
            } else {
                User.findOneAndUpdate({ _id: ObjectId(self_id) }, { $pull: { following: { user_id: ObjectId(his_id) } } }, function (error1, data1) {
                    if (error1) { res.json({ error: true, message: "Error while fetching selfID in the database." }); }
                    User.findOneAndUpdate({ _id: ObjectId(his_id) }, { $pull: { followers: { user_id: ObjectId(self_id) } } }, function (error2, result) {
                        if (error2) {
                            res.json({ error: true, message: "Error while add follower or following in the list.", description: error2 });
                        } else if (!result) {
                            res.json({ error: false, message: "No user" })
                        } else {
                            User.aggregate([{ $match: { _id: ObjectId(self_id) } },
                            { $lookup: { from: 'users', localField: 'following.user_id', foreignField: '_id', as: 'following' } },
                            { $lookup: { from: 'users', localField: 'followers.user_id', foreignField: '_id', as: 'followers' } }
                            ], function (error3, data) {
                                if (error3) {
                                    res.json({ error: true, message: "Error at the end.", error: error3 });
                                }
                                if (data) {
                                    res.json({ error: false, message: "Done", description: data })
                                    activity(ObjectId(self_id), "You unfollowed this person on", "Unfollowed You", ObjectId(his_id));
                                }
                            })
                        }
                    })
                })
            }
        })
    });

    app.post('/followerList', function (req, res) {                                               //    TO GET THE FOLLOWERS LIST
        authenticate(req, res, () => {
            console.log("followerList");
            let user_id = ObjectId(req.body.user_id);
            User.aggregate([{ $match: { _id: user_id } }, { $lookup: { from: 'users', localField: 'followers.user_id', foreignField: '_id', as: 'followers' } }], function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting the follower list.", description: error });
                } else if (!data) {
                    res.json({ error: false, message: "No match found." });
                } else {
                    res.json({ error: false, message: "Done", description: data[0].followers })
                }
            })
        })
    });

    app.post('/followingList', function (req, res) {                                              //    TO GET THE FOLLOWING LIST
        authenticate(req, res, () => {
            console.log("followingList");
            let user_id = ObjectId(req.body.user_id);
            User.aggregate([{ $match: { _id: user_id } }, { $lookup: { from: 'users', localField: 'following.user_id', foreignField: '_id', as: 'following' } }], function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting the follower list.", description: error });
                } else if (!data) {
                    res.json({ error: false, message: "No match found." });
                } else {
                    res.json({ error: false, message: "Done", description: data[0].following })
                }
            })
        })
    });

    app.post('/forgotPassword', function (req, res) {                                             //    TO CHANGE THE PASSWORD
        authenticate(req, res, () => {
            console.log("forgotPassword");
            type = req.body.type;
            let user_id = req.body.user_id;
            let checkStr = "";
            let email = "";
            let country_code = "";
            let phone = "";
            let admin = req.body.admin;
            let password = req.body.password;


            if (admin == "false") {
                if (type == "email") {
                    email = req.body.email;
                    checkStr = { email: email }
                } else if (type == "phone") {
                    country_code = req.body.country_code;
                    phone = req.body.phone;
                    checkStr = { phone: phone, country_code: '+' + country_code }
                }
                User.find(checkStr, function (err, data) {
                    if (err) {
                        res.json({ error: true, message: "Something went wrong.", data: err })
                    } else if (data.length == 1) {
                        if (type = "email") {
                            phone = data[0].phone;
                            country_code = data[0].country_code;
                        }
                        Funn.otp(country_code, email, phone, req, res);
                    } else {
                        res.json({ error: true, message: "You are not registered with us." })
                    }
                })
            }
        })
    });

    app.post('/getPost', function (req, res) {                                                    //    TO GET ALL POSTS 
        authenticate(req, res, () => {
            console.log("getPost")
            let type = req.body.type;
            let country_id = req.body.country_id;
            let user_id = req.body.user_id;
            if (type == "all") {
                if (user_id == null || user_id == "") {
                    Posts.aggregate([{ $match: { is_deleted: false } },
                    { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
                    { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
                    { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
                    { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } }],
                        function (error, allPosts) {
                            if (error) {
                                res.json({ error: true, message: "Error while getting all post before registration", description: error });
                            } else if (allPosts.length == 0) {
                                res.json({ error: false, message: "No data found" });
                            } else {
                                res.json({ error: false, message: "Done", description: allPosts });
                            }
                        })
                } else {
                    User.findOne({ _id: ObjectId(user_id) }, function (error, userData) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting user details from function.", description: error });
                        } else if (!userData) {
                            res.json({ error: true, message: "Data not found." });
                        } else {
                            let following = [];
                            for (let i = 0; i < userData.following.length; i++) {

                                following.push(ObjectId(userData.following[i].user_id));
                                if (i == userData.following.length - 1) {
                                    Posts.aggregate([{ $match: { _id: { $nin: userData.posts_block_list }, is_sold: false, is_stream: false, is_deleted: false, $or: [{ post_user_id: { $in: following } }, { is_promoted: true }] } },
                                    { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
                                    { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
                                    { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
                                    { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
                                    ], function (err, data) {
                                        if (err) {
                                            res.json({ error: true, message: "Something went wrong.", data: err });
                                        } else if (data.length == 0) {
                                            res.json({ error: false, message: 'No data found.' });
                                        } else {
                                            let filteredPosts = [];
                                            res.json({ error: false, message: "Done", description: data });
                                        }
                                    })
                                }
                            }

                            if (userData.following.length == 0) {
                                Posts.aggregate([{ $match: { is_promoted: true } },
                                { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
                                { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
                                { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
                                { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
                                ], function (err, data) {
                                    if (err) {
                                        res.json({ error: true, message: "Something went wrong.", data: err });
                                    } else if (data.length == 0) {
                                        res.json({ error: false, message: 'No data found.' });
                                    } else {
                                        let filteredPosts = [];
                                        res.json({ error: false, message: "Done", description: data });
                                    }
                                })
                            }
                        }
                    })
                }
            } else if (!country_id) {
                console.log("country id not defined. ", country_id)
            } else {
                User.findOne({ _id: ObjectId(user_id) }, function (error, userData) {
                    if (error) {
                        res.json({ error: true, message: "Error while getting user details from function.", description: error });
                    } else if (!userData) {
                        res.json({ error: false, message: "Data not found" });
                    } else {
                        let following = [];
                        for (let i = 0; i < userData.following.length; i++) {
                            following.push(ObjectId(userData.following[i].user_id));

                            if (i == userData.following.length - 1) {
                                doThis(following)
                            }
                        }
                        if (userData.following.length == 0) {
                            res.json({ error: false, message: "No data found" })
                        }

                        function doThis(following) {
                            Posts.aggregate([{ $match: { "available_country_id.country_id": ObjectId(country_id), is_deleted: false, is_sold: false, _id: { $nin: userData.posts_block_list }, is_stream: false, $or: [{ post_user_id: { $in: following } }, { is_promoted: true }] } },
                            { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
                            { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
                            { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
                            { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
                            ], function (err, data) {
                                if (err) {
                                    res.json({ error: true, message: "Something went wrong.", data: err });
                                } else {
                                    if (data) {
                                        res.json({ error: false, message: "Done", description: data });
                                    }
                                }
                            })
                        }
                    }
                })
            }
        })
    });

    app.post('/getRooms', function (req, res) {                                                   //    TO GET THE ROOMS LIST
        authenticate(req, res, () => {
            console.log("getRooms")
            let room_id = req.body.room_id,
                user_id = req.body.user_id,
                i = 0,
                j = 1;
            if (room_id != "") {
                Room.aggregate([{ $match: { _id: ObjectId(room_id) } },
                { $lookup: { from: 'users', localField: 'user', foreignField: '_id', as: 'usersDetails' } },
                { $project: { _id: 1, usersDetails: 1, created_at: 1 } },
                ], function (error, data) {
                    if (error) {
                        res.json({ error: true, message: error });
                    } else if (!data) {
                        res.json({ error: true, message: "Room not found." })
                    } else {
                        res.json({ error: false, message: "Done", description: data })
                    }
                })
            } else {
                Room.aggregate([{ $match: { user: ObjectId(user_id) } },
                { $lookup: { from: 'users', localField: 'user', foreignField: '_id', as: 'usersDetails' } },
                { $project: { _id: 1, usersDetails: 1, created_at: 1 } },
                ], function (error, data) {
                    if (error) {
                        res.json({ error: true, message: error });
                    } else if (data.length == 0) {
                        res.json({ error: true, message: "Room not found" })
                    } else {
                        res.json({ error: false, message: "Room found", description: data })
                    }
                })
            }
        })
    });

    app.post('/getMyPosts', function (req, res) {                                                 //    TO GET A PARTICULAR POST OF USER
        authenticate(req, res, () => {
            console.log("getMyPost")
            let user_id = req.body.user_id;
            Posts.aggregate([{ $match: { post_user_id: ObjectId(user_id), is_stream: false, is_deleted: false } },
            { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
            { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
            { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
            { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
            ], function (err, data) {
                if (err) {
                    res.json({ error: true, message: "Something went wrong.", data: err });
                } else if (!data) {
                    res.json({ error: false, message: 'No data found, user id might not be matched' })
                } else {
                    res.json({ error: false, message: "Done", description: data });
                }
            })
        })
    });

    app.post('/getMyFavPost', function (req, res) {                                               //    TO GET ALL THE FAVOURITE POSTS 
        authenticate(req, res, () => {
            console.log("getMyFavPost")
            let user_id = req.body.user_id,
                type = req.body.type;
            User.aggregate([{ $match: { _id: ObjectId(user_id) } },
            { $lookup: { from: "allposts", localField: "favPost.post_id", foreignField: "_id", as: "favPost" } }, { $unwind: '$favPost' },
            { $lookup: { from: "tags", localField: "favPost.tags.tag_id", foreignField: "_id", as: "favPost.tags" } },
            { $lookup: { from: "countries", localField: "favPost.available_country_id.country_id", foreignField: "_id", as: "favPost.available_country_id" } },
            { $lookup: { from: "users", localField: "favPost.likes.user_id", foreignField: "_id", as: "favPost.likes" } },
            { $lookup: { from: "users", localField: "favPost.post_user_id", foreignField: "_id", as: "favPost.userDetails" } },
            { $project: { _id: 0, favPost: 1 } }
            ], function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting all favourite posts.", description: error });
                } else if (data.length == 0) {
                    res.json({ error: false, message: "No data found" });
                } else {
                    let filtered = [];
                    if (type == "seller") {
                        for (let i = 0; i < data.length; i++) {
                            if (data[i].favPost.post_user_id.toString() == user_id.toString())
                                filtered.push(data[i]);
                            if (i == data.length - 1) {
                                sendData(filtered);
                            }
                        }
                    } else {
                        sendData(data);
                    }

                    function sendData(data) {
                        res.json({ error: false, message: "Done", description: data });

                    }
                }
            })
        })
    });

    app.post('/getMyStories', function (req, res) {                                               //    TO GET ALL THE STORIES OF A PARTICULAR USER
        authenticate(req, res, () => {
            console.log("getMyStories")
            let user_id = req.body.user_id,
                type = req.body.type,
                user;

            if (type == "seller") {
                doNow("", "")
            } else {
                if (user_id == null || user_id == "") {
                    Story.aggregate([{ $match: { status: "running" } },
                    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } },
                    ], function (errors, dataa) {
                        if (errors) {
                            res.json({ error: true, message: "Error while getting all stories before registration", description: errors });
                        } else if (dataa.length == 0) {
                            res.json({ error: false, message: "No data found." });
                        } else {
                            res.json({ error: false, message: "Done", description: dataa });
                        }
                    })
                } else {
                    Funn.userDetails(user_id, (userData) => {
                        if (userData.error == false) {
                            user = userData.description,
                                following = [], blocked = [], j = 0;
                            for (let i = 0; i < user.following.length; i++) {
                                following.push(ObjectId(user.following[i].user_id));
                                if (i == user.following.length - 1) {
                                    j = user.block_list.length;
                                    for (let i = 0; i < user.block_list.length; i++) {
                                        j--;
                                        blocked.push(ObjectId(user.block_list[i]))
                                    }
                                    if (j == 0)
                                        doNow(following, blocked);
                                }
                            }
                            if (user.following.length == 0) {
                                res.json({ error: false, message: "No data found" })
                            }
                        } else {
                            res.json({ error: true, message: "Something went wrong in getMyStories" })
                        }
                    })
                }
            }

            function doNow(following, blocked) {
                if (type == 'seller') {
                    Story.aggregate([{ $match: { user_id: ObjectId(user_id), status: "running" } },
                    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } },
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting all stories of a particular user.", description: error });
                        } else if (data.length == 0) {
                            res.json({ error: false, message: "No data found." });
                        } else {
                            res.json({ error: false, message: "Done", description: data });
                        }
                    })
                } else {
                    Story.aggregate([{ $match: { _id: { $nin: user.stories_block_list }, status: "running", user_id: { $nin: [ObjectId(user_id), blocked] }, user_id: { $in: following } } },
                    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } },
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting all stories of a particular user", description: error });
                        } else if (data.length == 0) {
                            res.json({ error: false, message: "No data found." });
                        } else {
                            res.json({ error: false, message: "Done", description: data });
                        }
                    })
                }
            }
        })
    });

    app.post('/highestBid', function (req, res) {                                                 //    TO GET THE HIGHEST OF A POST / STREAM / STORY
        authenticate(req, res, () => {
            console.log("highestBid");
            let type = req.body.type,
                post_id = req.body.post_id,
                stream_id = req.body.stream_id,
                story_id = req.body.story_id;
            switch (type) {
                case 'post':
                    Posts.aggregate([{ $match: { _id: ObjectId(post_id) } }, { $unwind: '$bids' },
                    { $lookup: { from: 'users', localField: 'bids.bid_user_id', foreignField: '_id', as: 'bids.bid_user_id' } },
                    {
                        "$group": {
                            "_id": {
                                _id: "$_id",
                                post_id: "$post_id",
                                bid_comment: "$bid_comment",
                                bid_amount: "$bid_amount",
                                bid_created_at: "$bid_created_at",
                                is_pinned: "$is_pinned"
                            },
                            "bids": { "$push": "$bids" },
                        }
                    },
                    { $project: { bids: 1 } }
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, description: error })
                        } else if (data.length == 0) {
                            res.json({ error: true, message: "No data found" });
                        } else {
                            let highestBid = 0,
                                bidDetails = {};
                            for (let i = 0; i < data[0].bids.length; i++) {
                                if (data[0].bids[i].bid_comment == "") {
                                    if (parseFloat(data[0].bids[i].bid_amount) > highestBid) {
                                        highestBid = data[0].bids[i].bid_amount;
                                        bidDetails = data[0].bids[i];
                                    }
                                }
                            }
                            res.json({ error: false, message: "Done", description: bidDetails })
                        }
                    })
                    break;

                case 'stream':
                    Streams.aggregate([{ $match: { _id: ObjectId(stream_id) } }, { $unwind: '$comment' },
                    { $lookup: { from: 'users', localField: 'comment.user_id', foreignField: '_id', as: 'comment.user_id' } },
                    {
                        "$group": {
                            "_id": {
                                _id: "$_id",
                                user_id: "$user_id",
                                stream_name: "$stream_name",
                                status: "$status",
                                viewerCount: "$viewerCount",
                                start_price: "$start_price",
                                created_at: "$created_at",
                                thumbUrl: "$thumbUrl",
                                amount: "$amount",
                                is_marked: "$is_marked"
                            },
                            "comment": { "$push": "$comment" },
                            "country": { "$push": "$country" },
                            "current_product": { "$push": "$current_product" }
                        }
                    },
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, description: error });
                        } else if (data.length == 0) {
                            res.json({ error: true, message: "No match found" });
                        } else {
                            let highestBid = 0,
                                bidDetails = {};
                            for (let i = 0; i < data[0].comment.length; i++) {
                                if (data[0].comment[i].amount) {
                                    if (parseFloat(data[0].comment[i].amount) > parseFloat(highestBid)) {
                                        highestBid = data[0].comment[i].amount;
                                        bidDetails = data[0].comment[i];
                                    }
                                }

                                if (i == data[0].comment.length - 1) {
                                    if (parseFloat(data[0]._id.start_price) < parseFloat(highestBid)) {
                                        res.json({ error: false, message: "Done", description: bidDetails })
                                    } else {
                                        res.json({ error: false, message: "Done", description: { amount: data[0]._id.start_price } })

                                    }
                                }
                            }
                        }
                    })
                    break;

                case 'story':
                    Story.aggregate([{ $match: { _id: ObjectId(story_id) } }, { $unwind: '$bidsAndComments' },
                    { $lookup: { from: 'users', localField: 'bidsAndComments.user_id', foreignField: '_id', as: 'bidsAndComments.user_id' } },
                    {
                        "$group": {
                            "_id": {
                                "created_at": "$created_at",
                                "amount": "$amount"
                            },
                            "bidsAndComments": { "$push": "$bidsAndComments" }
                        }
                    },
                    { $project: { bidsAndComments: 1, _id: 0 } },
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting the highest bid on story.", description: error });
                        } else if (data.length == 0) {
                            res.json({ error: true, message: "No data found while fetching highest bid on story." });
                        } else {
                            let highestBid = 0,
                                bidDetails = {};
                            for (let i = 0; i < data[0].bidsAndComments.length; i++) {
                                if (data[0].bidsAndComments[i].amount) {
                                    if (parseFloat(data[0].bidsAndComments[i].amount) > highestBid) {
                                        highestBid = data[0].bidsAndComments[i].amount;
                                        bidDetails = data[0].bidsAndComments[i];
                                    }
                                }
                            }
                            res.json({ error: false, message: "Done", description: bidDetails })
                        }
                    })
                    break;

                default:
                    res.json({ error: true, message: "You have passed wrong type." });
            }
        })
    });

    app.post('/iban', function (req, res) {                                                       //    TO UPDATE IBAN DETAILS (A)
        authenticate(req, res, () => {
            console.log("iban")
            let user_id = req.body.user_id,
                country = req.body.country,
                banNumber = req.body.banNumber,
                bankName = req.body.bankName,
                phoneNumber = req.body.phoneNumber;
            let obj = { country_code: country, banNumber: banNumber, bankName: bankName, phoneNumber: phoneNumber }
            User.findOneAndUpdate({ _id: user_id }, { IBAN: obj }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while setting data of IBAN.", description: error });
                } else if (!data) {
                    res.json({ error: true, message: "Data not found." });
                } else {
                    User.findOne({ _id: user_id }, function (error, result) {
                        if (error) {
                            res.json({ error: true, message: "Error after setting data of IBAN.", description: error });
                        } else if (!result) {
                            res.json({ error: false, message: "Data updated but not found." });
                        } else {
                            res.json({ error: false, message: "Done", description: result });
                            activity(ObjectId(user_id), "You register your IBAN information on", "iban information", ObjectId(user_id));
                        }
                    })
                }
            })
        })
    });

    app.post('/login', function (req, res) {                                                      //    TO LOGIN
        authenticate(req, res, () => {
            console.log("login: ", req.body)
            type = req.body.type;
            password = req.body.password;
            let checkStr = "";
            let email = "";
            let country_code = "";
            let phone = "";
            let lastLogin = new Date().getTime();

            if (type == "email") {
                email = req.body.email;
                checkStr = { email: email }
            } else if (type == "phone") {
                country_code = req.body.country_code;
                phone = req.body.phone;
                checkStr = { phone: phone, country_code: '+' + country_code }
            }

            User.findOneAndUpdate(checkStr, { last_login: lastLogin, is_admin_block: false }, function (err, data) {
                if (err) {
                    res.json({ error: true, message: "Something went wrong." });
                } else {
                    if (data) {
                        if (password == data.password) {
                            if (data.socket_id != "" && data.is_online == true) {
                                io.to(data.socket_id).emit("logout", { error: true, message: "You have logged in some other device." })
                                User.aggregate([{ $match: { _id: ObjectId(data._id) } },
                                { $lookup: { from: 'countries', localField: 'country_code', foreignField: 'countryCode', as: 'countryDetails' } }
                                ],
                                    function (error, data1) {
                                        if (!error) {
                                            res.json({ error: false, message: "Login success", data: data1 });
                                        }
                                    })
                            } else {
                                User.aggregate([{ $match: { _id: ObjectId(data._id) } },
                                { $lookup: { from: 'countries', localField: 'country_code', foreignField: 'countryCode', as: 'countryDetails' } }
                                ],
                                    function (error, data1) {
                                        if (!error) {
                                            res.json({ error: false, message: "Login success", data: data1 });
                                        }
                                    })
                            }
                        } else {
                            res.json({ error: true, message: "Please check your details and retry." });
                        }
                    } else {
                        res.json({ error: true, message: "You are not registered with us" });
                    }
                }
            })
        })
    });

    app.post('/notification', function (req, res) {                                               //    TO DELETE NOTIFICATIONS OF USER
        authenticate(req, res, () => {
            console.log("notification");
            let type = req.body.type,
                number = req.body.number,
                user_id = req.body.user_id,
                notification_id = req.body.notification_id;
            switch (type) {
                case 'read':
                    Notify.aggregate([{ $match: { receiver_id: ObjectId(user_id) } },
                    { $lookup: { from: 'users', localField: 'sender_id', foreignField: '_id', as: 'sender_id' } }
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while fetching all notification of a user.", description: error })
                        } else if (!data) {
                            res.json({ error: true, message: "Data not found." });
                        } else {
                            res.json({ error: false, message: "Done", description: data })
                        }
                    })
                    break;

                case 'delete':
                    switch (number) {
                        case 'one':
                            Notify.deleteOne({ _id: ObjectId(notification_id) }, function (error, result) {
                                if (error) {
                                    res.json({ error: true, message: "Error while deleting one notification of a user.", description: error });
                                } else if (!result) {
                                    res.json({ error: true, message: "No match found" });
                                } else {
                                    res.json({ error: false, message: "Deleted" });
                                }
                            })
                            break;

                        case 'all':
                            Notify.deleteMany({ receiver_id: ObjectId(user_id) }, function (error, deleted) {
                                if (error) {
                                    res.json({ error: true, message: "Error while deleting the all notification of a user.", description: error });
                                } else if (!deleted) {
                                    res.json({ error: true, message: "Data not found" });
                                } else {
                                    res.json({ error: false, message: "All notification deleted." });
                                }
                            })
                            break;
                    }
                    break;
            }
        })
    });

    app.post('/postLike', function (req, res) {                                                   //    TO LIKE OR UNLIKE ANY POST  (N & A)
        authenticate(req, res, () => {
            console.log("postLike");
            let post_id = req.body.post_id;
            let user_id = req.body.user_id;
            let response = req.body.is_like;
            let time = new Date();
            if (response == 1) {
                Posts.findOne({ _id: post_id }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Something went wrong while adding like details in the post.", description: error });
                    } else if (!data) {
                        res.json({ error: false, message: "No Post Found." });
                    } else {
                        let like = { post_id: ObjectId(post_id), user_id: ObjectId(user_id), create_at: time };
                        Posts.findOneAndUpdate({ _id: ObjectId(post_id) }, { $push: { post_like: like } }, function (error) {
                            if (error) {
                                res.json({ error: true, message: "Error while updating the like details in the post(Database)", description: error });
                            } else {
                                Posts.findOne({ _id: post_id }, function (error, data) {
                                    if (error) {
                                        res.json({ error: true, message: "Error while getting data at the last.", description: error });
                                    } else {
                                        Posts.aggregate([{ $match: { _id: data._id } },
                                        { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
                                        { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
                                        { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
                                        { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
                                        ], function (err, data) {
                                            if (err) {
                                                res.json({ error: true, message: "Something went wrong.", data: err });
                                            } else {
                                                if (data) {
                                                    res.json({ error: false, message: "Done", description: data });
                                                    Funn.notification(ObjectId(user_id), ObjectId(data.post_user_id), "liked your post.", "Post liked", data);
                                                    activity(ObjectId(user_id), "You liked this post", "post", ObjectId(post_id));
                                                }
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            } else {
                Posts.findOne({ _id: post_id }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Something went wrong while adding like details in the post.", description: error });
                    } else if (!data) {
                        res.json({ error: false, message: "No Post Found." });
                    } else {
                        Posts.findOneAndUpdate({ _id: ObjectId(post_id) }, { $pull: { post_like: { user_id: ObjectId(user_id) } } }, function (error, data1) {
                            if (error) {
                                res.json({ error: true, message: "Error while updating the like details in the post(Database)", description: error });
                            } else {
                                Posts.findOne({ _id: post_id }, function (error, data) {
                                    if (error) {
                                        res.json({ error: true, message: "Error while getting data at the last.", description: error });
                                    } else {
                                        Posts.aggregate([{ $match: { _id: data._id } },
                                        { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
                                        { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
                                        { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
                                        { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
                                        ], function (err, data) {
                                            if (err) {
                                                res.json({ error: true, message: "Something went wrong.", data: err });
                                            } else {
                                                if (data) {
                                                    res.json({ error: false, message: "Done", description: data });
                                                }
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    });

    app.post('/raiseMyBid', function (req, res) {                                                 //    TO RAISE YOUR BID ON POST  (N & A)
        authenticate(req, res, () => {
            console.log("RaiseMyBid")
            let id = ObjectId(req.body.post_id);
            Posts.findOneAndUpdate({ _id: ObjectId(id) }, { $pull: { bids: { bid_user_id: ObjectId(req.body.user_id) } } }, function (error, postFound) {
                if (error) {
                    res.json({ error: true, message: "Error while raising the bid", description: error });
                } else if (!postFound) {
                    res.json({ error: false, message: "Post not found" });
                } else {
                    let post_id = id;
                    let user_id = ObjectId(req.body.user_id);
                    let comment = req.body.comment;
                    let bid_amount = req.body.bid_amount;
                    let time = new Date().getTime();
                    let is_pinned = req.body.is_pinned;

                    if (bid_amount != "") {
                        comment = "";
                    }
                    User.aggregate([{ $match: { _id: ObjectId(user_id) } },
                    { $lookup: { from: 'countries', localField: 'country_code', foreignField: 'countryCode', as: 'country' } }
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting the user details to store with bid", description: error })
                        } else if (!data) {
                            res.json({ error: true, message: "No data found." });
                        } else {
                            let bidDetails = [{ post_id: ObjectId(post_id), bid_user_id: user_id, bid_comment: comment, bid_amount: bid_amount, bid_created_at: time, is_pinned: is_pinned, user_name: data[0].username, url: data[0].profilePhoto, country: data[0].country[0] }];

                            Posts.findOneAndUpdate({ _id: id }, { $push: { bids: bidDetails } }, function (error) {
                                if (error) {
                                    res.json({ error: true, message: "Error while adding bid in database." });
                                } else {
                                    Posts.findOne({ _id: id }, function (error, result) {
                                        if (!error) {
                                            res.json({ error: false, message: "Done", description: result.bids });
                                            Funn.notification(ObjectId(user_id), ObjectId(result.post_user_id), "made bid on your post.", "Bid on post", result._id);
                                        } else {
                                            res.json({ error: true, message: "Error at the last in raiseMyBid service.", description: error })
                                        }
                                        activity(ObjectId(user_id), "You made bid on this post", "post", ObjectId(result._id));
                                    })
                                }
                            })
                        }
                    })
                }
            })
        })
    });

    app.post('/register', function (req, res) {                                                   //    TO REGISTER 
        authenticate(req, res, () => {
            console.log("register")
            version = req.body.version;
            country_code = req.body.country_code;
            country_name = req.body.country_name;
            phone = req.body.phone;
            email = req.body.email;
            username = req.body.username;
            password = req.body.password;
            userType = req.body.userType
            profilePicUrl = req.body.profilePic

            User.findOne({ email: email }, function (error, found) {
                if (error) {
                    res.json({ error: true, message: "Error while checking email already exists or not", description: error });
                } else if (found) {
                    res.json({ error: true, message: "Email already exists" });
                } else {
                    Country.findOne({ countryCode: '+' + country_code }, function (error, countryData) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting the coutry details for user", description: error });
                        } else if (!countryData) {
                            res.json({ error: true, message: "Country not found" });
                        } else {
                            user = new User({
                                username: username,
                                email: email,
                                country_code: '+' + country_code,
                                country: countryData,
                                phone: phone,
                                password: password,
                                type: userType,
                                profilePhoto: profilePicUrl,
                                description: ""
                            })

                            user.save(function (err, data) {
                                if (err) {
                                    res.json({ error: true, message: "Something went wrong. Please try again", data: err })
                                } else {
                                    res.json({ error: false, message: "Register Successfully", data: data });
                                }
                            })
                        }
                    })
                }
            })
        })
    });

    app.post('/reportPost', function (req, res) {                                                 //    TO REPORT ANY POST (A)
        authenticate(req, res, () => {
            console.log("reportPost");
            let self_id = req.body.self_id,
                post_id = req.body.post_id,
                reason = req.body.reason,
                message = req.body.message,
                time = new Date().getTime();
            let report = new ReportPost({
                user_id: ObjectId(self_id),
                post_id: ObjectId(post_id),
                reason: reason,
                description: message,
                created_at: time
            })

            report.save(function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while reporting a post.", description: error });
                } else {
                    User.findOneAndUpdate({ _id: ObjectId(self_id) }, { $push: { posts_block_list: ObjectId(post_id) } }, function (error, finalResult) {
                        if (error) {
                            res.json({ error: true, message: "Reported post saved in the database but few steps could not update.", description: error })
                        } else if (!finalResult) {
                            res.json({ error: true, message: "Reported post saved in the database but could not update userProfile." });
                        } else {
                            res.json({ error: false, message: "Post Reported" });
                            activity(ObjectId(self_id), "You reported this post", "post", ObjectId(post_id));
                        }
                    })
                }
            })
        })
    });

    app.post('/reportStory', function (req, res) {                                                //    TO REPORT ANY POST (A)
        authenticate(req, res, () => {
            console.log("reportStory");
            let self_id = req.body.self_id,
                story_id = req.body.story_id,
                reason = req.body.reason,
                message = req.body.message,
                time = new Date().getTime();
            let report = new ReportStory({
                user_id: ObjectId(self_id),
                story_id: ObjectId(story_id),
                reason: reason,
                description: message,
                created_at: time
            })

            report.save(function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while reporting a story.", description: error });
                } else {
                    User.findOneAndUpdate({ _id: ObjectId(self_id) }, { $push: { stories_block_list: ObjectId(story_id) } }, function (error, finalResult) {
                        if (error) {
                            res.json({ error: true, message: "Reported story saved in the database but few steps could not update.", description: error })
                        } else if (!finalResult) {
                            res.json({ error: true, message: "Reported story saved in the database but could not update userProfile." });
                        } else {
                            res.json({ error: false, message: "Story Reported" });
                            activity(ObjectId(self_id), "You reported this story", "story", ObjectId(story_id));
                        }
                    })
                }
            })
        })
    });

    app.post('/reportUser', function (req, res) {                                                 //    TO REPORT USER (A)
        authenticate(req, res, () => {
            console.log("reportUser")
            let self_id = req.body.self_id,
                his_id = req.body.his_id,
                reason = req.body.reason,
                message = req.body.message;
            let time = new Date().getTime();
            let report = new ReportUser({
                user_id: ObjectId(self_id),
                reported_user_id: ObjectId(his_id),
                reason: reason,
                description: message,
                created_at: time
            })

            report.save(function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while saving data of reported post.", description: error });
                } else {
                    User.findOneAndUpdate({ _id: ObjectId(self_id) }, { $push: { block_list: ObjectId(his_id) } }, function (error, result) {
                        if (error) {
                            res.json({ error: true, message: "Error while blocking user.", description: error });
                        } else if (!result) {
                            res.json({ error: true, message: "User not found." });
                        } else {
                            res.json({ error: false, message: "User reported and blocked" });
                            activity(ObjectId(self_id), "You report a user on", "user reported", ObjectId(his_id));
                        }
                    })
                }
            })
        })
    });

    app.post('/searchByPlace', function (req, res) {                                              //    TO SEARCH POSTS BY PLACE NAME
        authenticate(req, res, () => {
            console.log("searchByPlace");
            let placeName = req.body.placeName;
            if (placeName == "") {
                Posts.distinct('place_name', {}, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while getting the posts according to place name.", description: error });
                    } else if (data.length == 0) {
                        res.json({ error: true, message: "No data found." });
                    } else {
                        let allPost = [],
                            count = data.length;
                        for (let i = 0; i < data.length; i++) {
                            Posts.aggregate([{ $match: { place_name: data[i], is_deleted: false } },
                            { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'userDetails' } }
                            ], function (error, result) {
                                count--;
                                if (error) {
                                    res.json({ error: true, message: "Error while getting distinct post by Place Name", description: error });
                                } else if (result.length == 0) {
                                    res.json({ error: true, message: "Data not found when the place name is ".data[i] });
                                } else {
                                    if (data[i] != "")
                                        allPost.push(result)
                                }
                                if (count == 0) {
                                    res.json({ error: false, message: "Done", description: allPost });
                                }
                            })
                        }
                    }
                })
            } else {
                Posts.aggregate([{ $match: { place_name: { $regex: ".*" + placeName + ".*", $options: "i" }, is_deleted: false } },
                { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'userDetails' } }
                ], function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while getting the posts according to place name.", description: error });
                    } else if (data.length == 0) {
                        res.json({ error: true, message: "No data found." });
                    } else {
                        res.json({ error: false, message: "Done", description: data });
                    }
                })
            }
        })
    });

    app.post('/searchCategories', function (req, res) {                                           //    TO SEARCH POSTS BY CATEGORY
        authenticate(req, res, () => {
            console.log("searchCategories");
            let category_id = req.body.category;
            if (category_id == "") {
                Tag.find(function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while getting all tags", description: error });
                    } else if (data.length == 0) {
                        res.json({ error: true, message: "Data not found" });
                    } else {
                        let allPost = [],
                            j = 0;
                        for (let i = 0; i < data.length; i++) {
                            Posts.aggregate([{ $match: { 'tags.tag_id': data[i]._id } },
                            { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'userDetails' } }
                            ], function (error, result) {
                                if (error) {
                                    res.json({ error: true, message: "Error while getting the post according to categories.", description: error });
                                } else if (result.length == 0) {
                                    if (i == data.length - 1 && j == 0)
                                        res.json({ error: true, message: "No data found." });
                                    else if (i == data.length - 1)
                                        res.json({ error: false, message: "Done", description: allPost })
                                } else {
                                    j++;
                                    allPost.push(result)
                                    if (i == data.length - 1) {
                                        res.json({ error: false, message: "Done", description: allPost })
                                    }
                                }
                            })
                        }
                    }
                })
            } else {
                Posts.aggregate([{ $match: { 'tags.tag_id': ObjectId(category_id), is_deleted: false, is_stream: false } },
                { $lookup: { from: 'users', localField: 'post_user_id', foreignField: '_id', as: 'userDetails' } }
                ], function (error, result) {
                    if (error) {
                        res.json({ error: true, message: "Error while getting the post according to categories.", description: error });
                    } else if (result.length == 0) {
                        res.json({ error: true, message: "No data found" });
                    } else {
                        res.json({ error: false, message: "Done", description: result })
                    }
                })
            }
        })
    });

    app.post('/searchUser', function (req, res) {                                                 //    TO SEARCH FOR A USER
        authenticate(req, res, () => {
            console.log("searchUser")
            let user_id = req.body.user_id, text = req.body.text;
            if (user_id == "" || user_id == null) {
                User.find({ username: { $regex: ".*" + text + ".*", $options: "i" } }, function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Something went wrong while getting users list [1]", description: error });
                    } else if (data.length == 0) {
                        res.json({ error: false, message: "No data found" });
                    } else {
                        res.json({ error: false, message: "Done", description: data });
                    }
                })
            } else {
                Funn.userDetails(user_id, (userData) => {
                    if (userData.error == false) {
                        let userDataa = userData.description;
                        User.find({ username: { $regex: ".*" + text + ".*", $options: "i" }, _id: { $nin: userDataa.block_list } }, function (error, data) {
                            if (error) {
                                res.json({ error: true, message: "Error while searching user name.", description: error });;
                            } else if (data.length == 0) {
                                res.json({ error: true, message: "No user found." });
                            } else {
                                res.json({ error: false, message: "Done.", description: data });
                            }
                        })
                    } else {
                        res.json({ error: true, message: "Something went wrong while getting users list" });
                    }
                })
            }
        })
    });

    app.post('/sendMessage', function (req, res) {                                                //    TO SEND MESSAGE (N)
        authenticate(req, res, () => {
            console.log("sendMessage")
            let sender_id = req.body.sender_id,
                room_id = req.body.room_id,
                message = req.body.message,
                messageType = req.body.messageType,
                time = new Date().getTime();
            let msg = new Messages({
                sender_id: ObjectId(sender_id),
                room_id: ObjectId(room_id),
                message: message,
                message_type: messageType,
                created_at: time
            });
            msg.save(function (error, data) {
                if (error) {
                    res.json({ error: true, description: error });
                } else {
                    Room.findOne({ _id: ObjectId(room_id) }, function (error, result) {
                        if (error) {
                            res.json({ error: true, message: "Error while getting room", description: error });
                        } else if (!result) {
                            res.json({ error: true, message: "Room not found." })
                        } else {
                            let id;
                            for (let i = 0; i < result.user.length; i++) {
                                if (result.user[i].toString() != sender_id.toString()) {
                                    id = result.user[i];
                                }
                                if (i == result.user.length - 1) {
                                    User.findOne({ _id: ObjectId(id) }, function (error, userData) {
                                        if (userData) {

                                            io.to(userData.socket_id).emit("messageReceived", {
                                                description: result,
                                                messageDetails: data,
                                                created_at: time.toString()
                                            })
                                            res.json({
                                                error: false,
                                                message: "Done",
                                                description: result,
                                                created_at: time.toString(),
                                                messageDetails: data
                                            });
                                            let obj = { messageDetails: data, roomDetails: result }
                                            Funn.notification(sender_id, id, "sent you message.", "Message Sent", obj)
                                        }
                                    })
                                }
                            }
                        }
                    })
                }
            })
        })
    });

    app.post('/stopMyStream', function (req, res) {                                               //    TO STOP STREAM IN THE BEGINING
        authenticate(req, res, () => {
            console.log("stopMyStream");
            let user_id = req.body.user_id;
            Streams.findOneAndUpdate({ user_id: ObjectId(user_id), status: "running" }, { status: 'stopped' }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Something went wrong while fetching running streams of a user to stop.", description: error });
                } else if (!data) {
                    res.json({ error: false, message: "No match found." });
                } else {

                    User.findOneAndUpdate({ _id: ObjectId(data.user_id) }, { $set: { is_live: "stopped" } }, function (error, result) {
                        if (error) {
                            res.json({ error: true, messages: "Error while updating is live of user", description: error });
                        } else if (!result) {
                            res.json({ error: true, message: "User not found" });
                        } else {
                            Streams.findOne({ _id: data._id }, function (error, stream) {
                                if (error) {
                                    console.log("Something went wrong while getting stream details in stopStream socket.", error);
                                } else if (!stream) {
                                    console.log("Stream updated but after that might be stream not found.");
                                } else {
                                    res.json({ error: false, message: "Streams stopped" })
                                    User.find(function (error, users) {
                                        if (error) { } else if (users.length == 0) { } else {
                                            let streamer
                                            for (let i = 0; i < users.length; i++) {
                                                if (users[i]._id.toString() == data.user_id) {
                                                    streamer = users[i];
                                                }
                                            }
                                            for (let i = 0; i < users.length; i++) {
                                                io.to(users[i].socket_id).emit("updateStream", { error: false, stream: stream, userDetails: streamer });
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        })
    });

    app.post('/stopStoryPost', function (req, res) {                                              //    TO STOP STATUS OF RUNNING STORY
        authenticate(req, res, () => {
            console.log("stopStoryPost")
            let story_id = req.body.story_id,
                user_id = req.body.user_id;
            Story.findOneAndUpdate({ _id: ObjectId(story_id), user_id: ObjectId(user_id), status: "running" }, { status: "stopped" }, function (error, result) {
                if (error) {
                    res.json({ error: true, message: "Error while updating status of running story.", description: error })
                } else if (!result) {
                    res.json({ error: true, message: "No data found" });
                } else {
                    Story.aggregate([{ $match: { _id: ObjectId(story_id) } },
                    { $lookup: { from: "users", localField: "user_id", foreignField: "_id", as: "userDetails" } },
                    ], function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while stopping story post.", description: error });
                        } else if (!data) {
                            res.json({ error: true, message: "No match found." });
                        } else {
                            res.json({ error: false, message: "Done", description: data })
                        }
                    })
                }
            })
        })
    });

    app.post('/storyPost', function (req, res) {                                                  //    TO POST A STORY (N & A)
        authenticate(req, res, () => {
            console.log("storyPost");
            let time = new Date().getTime();
            let nextDay = parseInt(time) + 86400000;
            let story = new Story({
                url: req.body.url,
                created_at: time,
                is_buyNow: req.body.is_buyNow,
                buyNowPrice: req.body.buyNowPrice,
                status: req.body.status,
                user_id: ObjectId(req.body.user_id),
                is_video: req.body.is_video,
                product_serial: req.body.product_serial,
                product_description: req.body.product_description,
                whatsapp_only: req.body.whatsapp_only,
                whatsapp_and_call: req.body.whatsapp_and_call,
            });
            story.save(function (error, data) {
                if (error) {
                    res.json({ error: true, message: "message Error while saving data of story.", description: error });
                } else {
                    Story.aggregate([{ $match: { user_id: ObjectId(req.body.user_id), status: "running" } },
                    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }
                    ], function (error, finalData) {
                        if (error) {
                            res.json({ error: true, message: "message Error when getting data after saving story details in the database", description: error });
                        } else if (!data) {
                            res.json({ error: true, message: "message Data saved but not found." });
                        } else {
                            User.find(function (error, result) {
                                if (error) {
                                    res.json({ error: true, message: "Error while stop stream.", description: error });
                                } else {
                                    for (let i = 0; i < result.length; i++) {
                                        io.to(result[i].socket_id).emit("updateStory", { error: false, message: "Done", description: finalData });
                                    }
                                    var j = schedule.scheduleJob(nextDay, function () {
                                        Story.findOneAndUpdate({ _id: ObjectId(data._id), status: "running" }, { status: "stopped" }, function (error, updateData) {
                                            if (error) {
                                                res.json({ error: true, message: "Error while updating 24 hours of story", description: error });
                                            } else if (!updateData) {
                                                res.json({ error: true, message: "No data found when time to stop story." });
                                            } else {
                                                res.json({ message: "story removed" })
                                            }
                                        })
                                    });
                                    res.json({ error: false, message: "Done" })
                                    Fun.updateCount(req.body.user_id, "story", (data) => { })
                                    activity(ObjectId(req.body.user_id), "You upload a story on", "story upload", ObjectId(data._id));

                                    User.findOne({ _id: ObjectId(req.body.user_id) }, function (error, userDetails) {
                                        if (error) {
                                            console.log({ error: true, message: "Error while getting user details to send notitification.", description: error });
                                        } else if (!userDetails) {
                                            console.log({ error: true, message: "No user find" });
                                        } else {
                                            userDetails.followers.forEach(element => {
                                                Funn.notification(ObjectId(req.body.user_id), element.user_id, 'upload story.', 'Story', data)
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        })
    });

    app.post('/streamDetails', function (req, res) {                                              //    TO GET ALL RUNNING STREAM DATA
        authenticate(req, res, () => {
            console.log("streamDetails")
            Streams.aggregate([{ $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }], function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting the details of live stream.", description: error });
                } else if (!data) {
                    res.json({ error: false, message: "No match found." });
                } else {
                    let details = [];
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].status == "running") {
                            details.push(data[i])
                        }
                    }
                    res.json({ error: false, message: "Done", description: details });
                }
            })
        })
    });

    app.post('/support', function (req, res) {                                                    //    TO STORE ALL PROBLEMS OF USER IN DATABASE
        authenticate(req, res, () => {
            console.log("support");
            let user_id = req.body.user_id,
                description = req.body.description,
                message = req.body.message;
            let support = new Support({
                user_id: ObjectId(user_id),
                description: description,
                message: message
            });

            support.save(function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while saving problem of user.", description: error });
                } else {
                    res.json({ error: false, message: "Done", description: data });
                }
            })
        })
    });

    app.post('/tags', function (req, res) {                                                       //    TO ADD TAGS IN THE DATABASE
        authenticate(req, res, () => {
            console.log("tags");
            let type = req.body.type;
            let name = req.body.name;
            let createdAt = new Date();
            let updatedAt = new Date();
            switch (type) {
                case 'create':

                    let tag = new Tag();
                    tag.name = name;
                    tag.createdAt = createdAt;
                    tag.updatedAt = updatedAt;

                    tag.save(function (error, data) {
                        if (error) {
                            res.json({ error: true, message: 'Error while storing tags into database.', description: error });
                        } else {
                            res.json({ error: false, message: 'Done', description: data })
                        }
                    })

                    break;

                case 'read':
                    Tag.find(function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while fetching tags from database.", description: error });
                        } else {
                            res.json({ error: false, message: "Done", description: data });
                        }
                    })

                    break;

                case 'update':
                    break;

                case 'delete':
                    break;
            }
        })
    });

    app.post('/userData', function (req, res) {                                                   //    TO GETT ALL DETAILS OF A USER
        authenticate(req, res, () => {
            console.log("userData")
            let user_id = ObjectId(req.body.user_id);
            User.aggregate([{ $match: { _id: user_id } },
            { $lookup: { from: 'users', localField: 'followers.user_id', foreignField: '_id', as: 'followers' } },
            { $lookup: { from: 'users', localField: 'following.user_id', foreignField: '_id', as: 'following' } },
            { $lookup: { from: 'countries', localField: 'country_code', foreignField: 'countryCode', as: 'countryDetails' } }
            ], function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting details of a user.", description: error });
                } else if (data.length == 0) {
                    res.json({ error: false, message: "No data found." });
                } else {
                    res.json({ error: false, message: "Done", description: data });
                }
            })
        })
    });

    app.post('/userPost', function (req, res) {                                                   //    TO CREATE A POST (N & A)
        authenticate(req, res, () => {
            console.log("userPost")
            let posts = new Posts();

            let tag = req.body.tag;
            let allTags = tag.split(",");
            let tagToPut = [];
            for (let i = 0; i < allTags.length; i++) {
                let tagObject = { tag_id: ObjectId(allTags[i]) };
                tagToPut.push(tagObject);
            }
            posts.tags = tagToPut;

            let country = req.body.country.split(",");
            countryIds = [];
            for (let i = 0; i < country.length; i++) {
                countryIds.push({ country_id: ObjectId(country[i]) })
            }
            posts.available_country_id = countryIds;


            let url = req.body.image_url.split(",");
            let allImageUrl = [];
            for (let i = 0; i < url.length; i++) {
                allImageUrl.push({ image_url: url[i], is_video: req.body.is_video });
            }

            let date = new Date()
            posts.description = req.body.description;
            posts.latitude = req.body.latitude;
            posts.longitude = req.body.longitude;
            posts.place_name = req.body.place_name;
            posts.post_user_id = ObjectId(req.body.post_user_id);
            posts.is_instagram = req.body.is_instagram;
            posts.is_twitter = req.body.is_twitter;
            posts.amount = req.body.amount;
            posts.is_auction = req.body.is_auction;
            posts.buy_now = req.body.buy_now;
            posts.whatsapp_only = req.body.whatsapp_only;
            posts.whatsapp_and_call = req.body.whatsapp_and_call;
            posts.product_serial = req.body.product_serial;
            posts.payment_type = req.body.payment_type;
            posts.language = req.body.language;
            posts.only_user_from_selected_country = req.body.only_user_from_selected_country;
            posts.post_created_at = date.getTime();
            posts.status = req.body.status;
            posts.is_sold = req.body.is_sold;
            posts.is_story = req.body.is_story;
            posts.post_images = allImageUrl;

            posts.save(function (error, result) {
                if (error) {
                    res.json({ error: true, message: "Something went wrong while adding data into database.", description: error });
                } else {
                    Posts.aggregate([{ $match: { _id: result._id } },
                    { $lookup: { from: "tags", localField: "tags.tag_id", foreignField: "_id", as: "tags" } },
                    { $lookup: { from: "countries", localField: "available_country_id.country_id", foreignField: "_id", as: "available_country_id" } },
                    { $lookup: { from: "users", localField: "post_like.user_id", foreignField: "_id", as: "post_like" } },
                    { $lookup: { from: "users", localField: "post_user_id", foreignField: "_id", as: "userDetails" } },
                    ], function (err, data) {
                        if (err) {
                            res.json({ error: true, message: "Something went wrong.", data: err });
                        } else {
                            if (data) {
                                res.json({ error: false, message: "Done", description: data });
                                for (let i = 0; i < data[0].userDetails[0].followers.length; i++) {
                                    Funn.notification(req.body.post_user_id, data[0].userDetails[0].followers[i].user_id, "upload a post", "New post", result);
                                }
                                Fun.updateCount(req.body.post_user_id, "post", (data) => { })
                                activity(ObjectId(req.body.post_user_id), "You created this post", "post", ObjectId(result._id));
                            }
                        }
                    })

                }
            })
        })
    });

    app.post('/updateProduct', function (req, res) {                                              //    TO UPDATE PRODUCT OF LIVESTREAM
        console.log("updateProduct");
        let stream_id = req.body.stream_id,
            startPrice = req.body.start_price;
        let obj = {
            stream_id: ObjectId(stream_id),
            image_url: req.body.image_url,
            product_serial: req.body.product_serial,
            price: req.body.start_price,
            payment_type: req.body.payment_type,
            is_sold: req.body.is_sold,
            description: req.body.description
        }
        Streams.findOneAndUpdate({ _id: ObjectId(stream_id) }, { comment: [], start_price: startPrice, current_product: obj }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while updating start price of stream", description: error });
            } else if (!data) {
                res.json({ error: true, message: "No stream found." });
            } else {
                Streams.aggregate([{ $match: { _id: ObjectId(stream_id) } },
                { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }
                ], function (error, result) {
                    if (!error) {
                        User.find(function (error, data) {
                            if (error) {
                                console.log("Error while fetching stream.")
                            } else {
                                for (let i = 0; i < data.length; i++) {
                                    io.to(data[i].socket_id).emit("updateProduct", { error: false, message: "Done", stream: result[0], comments: result[0].comment, userDetails: result[0].userDetails });

                                    if (i == data.length - 1) {
                                        res.json({ error: false, message: "Done" })
                                    }
                                }
                            }
                        })
                    } else {
                        console.log("Error int the last of addComment");
                    }
                })
            }
        })
    });

    app.post('/updateProfile', function (req, res) {                                              //    TO UPDATE PROFILE OF A USER (A)
        authenticate(req, res, () => {
            console.log("updateProfile");
            let user_id = req.body.user_id;
            let image_url = req.body.image_url;
            let description = req.body.description;
            let user_name = req.body.user_name;
            let password = req.body.password;
            User.findOneAndUpdate({ _id: ObjectId(user_id) }, { $set: { profilePhoto: image_url, description: description, username: user_name, password: password } },
                function (error, data) {
                    if (error) {
                        res.json({ error: true, message: "Error while updating user details.", description: error });
                    } else if (!data) {
                        res.json({ error: true, message: "Data not found." });
                    } else {
                        User.findOne({ _id: user_id }, function (error, result) {
                            if (error) {
                                res.json({ error: true, message: "Error in the last while getting user details after updating.", description: error });
                            } else {
                                res.json({ error: false, message: "Done", description: result });
                                activity(ObjectId(user_id), "You update your profile on", "update profile", ObjectId(user_id));
                            }
                        })
                    }
                })
        })
    });

    app.post('/updateStorySettings', function (req, res) {                                        //    TO UPDATESTORYSETTING
        authenticate(req, res, () => {
            console.log('updateStorySettings params ---> ', req.body)
            let user_id = req.body.user_id;
            User.findOneAndUpdate({ _id: ObjectId(user_id) }, { $set: req.body }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while updating settings in user details.", description: error });
                } else if (!data) {
                    res.json({ error: false, message: "User not found." });
                } else {
                    User.findOne({ _id: user_id }, function (error, result) {
                        if (error) {
                            res.json({ error: true, message: "Something went wrong after updating the details in updateStorySettings", description: error });
                        } else if (!result) {
                            res.json({ error: true, message: "Data not found." });
                        } else {
                            res.json({ error: false, message: "Done", description: result });
                            activity(ObjectId(user_id), "You made updates in story setting on", "update story setting", ObjectId(user_id));
                        }
                    })
                }
            })
        })
    });

    app.post('/updateThumb', function (req, res) {                                                //    TO UPDATE THUMB URL OF LIVE STREAM
        authenticate(req, res, () => {
            console.log("updateThumb");
            let image_url = req.body.image_url;
            let stream_id = req.body.stream_id;
            Streams.findOneAndUpdate({ _id: stream_id }, { thumbUrl: image_url }, function (error, dataStream) {
                if (error) {
                    res.json({ error: true, message: "Error while updating thumb url.", description: error });
                } else if (!dataStream) {
                    res.json({ error: false, message: "Stream not found" });
                } else {
                    Streams.findOne({ _id: stream_id }, function (error, stream) {
                        if (error) {
                            res.json({ error: true, message: "Error after updating thumb url.", error: error });
                        } else if (!stream) {
                            res.json({ error: false, message: "Thumb url updated but stream might not be found." });
                        } else {
                            User.find(function (error, data) {
                                if (error) {
                                    res.json({ error: false, message: "Something went wrong.", description: error });
                                } else {

                                    let streamer;
                                    for (let i = 0; i < data.length; i++) {
                                        if (dataStream.user_id.toString() == data[i]._id.toString()) {
                                            streamer = data[i];
                                        }
                                    }
                                    for (let i = 0; i < data.length; i++) {
                                        io.to(data[i].socket_id).emit("updateStream", { error: false, thumbUrl: image_url, stream: dataStream, userDetails: streamer, message: "Done" });
                                    }
                                    res.json({ error: false, message: "done", stream: stream, userDetails: streamer })
                                }
                            })
                        }
                    })
                }
            })
        })
    });

    app.post('/userList', function (req, res) {                                                   //    TO SHOW ALL THE USER
        authenticate(req, res, () => {
            console.log("userList");
            User.find(function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while getting all users detail", description: error });
                } else if (data.length == 0) {
                    res.json({ error: true, message: "No data found" });
                } else {
                    res.json({ error: false, message: "Done", description: data });
                }
            })
        })
    });

    app.post('/userType', function (req, res) {                                                   //    TO CHANGE THE THE USER TYPE
        authenticate(req, res, () => {
            console.log("userType")
            let id = req.body.id;
            let userType = req.body.type;

            User.findOne({ _id: id }, function (error, user) {
                if (error) {
                    res.json({ error: true, message: error, line: "thisOne" });
                } else if (!user) {
                    res.json({ error: false, message: "User not found" });
                } else {
                    if (userType) {
                        User.findOneAndUpdate({ _id: id }, { type: userType }, function (error, data) {
                            if (error) {
                                res.json({ error: true, message: "Error while setting user type in database" });
                            } else {
                                User.findOne({ _id: id }, function (error, data) {
                                    res.json({ error: false, data: data })
                                })
                            }
                        })
                    } else {
                        res.json({ error: true, message: "User Type not given." });
                    }
                }
            })
        })
    });

    app.post('/verifyCode', function (req, res) {                                                 //    TO VERIFY THE CODE
        authenticate(req, res, () => {
            console.log("verifyCode")
            let type = req.body.type;
            let code = req.body.code;
            let checkStr = "";
            let email = "";
            let country_code = "";
            let phone = "";

            if (type == "email") {
                email = req.body.email;
                checkStr = { email: email, otp: code }
            } else if (type == "phone") {
                country_code = req.body.country_code;
                phone = req.body.phone;
                checkStr = { phone: phone, country_code: country_code, otp: code }
            }

            Otp.find(checkStr, function (err, data) {
                if (err) {
                    res.json({ error: true, message: "Something went wrong", data: err });
                } else {
                    if (data.length == 1) {
                        if (code == data[0].otp) {
                            res.json({ error: false, message: "Otp Verified" });
                            deleteOtp(data[0]._id)
                        } else {
                            res.json({ error: true, message: "Wrong otp, Please check and enter again" });
                        }
                    } else {
                        res.json({ error: true, message: "Error occurred, please try again" })
                    }
                }
            })

            function deleteOtp(id) {
                Otp.deleteOne({ _id: ObjectId(id) }, function (error, done) {
                    if (error) {
                        console.log({ error: true, message: "Error while deleting otp.", description: error });
                    } else if (!done) {
                        console.log({ error: true, message: "Otp not found" });
                    } else {
                        console.log({ error: false, message: "Otp deleted" })
                    }
                })
            }

        })
    });

    app.post('/verifyPhone', function (req, res) {                                                //    TO CHECK USER ALREADY REGISTERED OR NOT
        authenticate(req, res, () => {
            console.log("verifyPhone")
            country_code = req.body.country_code;
            phone = req.body.phone;
            email = req.body.email;

            User.findOne({ country_code: country_code, phone: phone }, function (error, result) {
                if (error) {
                    res.json({ error: true, message: "Error while checking is user registered or not.", description: error });
                } else if (result) {
                    res.json({ error: true, message: "You are already registered." });
                } else {
                    Otp.deleteOne({ phone: phone }, function (error, found) {
                        if (error) {
                            res.json({ error: true, message: "Error while deleting existing otp from database.", description: error });
                        } else if (!found) {
                            Funn.otp(country_code, email, phone, req, res);
                        } else {
                            Funn.otp(country_code, email, phone, req, res);
                        }
                    })
                }
            })
        })
    });

    app.post('/vote', function (req, res) {                                                       //    TO VOTE FOR ANY POST (N & A)
        authenticate(req, res, () => {
            console.log("VoteAPI")
            let user_id = req.body.user_id,
                post_id = req.body.post_id,
                is_vote = req.body.is_vote,
                obj = { user_id: ObjectId(user_id), response: is_vote },
                is_deleted = "false";

            Posts.findOne({ _id: ObjectId(post_id) }, function (error, result) {
                if (error) {
                    res.json({ error: true, message: "Error while getting favourite post details.", description: error })
                } else if (!result) {
                    res.json({ error: false, message: "No data found." });
                } else {
                    let exists, query;
                    result.vote.forEach(element => {
                        if (element.user_id == user_id)
                            exists = element;
                    });

                    if (exists) {
                        if (exists.response == is_vote) {
                            Posts.findOneAndUpdate({ _id: ObjectId(post_id) }, { $pull: { vote: { user_id: ObjectId(user_id) } } }, function (error, afterPull) {
                                if (error) {
                                    res.json({ error: false, message: "Error while updating new records.", description: error })
                                } else if (!afterPull) {
                                    res.json({ error: false, message: "No match found." });
                                } else {
                                    getLatestData(is_vote);
                                    activity(ObjectId(user_id), "You removed your vote from this post", "post", ObjectId(post_id));
                                }
                            })
                        } else {
                            Posts.findOneAndUpdate({ _id: ObjectId(post_id), "vote.user_id": ObjectId(user_id) }, { $set: { "vote.$.response": is_vote } }, function (error, data) {
                                if (error) {
                                    res.json({ error: true, message: "Error while updating existing data of post.", description: error });
                                } else if (!data) {
                                    res.json({ error: false, message: "No data found." });
                                } else {
                                    getLatestData(is_vote);
                                    activity(ObjectId(user_id), "You changed your vote for this post", "post", ObjectId(post_id));
                                    Funn.notification(user_id, data.post_user_id, "vote on your post", "New Vote", data)
                                }
                            })
                        }
                    } else {
                        Posts.findOneAndUpdate({ _id: ObjectId(post_id) }, { $push: { vote: obj } }, function (error, newData) {
                            if (error) {
                                res.json({ error: false, message: "Error while updating new records.", description: error });
                            } else if (!newData) {
                                res.json({ error: false, message: "No match found." });
                            } else {
                                getLatestData(is_vote);
                                activity(ObjectId(user_id), "You voted for this post", "post", ObjectId(post_id));
                                Funn.notification(user_id, newData.post_user_id, "vote on your post", "New Vote", newData)
                            }
                        })
                    }

                    function getLatestData(response) {
                        Posts.findOne({ _id: ObjectId(post_id) }, function (error, finalData) {
                            if (error) {
                                res.json({ error: false, message: "Error while updating new records.", description: error });
                            } else if (!finalData) {
                                res.json({ error: false, message: "No match found." });
                            } else {
                                let totalVotes = finalData.vote.length,
                                    voteUp = 0,
                                    voteDown = 0,
                                    result;
                                if (totalVotes != "0") {
                                    for (let i = 0; i < totalVotes; i++) {
                                        if (finalData.vote[i].response == "1") {
                                            voteUp++;
                                        } else {
                                            voteDown++;
                                        }
                                        if (i == totalVotes - 1) {
                                            result = voteUp / totalVotes * 100;
                                            res.json({ error: false, message: "Done", description: finalData, percent: result + '%' });
                                            updatePost(result);
                                            User.find(function (error, users) {
                                                if (error) {
                                                    res.json({ error: true, message: "Error", description: error });
                                                } else if (!users) {
                                                    res.json({ error: false, message: "No data found." });
                                                } else {
                                                    for (let i = 0; i < users.length; i++) {
                                                        io.to(users[i].socket_id).emit("updateVote", { error: false, message: "Vote updated", post_id: ObjectId(post_id), voteStatus: response, percent: result + '%' });
                                                    }
                                                }
                                            })
                                        }
                                    }
                                } else {
                                    res.json({ error: false, message: "Done", description: finalData, percent: 0 + '%' });
                                    User.find(function (error, users) {
                                        if (error) {
                                            res.json({ error: true, message: "Error", description: error });
                                        } else if (!users) {
                                            res.json({ error: false, message: "No data found." });
                                        } else {
                                            for (let i = 0; i < users.length; i++) {
                                                io.to(users[i].socket_id).emit("updateVote", { error: false, message: "Vote updated", post_id: ObjectId(post_id), voteStatus: response, percent: 0 + '%' });
                                                updatePost(0);
                                            }
                                        }
                                    })
                                }
                            }
                        })
                    }

                    function updatePost(percent) {
                        Posts.findOneAndUpdate({ _id: ObjectId(post_id) }, { percent: percent + '%' }, function (error, done) {
                            if (error) {
                                console.log({ error: true, message: "Percentage calculated but not saved in database.", description: err })
                            } else if (!done) {
                                console.log({ error: true, message: "Data not found at the time of percent" })
                            } else {
                                console.log("Percentage setted")
                            }
                        })
                    }
                }
            })
        })
    });

    app.post("/test", function (req, res) {
        console.log("test");
        let product_id = req.body.product_id, user_id = req.body.user_id, appleReceipt = req.body.appleReceipt, transaction_identifier = req.body.transaction_identifier, user_type = req.body.user_type, androidObject = [];

        console.log("payload length: ", appleReceipt.length)

        if (user_type == "ios") {
            appleReceiptVerify.validate({
                receipt: appleReceipt
            }, function (err, products) {
                if (err) {
                    res.json({ error: true, update: true, message: "apple receipt is invalid" })
                    return console.error(err);
                } else {
                    console.log("><><><><><><><><> ", products);
                    Funn.userDetails(user_id, (yes) => {

                        let user = yes.description;

                        var productArrFiltered = products.filter(product => product.productId == product_id);

                        if (productArrFiltered.length == 0) {
                            console.log("productArrFiltered.length == 0")
                            res.json({ error: true, update: true })
                            return;
                        }

                        console.log("productArrFiltered=>>> ", productArrFiltered)

                        var productArrSorted = productArrFiltered.sort((product1, product2) => {
                            return (product1.expirationDate > product2.expirationDate) ? -1 : 1;
                        })
                        console.log("productArrSorted=>>> ", productArrSorted)

                        //Get first Product
                        var firstProduct = productArrSorted[0];
                        if (firstProduct == "undefined" || firstProduct == null || firstProduct == "") {
                            console.log("firstProduct is undefined")
                            res.json({ error: true, update: true })
                            return;
                        }
                        console.log("firstProduct=>>> ", firstProduct)

                        let arr = [];
                        arr.push(firstProduct);


                        InApp.findOne({ user_id: ObjectId(user_id) }, function (error, data) {

                            if (error) {
                                console.log("in error part line 3505 when data not found in inapp purchase");
                                changeInAppModelAndCount(arr, appleReceipt, user_id, product_id, firstProduct, user, res)
                            }
                            else if (data) {

                                var localIosResponse = data.ios_response

                                var localFiltered = localIosResponse.filter(localProduct => localProduct.productId == product_id);

                                if (localFiltered.length == 0) {
                                    console.log("localFiltered.length == 0")
                                    // call function here

                                    changeInAppModelAndCount(arr, appleReceipt, user_id, product_id, firstProduct, user, res)

                                } else {
                                    console.log("localFiltered=>>> ", localFiltered)

                                    var localSorted = localFiltered.sort((product1, product2) => {
                                        return (product1.expirationDate > product2.expirationDate) ? -1 : 1;
                                    })

                                    var localfirstProduct = localSorted[0];

                                    if (localfirstProduct == "undefined" || localfirstProduct == null || localfirstProduct == "") {
                                        console.log("localfirstProduct is undefined")
                                        //call function here 

                                        changeInAppModelAndCount(arr, appleReceipt, user_id, product_id, firstProduct, user, res)

                                    } else {
                                        console.log("localfirstProduct=>>> ", localfirstProduct)

                                        // Apple product expiration date is grateer than local product expiration date
                                        console.log("localfirstProduct.expirationDate => ", localfirstProduct.expirationDate)
                                        console.log("firstProduct.expirationDate =>> ", firstProduct.expirationDate)
                                        if (localfirstProduct.expirationDate <= firstProduct.expirationDate) {
                                            // call function here
                                            console.log("welcomeeeeeeee")

                                            changeInAppModelAndCount(arr, appleReceipt, user_id, product_id, firstProduct, user, res)
                                        }
                                    }
                                }
                            }
                            else {
                                console.log("when data not found in inapp purchase");
                                changeInAppModelAndCount(arr, appleReceipt, user_id, product_id, firstProduct, user, res)
                            }
                        })
                    })
                }
            });
        } else {
            androidObject.push(req.body.androidObject);
            InApp.findOneAndUpdate({ user_id: ObjectId(user_id) }, { $push: { android_response: androidObject } }, function (error, data) {
                if (error) {
                    res.json({ error: true, message: "Error while updating the android data of in app purchase", description: error });
                } else if (!data) {
                    let inApp = new InApp({
                        user_id: ObjectId(user_id),
                        android_response: androidObject
                    })

                    inApp.save(function (error, data) {
                        if (error) {
                            res.json({ error: true, message: "Error while inserting the android in app values", description: error })
                        } else {
                            res.json({ error: false, message: "Data saved", description: data });
                        }
                    })
                } else {
                    res.json({ error: false, message: "Data saved", description: data });
                }
            })
        }

        function letsDoThis(type, product, userExpiryDate, userCount) {
            let expiryDate = parseInt(userExpiryDate) + 2592000000;
            if (type == "post") {
                let unObj = { status: true, activation: product.purchaseDate, expiry: product.expirationDate, count: userCount }
                let obj = { unlimited_post_count: unObj }
                Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                    if (yes.error == false) {
                        res.json({ error: false, update: true })
                    }
                })
            } else if (type == "story") {
                let unObj = { status: true, activation: product.purchaseDate, expiry: product.expirationDate, count: userCount }
                let obj = { unlimited_story_count: unObj }
                Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                    if (yes.error == false) {
                        res.json({ error: false, update: true })
                    }
                })
            } else {
                let unObj = { status: true, activation: product.purchaseDate, expiry: product.expirationDate, count: userCount }
                let obj = { unlimited_live_count: unObj }
                Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                    if (yes.error == false) {
                        res.json({ error: false, update: true })
                    }
                })
            }

            // InApp.findOneAndUpdate({ user_id: ObjectId(user_id) }, { $set: { apple_receipt: appleReceipt }, $push: { ios_response: product } }, function (error, data) {
            //     if (error) {
            //         console.log({ error: true, message: error });
            //     } else if (!data) {
            //         console.log({ error: true, message: "User not found" });
            //     } else {

            //     }
            // })
        }

        function changeInAppModelAndCount(arr, appleReceipt, user_id, product_id, firstProduct, user, res) {

            InApp.findOneAndUpdate({ user_id: ObjectId(user_id) }, { $set: { apple_receipt: appleReceipt }, $push: { ios_response: arr } }, function (error, done) {
                if (error) { } else if (!done) {
                    let inApp = new InApp({
                        user_id: ObjectId(user_id),
                        apple_receipt: appleReceipt,
                        user_type: "ios",
                        ios_response: arr
                    });
                    inApp.save(function () { })
                } else { }
            })

            console.log("PRODUCT ID is =>  ", product_id);
            // console.log("firstProduct.purchaseDate is =>  ", firstProduct.purchaseDate)
            console.log("user.unlimited_post_count.activation is =>  ", user.unlimited_post_count.activation)

            switch (product_id) {

                case "AutoRenewableUnlimitedPostsForOneMonth1":
                    console.log("Case 1 called")
                    firstTime("post", firstProduct, user.unlimited_post_count.count, res);
                    break;

                case "AutoRenewableUnlimitedStoriesForOneMonth1":
                    console.log("case 2 called")
                    firstTime("story", firstProduct, user.unlimited_story_count.count, res);
                    break;

                case "AutoRenewableUnlimitedLiveStreamsForOneMonth1":
                    console.log("case 3 called")
                    firstTime("live", firstProduct, user.unlimited_live_count.count, res);
                    break;

                default:
                    console.log(product_id);
            }
        }

        function firstTime(type, firstProduct, count, res) {
            //let newDate = parseInt(firstProduct.purchaseDate) + 2592000000;
            // var newObject 
            console.log("First time type => ", type)
            console.log("First time first product  => ", firstProduct)

            if (type == "post") {
                let newObject = { status: true, activation: firstProduct.purchaseDate, expiry: firstProduct.expirationDate, count: count };
                let anotherObj = { unlimited_post_count: newObject }
                Fun.updateUnlimitedCount(user_id, anotherObj, (yes) => {
                    if (yes.error == false) {
                        res.json({ error: false, update: true })
                    }
                })
            } else if (type == "story") {
                let newObject = { status: true, activation: firstProduct.purchaseDate, expiry: firstProduct.expirationDate, count: count };
                let anotherObj = { unlimited_story_count: newObject }
                Fun.updateUnlimitedCount(user_id, anotherObj, (yes) => {
                    if (yes.error == false) {
                        res.json({ error: false, update: true })
                    }
                })
            } else {
                let newObject = { status: true, activation: firstProduct.purchaseDate, expiry: firstProduct.expirationDate, count: count };
                let anotherObj = { unlimited_live_count: newObject }
                Fun.updateUnlimitedCount(user_id, anotherObj, (yes) => {
                    if (yes.error == false) {
                        res.json({ error: false, update: true })
                    }
                })
            }
        }
    });

    app.post("/checkReceipt", function (req, res) {
        console.log("checkReceipt");
        let user_id = req.body.user_id, appleReceipt = req.body.appleReceipt;
        InApp.findOne({ user_id: ObjectId(user_id) }, function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error while comparing apple receipt", description: error });
            } else if (!data) {
                res.json({ error: true, message: "No apple receipt and user found" });
            } else {
                if (data.apple_receipt != "") {
                    if (data.apple_receipt != null) {
                        if (data.apple_receipt == appleReceipt) {
                            res.json({ error: false, messages: false });
                        } else {
                            res.json({ error: false, message: true });
                        }
                    }
                }
            }
        })
    });

    app.post("/getPurchaseData", function (req, res) {
        console.log("getPurchaseData");
        let user_id = req.body.user_id, productId = req.body.productId;

        InApp.aggregate([{ $unwind: '$android_response' }, { $match: { user_id: ObjectId(user_id), 'android_response.productId': productId } }], function (error, data) {
            if (error) {
                res.json({ error: true, message: "Error", description: error })
            } else if (data.length == 0) {
                res.json({ error: true, message: "User not found" });
            } else {
                res.json({ error: false, message: "Done", description: data })
            }
        })
    });

    app.post("/restorePurchase", function (req, res) {

        console.log("restorePurchase Called");

        let user_id = req.body.user_id, product_id = req.body.product_id;

        console.log("productId in restore purchase is ", product_id)

        InApp.findOne({ user_id: ObjectId(user_id) }, function (error, data) {
            if (error) {

            } else if (!data) {
                res.json({ error: true, message: "Something went wrong user not found" })
            } else {

                // Retrive AppleResponse(ios_response) from user database based on userId and prodcut id
                // Ascending based on purchase date 
                // Then check that the product is applied on user or not, if yes then update database

                let iosResponseArr = data.ios_response
                console.log("iosresponse in restore purchase => ", iosResponseArr)

                var filteredRsponse = iosResponseArr.filter(product => product.productId == product_id);
                console.log("filteredRsponse in restore purchase => ", filteredRsponse)

                if (filteredRsponse.length == 0) {
                    console.log("filtered response length is 0 in restore purchase.")
                    res.json({ error: false, update: false })
                    return;
                }

                var sortedResponse = filteredRsponse.sort((product1, product2) => {
                    return (product1.expirationDate > product2.expirationDate) ? -1 : 1;
                })

                console.log("sortedResponse in restore product =>>  ", sortedResponse)

                var firstProduct = sortedResponse[0]

                console.log("FirstProduct in restore product >>>>>>>>> ", firstProduct)
                if (!firstProduct) {
                    res.json({ error: false, update: false })
                    return;
                }

                appleReceiptVerify.validate({
                    receipt: data.apple_receipt
                }, function (err, products) {
                    if (err) {

                        console.error(err);
                        res.json({ error: false, update: false })
                        return

                    } else {
                        console.log("Restore Products apple response in restore product =>>>>> ", products);

                        console.log("firstProduct.originalTransactionId in restore product >>>  ", firstProduct.originalTransactionId)

                        let filteredAppleProduct = products.filter(p => p.originalTransactionId == firstProduct.originalTransactionId)

                        console.log("filteredAppleProdut in restore product =>>> ", filteredAppleProduct)

                        if (filteredAppleProduct.length == 0) {
                            console.log("filtered apple product length is 0 in restore product")
                            res.json({ error: false, update: false })
                            return;
                        }

                        let sortedAppleProduct = filteredAppleProduct.sort((p1, p2) => {
                            return (p1.expirationDate > p2.expirationDate) ? -1 : 1;
                        })

                        console.log("sortedAppleProduct in restore product =>>> ", sortedAppleProduct)

                        let mainProduct = sortedAppleProduct[0]

                        console.log("mainProduct in restore product =>>> ", mainProduct)

                        console.log("mainProduct expirationDate in restore purchased =>>> ", mainProduct.expirationDate)
                        console.log("firstProduct expirationDate in restore purchased =>>> ", firstProduct.expirationDate)

                        if (mainProduct) {
                            if (mainProduct.expirationDate == firstProduct.expirationDate) {
                                console.log("restore purchased returned TRUE")
                                res.json({ error: false, update: true })
                                return;
                            }
                            if (mainProduct.expirationDate > Date.now()) {

                                console.log("restoration started")

                                Funn.userDetails(user_id, (yes) => {
                                    if (yes.error == false) {

                                        let user = yes.description

                                        console.log("user found in restore purchase")

                                        switch (product_id) {

                                            case "AutoRenewableUnlimitedPostsForOneMonth1":

                                                console.log("FIRST CASE OF RESTORE PURCHASE")

                                                // if (user.unlimited_post_count.activation != "") {
                                                //     console.log("1")
                                                // if (firstProduct.purchaseDate > user.unlimited_post_count.activation) {
                                                //     console.log("2")
                                                // let date1 = parseInt(user.unlimited_post_count.expiry) + 2592000000;
                                                // if (firstProduct.purchaseDate > date1) {
                                                // console.log("3")
                                                let newObject1 = { status: true, activation: mainProduct.purchaseDate, expiry: mainProduct.expirationDate, count: user.unlimited_post_count.count };
                                                let anotherObj1 = { unlimited_post_count: newObject1 }
                                                Funn.updateUnlimitedCount(user_id, anotherObj1, (yes) => {
                                                    if (yes.error == false) {
                                                        console.log("successfully updated count in restore purchase post")
                                                        res.json({ error: false, update: true })
                                                    }
                                                })


                                                InApp.update({ _id: ObjectId(data._id), "ios_response.originalTransactionId": mainProduct.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": mainProduct.originalTransactionId } } }, function (error, updateData) {
                                                    if (error) {
                                                        console.log("error while pulll in restore purchase post", error.message);
                                                    }
                                                    else {
                                                        console.log("successfully pull in restore purchase post");
                                                        InApp.findOneAndUpdate({ _id: ObjectId(data._id) }, { $push: { ios_response: mainProduct } }, function (err, done) {
                                                            if (err) {
                                                                console.log("error while pushing in restore purchase post ", err.message);
                                                            }
                                                            else {
                                                                console.log("push coplete, Everything is done  in resotore purchase post");
                                                            }
                                                        })
                                                    }
                                                })



                                                // } else {
                                                //     console.log("4")
                                                //     let newObject11 = { status: true, activation: firstProduct.purchaseDate, expiry: date1, count: user.unlimited_post_count.count };
                                                //     let anotherObj11 = { unlimited_post_count: newObject11 }
                                                //     Funn.updateUnlimitedCount(user_id, anotherObj11, (yes) => {
                                                //         if (yes.error == false) {
                                                //             console.log("6")
                                                //             res.json({ error: false, update: true })
                                                //         }
                                                //     })
                                                // }
                                                // } else {
                                                //     res.json({ error: false, update: false })
                                                // }
                                                // } else {
                                                //     console.log("5")
                                                //     let newObject11 = { status: true, activation: firstProduct.purchaseDate, expiry: parseInt(firstProduct.purchaseDate) + 2592000000, count: user.unlimited_post_count.count };
                                                //     let anotherObj11 = { unlimited_post_count: newObject11 }
                                                //     Funn.updateUnlimitedCount(user_id, anotherObj11, (yes) => {
                                                //         if (yes.error == false) {
                                                //             console.log("7")
                                                //             res.json({ error: false, update: true })
                                                //         }
                                                //     })
                                                // }

                                                break;

                                            case "AutoRenewableUnlimitedStoriesForOneMonth1":

                                                // if (user.unlimited_story_count.activation != "") {
                                                //     if (firstProduct.purchaseDate > user.unlimited_story_count.activation) {
                                                //         let date2 = parseInt(user.unlimited_story_count.expiry) + 2592000000;
                                                //         if (firstProduct.purchaseDate > date2) {
                                                //             let newObject2 = { status: true, activation: firstProduct.purchaseDate, expiry: parseInt(firstProduct.purchaseDate) + 2592000000, count: user.unlimited_story_count.count };
                                                //             let anotherObj2 = { unlimited_story_count: newObject2 }
                                                //             Funn.updateUnlimitedCount(user_id, anotherObj2, (yes) => {
                                                //                 if (yes.error == false) {
                                                //                     res.json({ error: false, update: true })
                                                //                 }
                                                //             })
                                                //         } else {
                                                let newObject22 = {
                                                    status: true, activation: mainProduct.purchaseDate, expiry: mainProduct.expirationDate,
                                                    count: user.unlimited_story_count.count
                                                };
                                                let anotherObj22 = { unlimited_story_count: newObject22 }
                                                Funn.updateUnlimitedCount(user_id, anotherObj22, (yes) => {
                                                    if (yes.error == false) {
                                                        console.log("successfully updatecount in restore purchase story")
                                                        res.json({ error: false, update: true })
                                                    }
                                                })

                                                InApp.update({ _id: ObjectId(data._id), "ios_response.originalTransactionId": mainProduct.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": mainProduct.originalTransactionId } } }, function (error, updateData) {
                                                    if (error) {
                                                        console.log("error while pulll in restore purchase story ", error.message);
                                                    }
                                                    else {
                                                        console.log("successfully pull in restore purchase story  ");
                                                        InApp.findOneAndUpdate({ _id: ObjectId(data._id) }, { $push: { ios_response: mainProduct } }, function (err, done) {
                                                            if (err) {
                                                                console.log("error while pushing in restore purchase story ", err.message);
                                                            }
                                                            else {
                                                                console.log("push coplete, Everything is done in restore purchase story  ");
                                                            }
                                                        })
                                                    }
                                                })
                                                //         }
                                                //     } else {
                                                //         res.json({ error: false, update: false })
                                                //     }
                                                // } else {
                                                //     let newObject11 = { status: true, activation: firstProduct.purchaseDate, expiry: parseInt(firstProduct.purchaseDate) + 2592000000, count: user.unlimited_story_count.count };
                                                //     let anotherObj11 = { unlimited_story_count: newObject11 }
                                                //     Funn.updateUnlimitedCount(user_id, anotherObj11, (yes) => {
                                                //         if (yes.error == false) {
                                                //             res.json({ error: false, update: true })
                                                //         }
                                                //     })
                                                // }

                                                break;

                                            case "AutoRenewableUnlimitedLiveStreamsForOneMonth1":

                                                // if (user.unlimited_live_count.activation != "") {
                                                //     if (firstProduct.purchaseDate > user.unlimited_live_count.activation) {
                                                //         let date3 = parseInt(user.unlimited_live_count.expiry) + 2592000000;
                                                //         if (firstProduct.purchaseDate > date3) {
                                                //             let newObject3 = { status: true, activation: firstProduct.purchaseDate, expiry: parseInt(firstProduct.purchaseDate) + 2592000000, count: user.unlimited_live_count.count };
                                                //             let anotherObj3 = { unlimited_live_count: newObject3 }
                                                //             Funn.updateUnlimitedCount(user_id, anotherObj3, (yes) => {
                                                //                 if (yes.error == false) {
                                                //                     res.json({ error: false, update: true })
                                                //                 }
                                                //             })
                                                //         } else {
                                                let newObject33 = { status: true, activation: mainProduct.purchaseDate, expiry: mainProduct.expirationDate, count: user.unlimited_live_count.count };
                                                let anotherObj33 = { unlimited_live_count: newObject33 }
                                                Funn.updateUnlimitedCount(user_id, anotherObj33, (yes) => {
                                                    if (yes.error == false) {
                                                        console.log("succesfully update count in restore purchase live ")
                                                        res.json({ error: false, update: true })
                                                    }
                                                })

                                                InApp.update({ _id: ObjectId(data._id), "ios_response.originalTransactionId": mainProduct.originalTransactionId }, { $pull: { "ios_response": { "originalTransactionId": mainProduct.originalTransactionId } } }, function (error, updateData) {
                                                    if (error) {
                                                        console.log("error while pulll  in restore purchase live ", error.message);
                                                    }
                                                    else {
                                                        console.log("successfully pull in restore purchase live");
                                                        InApp.findOneAndUpdate({ _id: ObjectId(data._id) }, { $push: { ios_response: mainProduct } }, function (err, done) {
                                                            if (err) {
                                                                console.log("error while pushing in restore purchase live ", err.message);
                                                            }
                                                            else {
                                                                console.log("push coplete, Everything is done  in restore purchase live ");
                                                            }
                                                        })
                                                    }
                                                })
                                                //         }
                                                //     } else {
                                                //         res.json({ error: false, update: false })
                                                //     }
                                                // } else {
                                                //     let newObject11 = { status: true, activation: firstProduct.purchaseDate, expiry: parseInt(firstProduct.purchaseDate) + 2592000000, count: user.unlimited_live_count.count };
                                                //     let anotherObj11 = { unlimited_live_count: newObject11 }
                                                //     Funn.updateUnlimitedCount(user_id, anotherObj11, (yes) => {
                                                //         if (yes.error == false) {
                                                //             res.json({ error: false, update: true })
                                                //         }
                                                //     })
                                                // }

                                                break;
                                        }
                                    }
                                })

                            } else {
                                res.json({ error: true, message: "You are not enrolled for this subscription" })
                            }
                        } else {
                            res.json({ error: true, message: "You are not enrolled for this subscription" })
                        }
                    }
                })

                console.log("RESTORE COMPLETED")
                // return;

                // if (data.apple_receipt == "") {
                //     res.json({ error: false, updated: true, message: "You dont't have any subsciption for this product." });
                // } else {

                //     appleReceiptVerify.validate({
                //         receipt: data.apple_receipt
                //     }, function (err, products) {
                //         if (err) {
                //             return console.error(err);
                //         } else {
                //             Funn.userDetails(user_id, (yes) => {
                //                 if (yes.error == false) {
                //                     let user = yes.description;
                //                     var productArrFiltered = products.filter(product => product.productId == product_id);
                //                     console.log("productArrFiltered=>>> ", productArrFiltered)

                //                     var productArrSorted = productArrFiltered.sort((product1, product2) => {
                //                         return (product1.purchaseDate > product2.purchaseDate) ? -1 : 1;
                //                     })
                //                     console.log("productArrSorted=>>> ", productArrSorted)

                //                     var firstProduct = productArrSorted[0];

                //                     switch (product_id) {

                //                         case "UnlimitedPostForOneMonth":

                //                             if (firstProduct.purchaseDate > user.unlimited_post_count.activation) {
                //                                 if (user.unlimited_post_count.expiry == "")
                //                                     letsDoThis("post", firstProduct, firstProduct.purchaseDate, user.unlimited_post_count.count)
                //                                 else
                //                                     letsDoThis("post", firstProduct, user.unlimited_post_count.expiry, user.unlimited_post_count.count)
                //                             } else {
                //                                 res.json({ error: false, update: false })
                //                             }
                //                             break;

                //                         case "UnlimitedStoriesForOneMonth":

                //                             if (firstProduct.purchaseDate > user.unlimited_story_count.activation) {
                //                                 if (user.unlimited_story_count.expiry == "")
                //                                     letsDoThis("story", firstProduct, firstProduct.purchaseDate, user.unlimited_story_count.count)
                //                                 else
                //                                     letsDoThis("story", firstProduct, user.unlimited_story_count.expiry, user.unlimited_story_count.count)
                //                             } else {
                //                                 res.json({ error: false, update: false })
                //                             }
                //                             break;

                //                         case "UnlimitedLiveStreamsForOneMonth":

                //                             if (firstProduct.purchaseDate > user.unlimited_live_count.activation) {
                //                                 if (user.unlimited_live_count.expiry == "")
                //                                     letsDoThis("live", firstProduct, firstProduct.purchaseDate, user.unlimited_live_count.count)
                //                                 else
                //                                     letsDoThis("live", firstProduct, user.unlimited_live_count.expiry, user.unlimited_live_count.count)
                //                             } else {
                //                                 res.json({ error: false, update: false })
                //                             }
                //                             break;

                //                         default:
                //                             console.log(product_id);
                //                     }
                //                 }
                //             })
                //         }
                //     })
                // }
            }
        })

        function letsDoThis(type, product, userExpiryDate, userCount) {
            let expiryDate = parseInt(userExpiryDate) + 2592000000;
            if (type == "post") {
                let unObj = { status: true, activation: product.purchaseDate, expiry: expiryDate, count: userCount }
                let obj = { unlimited_post_count: unObj }
                Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                    if (yes.error == false) {
                        res.json({ error: false, update: true })
                    }
                })
            } else if (type == "story") {
                let unObj = { status: true, activation: product.purchaseDate, expiry: expiryDate, count: userCount }
                let obj = { unlimited_story_count: unObj }
                Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                    if (yes.error == false) {
                        res.json({ error: false, update: true })
                    }
                })
            } else {
                let unObj = { status: true, activation: product.purchaseDate, expiry: expiryDate, count: userCount }
                let obj = { unlimited_live_count: unObj }
                Fun.updateUnlimitedCount(user_id, obj, (yes) => {
                    if (yes.error == false) {
                        res.json({ error: false, update: true })
                    }
                })
            }

            // InApp.findOneAndUpdate({ user_id: ObjectId(user_id) }, { $set: { apple_receipt: appleReceipt } }, function (error, data) {
            //     if (error) {
            //         console.log({ error: true, message: error });
            //     } else if (!data) {
            //         console.log({ error: true, message: "User not found" });
            //     } else {

            //     }
            // })
        }
    });

    app.post("/observeSubscriptionStatus", function (req, res) {
        console.log("requet in 9000 is ", req.body)

        res.status(200)
        res.end();
    })

    // -----------------------------------------------------------------   API'S END   -----------------------------------------------------------------

    // NOTE: ALL THE COMMENTS ARE AVAILABLE IN THE COMMITS BEFORE 26-FEB-2019

}