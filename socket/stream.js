var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

var Streams = require('../models/stream');
var User = require('../models/user');
var Story = require('../models/story');
var Fun = require('../functions/function')

module.exports = function (io) {

    io.on('connection', function (socket) {

        let socket_id = socket.id;

        socket.on('updateOnlineStatus', function (params) {                                           //    TO CHANGE ONLINE STATUS
            console.log("updateOnlineStatus");
            let user_id = params.user_id;
            let online = params.is_online;
            let playerId = params.playerId;
            let type = params.user_type;
            let last_login = params.last_login;
            // console.log("id: ", playerId, ' - type: ', type, " - user_id", user_id, ' - online: ', online, " socketId: ", socket_id, " last login: ", last_login)

            if (type == null || type == "") {
                console.log(lakshya)
            }

            if (online) {
                // console.log("user Online: ", user_id, '/ socketId: ', socket.id);
                Fun.userDetails(user_id, (userData) => {
                    // console.log("user data on line 92 >>>>>> ", userData);
                    if (userData.error == false) {
                        if (userData.description.last_login == last_login && userData.description.is_admin_block == false) {
                            console.log("condition true")
                            User.findOneAndUpdate({ _id: ObjectId(user_id) }, { socket_id: socket.id, is_online: online, playerId: playerId, user_type: type, last_login: last_login }, function (error, result) {
                                if (error) {
                                    console.log("Error while updating socket id.", error);
                                } else {
                                    socket.emit('onlineStatusUpdated', { data: result });
                                    // console.log("emitttttteeeeeeeeeedddddddddddddd")
                                }
                            })
                        } else {
                            console.log("something is fishieeeeeeeeeeeeeeee")
                            io.to(socket_id).emit("logout", { error: true, message: "You have been logged in some other device also" })
                        }
                    } else {
                        console.log("something went wrong in updateOnlineStatus")
                    }
                })

            } else {

                User.findOneAndUpdate({ _id: ObjectId(user_id) }, { socket_id: "", is_online: online, playerId: playerId }, function (error, result) {
                    if (error) {
                        console.log("Error while updating socket id.", error);
                    } else {
                        socket.emit('onlineStatusUpdated', { data: result });
                    }
                })
            }

        });

        socket.on('stopStream', function (params) {                                                   //    TO STOP STREAM AND CHANGE STATUS

            console.log("stopStream", params.user_id, )
            let user_id = params.user_id;
            let stream_id = params.stream_id;

            Streams.findOneAndUpdate({ user_id: ObjectId(user_id), status: "running" }, { status: "stopped" }, function (error, data) {
                if (error) {
                    console.log("Error while updating status stopped of stream.");
                } else if (!data) {
                    console.log("Stream already stopped.")
                } else {
                    console.log("1")
                    Streams.findOne({ _id: ObjectId(stream_id) }, function (error, stream) {
                        if (error) {
                            console.log("Something went wrong while getting stream details in stopStream socket.", error);
                        } else if (!stream) {
                            console.log("Stream updated but after that might be stream not found.");
                        } else {
                            User.findOneAndUpdate({ _id: ObjectId(user_id) }, { is_live: "stopped" }, function (error, updatedUser) {
                                if (error) {
                                    console.log({ error: true, message: "Error while updating is_live status of user", description: error });
                                } else if (!updatedUser) {
                                    console.log({ error: true, message: "No user found to update is_live status" });
                                } else {
                                    User.find(function (error, result) {
                                        if (error) {
                                            console.log("Error while stop stream.", error);
                                        } else {
                                            let streamer;
                                            for (let i = 0; i < result.length; i++) {
                                                if (data.user_id.toString() == result[i]._id.toString()) {
                                                    streamer = result[i];
                                                }
                                            }
                                            // console.log('stream status',stream.status)
                                            for (let i = 0; i < result.length; i++) {
                                                io.to(result[i].socket_id).emit("updateStream", { error: false, stream: stream, userDetails: streamer });
                                            }
                                            console.log("Stream successfully stopped.");
                                        }
                                    })
                                }
                            })
                        }
                    })

                }
            })

            // socket.on('stopStream', function (params) {              //  In future if need to emit only to its followers.            //  TO CHANGE STATUS OF STREAM
            //     console.log("StopStream");
            //     let user_id = ObjectId(params.user_id);
            //     let stream_name = params.stream_name;
            //     Streams.findOneAndUpdate({ user_id: user_id, status: "running", stream_name: stream_name }, { status: "stopped" }, function (error, data) {
            //         User.findOneAndUpdate({ _id: user_id, is_live: "running" }, { is_live: "stopped" }, function (error2, done) {
            //             if (error2) {
            //                 console.log("Error while updating status stopped in user node on is_live.");
            //             } else {
            //                 console.log("Status successfully updated is_live is stopped now.");
            //             }
            //             console.log("first time data is : ", data)
            //             if (error) {
            //                 console.log("Error while changing status in stream.", error);
            //             } else if (!data) {
            //                 console.log("No result found.");
            //             } else {
            //                 let stream = data;
            //                 let id = data.user_id;
            //                 Streams.aggregate([{ $match: { user_id: id, stream_name: stream_name } },
            //                 { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } },
            //                 ], function (error, result) {
            //                     if (error) {
            //                         console.log("error at the time of getting data with lookup after saving.", error);
            //                     } else if (result.length == 0) {
            //                         console.log("No data found");
            //                     } else {
            //                         User.aggregate([{ $match: { _id: id } },                //   This commenet code is emiting only to the followers of a particular user.
            //                         { $lookup: { from: "users", localField: "followers.user_id", foreignField: "_id", as: "followers" } }],
            //                             function (err, allData) {
            //                                 if (err) {
            //                                     console.log("error is ", err);
            //                                 }
            //                                 else {
            //                                     let i = 0;
            //                                     allData[0].followers.forEach(element => {
            //                                         console.log("offline ", i++);
            //                                         let room_id = id;
            //                                         console.log("data socket id is", element.socket_id);
            //                                         io.to(element.socket_id).emit("updateStream", { error: false, stream: stream, user_id: id });
            //                                     });
            //                                 }
            //                             })
            //                     }
            //                 })
            //             }
            //         })
            //     })
            // });
            console.log(" ")
        });
 
        socket.on('joinLeaveStream', function (params) {                                              //    TOP JOIN LIVE STREAM
            console.log(">>>>>>>>>>> ", "joinLeaveStream is_join ", params.is_join, " streamid: ", params.stream_id, " user_id: ", params.user_id);
            let user_id = params.user_id;
            let stream_id = params.stream_id;
            let is_join = params.is_join;

            Streams.findOne({ _id: ObjectId(stream_id), status: "running" }, function (error, result) {
                if (error) {
                    console.log("Error while finding live stream at the of joining.")
                } else if (!result) {
                    console.log("No data found in joinLeaveStream");
                } else {
                    let stream;
                    let count = result.viewerCount;
                    if (count >= 0) {
                        if (is_join)
                            count++;
                        else
                            count--;
                    } else {
                        count = 0;
                    }

                    Streams.findOneAndUpdate({ _id: ObjectId(stream_id), status: "running" }, { viewerCount: count }, function (error, data) {
                        if (error) {
                            console.log("Error while join")
                        } else {
                            Streams.findOne({ _id: stream_id }, function (error, latest) {
                                if (error) {
                                    console.log("Error in the second Last of joinLeaveStream", error);
                                } else {
                                    stream = latest;
                                    User.find(function (error, details) {
                                        if (error) {
                                            console.log("Error in the last.");
                                        } else {
                                            let userCommented;
                                            let streamer;
                                            for (let i = 0; i < details.length; i++) {
                                                if (latest.user_id.toString() == details[i]._id.toString()) {
                                                    userCommented = details[i];
                                                }
                                                if (data.user_id.toString() == details[i]._id.toString())
                                                    streamer = details[i];
                                            }
                                            for (let i = 0; i < details.length; i++) {
                                                io.to(details[i].socket_id).emit("updateStream", { error: false, stream: stream, joined_user_id: userCommented, userDetails: streamer });
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        });

        socket.on('addComment', function (params) {                                                   //    TO ADD COMMENTS ON LIVE STREAM
            console.log("addComment");
            let user_id = ObjectId(params.user_id);
            let text = params.text;
            let type = params.type;
            let stream_id = ObjectId(params.stream_id), time = new Date().getTime();
            // comment include all data 
            let comment = { user_id: user_id, created_at: time }
            if (type == "comment") {
                comment.comment = text;
            }
            else {
                comment.amount = text;
            }
            Streams.findOneAndUpdate({ _id: stream_id, status: "running" }, { $push: { comment: comment } }, function (error, dataStream) {
                if (error) {
                    console.log("Error while updating comment in stream through socket.", error);
                } else if (!dataStream) {
                    console.log("No data found. ID= ", stream_id);
                } else {
                    Streams.aggregate([{ $match: { _id: stream_id } },
                    { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userDetails' } }, { $unwind: '$comment' },
                    { $lookup: { from: 'users', localField: 'comment.user_id', foreignField: '_id', as: 'comment.user_id' } },
                    {
                        $group: {
                            _id: {
                                _id: '$_id', stream_name: '$stream_name', user_id: '$userDetails', status: '$status', viewerCount: '$viewerCount', country: '$country',
                                thumbUrl: '$thumbUrl'
                            },
                            'comments': { '$push': '$comment' },
                        }
                    }
                    ], function (error, result) {
                        if (!error) {
                            User.find(function (error, data) {
                                if (error) {
                                    console.log("Error while fetching stream.")
                                } else {
                                    // console.log("result ", result)
                                    for (let i = 0; i < data.length; i++) {
                                        io.to(data[i].socket_id).emit("updateStream", { error: false, message: "Done", stream: result[0]._id, comments: result[0].comments, userDetails: result[0]._id.user_id[0] });
                                        console.log( '-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-', { error: false, message: "Done", stream: result[0]._id, comments: result[0].comments, userDetails: result[0]._id.user_id[0] })
                                    }
                                }
                            })
                        } else {
                            console.log("Error int the last of addComment");
                        }
                    })
                }
            })
        });
        
    })
}